## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Okta. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Okta.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Okta. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">listApplications(q, after, limit, filter, expand, includeNonDeleted, callback)</td>
    <td style="padding:15px">List Applications</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplication(application, activate, callback)</td>
    <td style="padding:15px">Add Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplication(appId, callback)</td>
    <td style="padding:15px">Delete Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplication(appId, expand, callback)</td>
    <td style="padding:15px">Get Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApplication(appId, application, callback)</td>
    <td style="padding:15px">Update Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCsrsForApplication(appId, callback)</td>
    <td style="padding:15px">List Certificate Signing Requests for Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/credentials/csrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateCsrForApplication(appId, metadata, callback)</td>
    <td style="padding:15px">Generate Certificate Signing Request for Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/credentials/csrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeCsrFromApplication(appId, csrId, callback)</td>
    <td style="padding:15px">revokeCsrFromApplication</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/credentials/csrs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsrForApplication(appId, csrId, callback)</td>
    <td style="padding:15px">getCsrForApplication</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/credentials/csrs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApiV1AppsAppIdCredentialsCsrsCsrIdLifecyclePublish(appId, csrId, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/credentials/csrs/{pathv2}/lifecycle/publish?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listApplicationKeys(appId, callback)</td>
    <td style="padding:15px">List Key Credentials for Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/credentials/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateApplicationKey(appId, validityYears, callback)</td>
    <td style="padding:15px">Generates a new X.509 certificate for an application key credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/credentials/keys/generate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationKey(appId, keyId, callback)</td>
    <td style="padding:15px">Get Key Credential for Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/credentials/keys/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloneApplicationKey(appId, keyId, targetAid, callback)</td>
    <td style="padding:15px">Clone Application Key Credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/credentials/keys/{pathv2}/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listScopeConsentGrants(appId, expand, callback)</td>
    <td style="padding:15px">Lists all scope consent grants for the application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/grants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">grantConsentToScope(appId, oAuth2ScopeConsentGrant, callback)</td>
    <td style="padding:15px">Grants consent for the application to request an OAuth 2.0 Okta scope</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/grants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeScopeConsentGrant(appId, grantId, callback)</td>
    <td style="padding:15px">Revokes permission for the application to request the given scope</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/grants/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScopeConsentGrant(appId, grantId, expand, callback)</td>
    <td style="padding:15px">Fetches a single scope consent grant for the application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/grants/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listApplicationGroupAssignments(appId, q, after, limit, expand, callback)</td>
    <td style="padding:15px">List Groups Assigned to Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplicationGroupAssignment(appId, groupId, callback)</td>
    <td style="padding:15px">Remove Group from Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationGroupAssignment(appId, groupId, expand, callback)</td>
    <td style="padding:15px">Get Assigned Group for Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplicationGroupAssignment(appId, groupId, applicationGroupAssignment, callback)</td>
    <td style="padding:15px">Assign Group to Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateApplication(appId, callback)</td>
    <td style="padding:15px">Activate Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/lifecycle/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateApplication(appId, callback)</td>
    <td style="padding:15px">Deactivate Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/lifecycle/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeOAuth2TokensForApplication(appId, callback)</td>
    <td style="padding:15px">Revokes all tokens for the specified application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listOAuth2TokensForApplication(appId, expand, after, limit, callback)</td>
    <td style="padding:15px">Lists all tokens for the application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeOAuth2TokenForApplication(appId, tokenId, callback)</td>
    <td style="padding:15px">Revokes the specified token for the specified application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/tokens/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOAuth2TokenForApplication(appId, tokenId, expand, callback)</td>
    <td style="padding:15px">Gets a token for the specified application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/tokens/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listApplicationUsers(appId, q, queryScope, after, limit, filter, expand, callback)</td>
    <td style="padding:15px">List Users Assigned to Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignUserToApplication(appId, appUser, callback)</td>
    <td style="padding:15px">Assign User to Application for SSO & Provisioning</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplicationUser(appId, userId, sendEmail, callback)</td>
    <td style="padding:15px">Remove User from Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationUser(appId, userId, expand, callback)</td>
    <td style="padding:15px">Get Assigned User for Application</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApplicationUser(appId, userId, appUser, callback)</td>
    <td style="padding:15px">Update Application Profile for Assigned User</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/apps/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAuthorizationServers(q, limit, after, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthorizationServer(authorizationServer, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthorizationServer(authServerId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthorizationServer(authServerId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAuthorizationServer(authServerId, authorizationServer, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listOAuth2Claims(authServerId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/claims?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOAuth2Claim(authServerId, oAuth2Claim, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/claims?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOAuth2Claim(authServerId, claimId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/claims/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOAuth2Claim(authServerId, claimId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/claims/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOAuth2Claim(authServerId, claimId, oAuth2Claim, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/claims/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listOAuth2ClientsForAuthorizationServer(authServerId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeRefreshTokensForAuthorizationServerAndClient(authServerId, clientId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/clients/{pathv2}/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRefreshTokensForAuthorizationServerAndClient(authServerId, clientId, expand, after, limit, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/clients/{pathv2}/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeRefreshTokenForAuthorizationServerAndClient(authServerId, clientId, tokenId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/clients/{pathv2}/tokens/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRefreshTokenForAuthorizationServerAndClient(authServerId, clientId, tokenId, expand, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/clients/{pathv2}/tokens/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAuthorizationServerKeys(authServerId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/credentials/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rotateAuthorizationServerKeys(authServerId, use, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/credentials/lifecycle/keyRotate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateAuthorizationServer(authServerId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/lifecycle/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateAuthorizationServer(authServerId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/lifecycle/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAuthorizationServerPolicies(authServerId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthorizationServerPolicy(authServerId, policy, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthorizationServerPolicy(authServerId, policyId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthorizationServerPolicy(authServerId, policyId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAuthorizationServerPolicy(authServerId, policyId, policy, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateAuthorizationServerPolicy(authServerId, policyId, callback)</td>
    <td style="padding:15px">Activate Authorization Server Policy</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/policies/{pathv2}/lifecycle/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateAuthorizationServerPolicy(authServerId, policyId, callback)</td>
    <td style="padding:15px">Deactivate Authorization Server Policy</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/policies/{pathv2}/lifecycle/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAuthorizationServerPolicyRules(policyId, authServerId, callback)</td>
    <td style="padding:15px">Enumerates all policy rules for the specified Custom Authorization Server and Policy.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/policies/{pathv2}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthorizationServerPolicyRule(policyId, authServerId, policyRule, callback)</td>
    <td style="padding:15px">Creates a policy rule for the specified Custom Authorization Server and Policy.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/policies/{pathv2}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthorizationServerPolicyRule(policyId, authServerId, ruleId, callback)</td>
    <td style="padding:15px">Deletes a Policy Rule defined in the specified Custom Authorization Server and Policy.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/policies/{pathv2}/rules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthorizationServerPolicyRule(policyId, authServerId, ruleId, callback)</td>
    <td style="padding:15px">Returns a Policy Rule by ID that is defined in the specified Custom Authorization Server and Policy.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/policies/{pathv2}/rules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAuthorizationServerPolicyRule(policyId, authServerId, ruleId, policyRule, callback)</td>
    <td style="padding:15px">Updates the configuration of the Policy Rule defined in the specified Custom Authorization Server a</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/policies/{pathv2}/rules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateAuthorizationServerPolicyRule(authServerId, policyId, ruleId, callback)</td>
    <td style="padding:15px">Activate Authorization Server Policy Rule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/policies/{pathv2}/rules/{pathv3}/lifecycle/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateAuthorizationServerPolicyRule(authServerId, policyId, ruleId, callback)</td>
    <td style="padding:15px">Deactivate Authorization Server Policy Rule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/policies/{pathv2}/rules/{pathv3}/lifecycle/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listOAuth2Scopes(authServerId, q, filter, cursor, limit, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/scopes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOAuth2Scope(authServerId, oAuth2Scope, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/scopes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOAuth2Scope(authServerId, scopeId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/scopes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOAuth2Scope(authServerId, scopeId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/scopes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOAuth2Scope(authServerId, scopeId, oAuth2Scope, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/authorizationServers/{pathv1}/scopes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listEventHooks(callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/eventHooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEventHook(eventHook, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/eventHooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEventHook(eventHookId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/eventHooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventHook(eventHookId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/eventHooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateEventHook(eventHookId, eventHook, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/eventHooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateEventHook(eventHookId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/eventHooks/{pathv1}/lifecycle/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateEventHook(eventHookId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/eventHooks/{pathv1}/lifecycle/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyEventHook(eventHookId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/eventHooks/{pathv1}/lifecycle/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFeatures(callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/features?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFeature(featureId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/features/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFeatureDependencies(featureId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/features/{pathv1}/dependencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFeatureDependents(featureId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/features/{pathv1}/dependents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFeatureLifecycle(featureId, lifecycle, mode, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/features/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroups(q, filter, after, limit, expand, callback)</td>
    <td style="padding:15px">List Groups</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroup(group, callback)</td>
    <td style="padding:15px">Add Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroupRules(limit, after, search, expand, callback)</td>
    <td style="padding:15px">List Group Rules</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroupRule(groupRule, callback)</td>
    <td style="padding:15px">Create Group Rule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupRule(ruleId, callback)</td>
    <td style="padding:15px">Delete a group Rule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupRule(ruleId, expand, callback)</td>
    <td style="padding:15px">Get Group Rule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroupRule(ruleId, groupRule, callback)</td>
    <td style="padding:15px">Updates a group rule. Only `INACTIVE` rules can be updated.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateGroupRule(ruleId, callback)</td>
    <td style="padding:15px">Activate a group Rule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/rules/{pathv1}/lifecycle/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateGroupRule(ruleId, callback)</td>
    <td style="padding:15px">Deactivate a group Rule</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/rules/{pathv1}/lifecycle/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroup(groupId, callback)</td>
    <td style="padding:15px">Remove Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroup(groupId, callback)</td>
    <td style="padding:15px">List Group Rules</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroup(groupId, group, callback)</td>
    <td style="padding:15px">Update Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAssignedApplicationsForGroup(groupId, after, limit, callback)</td>
    <td style="padding:15px">List Assigned Applications</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroupAssignedRoles(groupId, expand, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignRoleToGroup(assignRoleRequest, groupId, disableNotifications, callback)</td>
    <td style="padding:15px">Assigns a Role to a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeRoleFromGroup(groupId, roleId, callback)</td>
    <td style="padding:15px">Unassigns a Role from a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRole(groupId, roleId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listApplicationTargetsForApplicationAdministratorRoleForGroup(groupId, roleId, after, limit, callback)</td>
    <td style="padding:15px">Lists all App targets for an `APP_ADMIN` Role assigned to a Group. This methods return list may inc</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/roles/{pathv2}/targets/catalog/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeApplicationTargetFromApplicationAdministratorRoleGivenToGroup(groupId, roleId, appName, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/roles/{pathv2}/targets/catalog/apps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addApplicationTargetToAdminRoleGivenToGroup(groupId, roleId, appName, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/roles/{pathv2}/targets/catalog/apps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeApplicationTargetFromAdministratorRoleGivenToGroup(groupId, roleId, appName, applicationId, callback)</td>
    <td style="padding:15px">Remove App Instance Target to App Administrator Role given to a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/roles/{pathv2}/targets/catalog/apps/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addApplicationInstanceTargetToAppAdminRoleGivenToGroup(groupId, roleId, appName, applicationId, callback)</td>
    <td style="padding:15px">Add App Instance Target to App Administrator Role given to a Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/roles/{pathv2}/targets/catalog/apps/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroupTargetsForGroupRole(groupId, roleId, after, limit, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/roles/{pathv2}/targets/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeGroupTargetFromGroupAdministratorRoleGivenToGroup(groupId, roleId, targetGroupId, callback)</td>
    <td style="padding:15px">removeGroupTargetFromGroupAdministratorRoleGivenToGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/roles/{pathv2}/targets/groups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addGroupTargetToGroupAdministratorRoleForGroup(groupId, roleId, targetGroupId, callback)</td>
    <td style="padding:15px">addGroupTargetToGroupAdministratorRoleForGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/roles/{pathv2}/targets/groups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroupUsers(groupId, after, limit, callback)</td>
    <td style="padding:15px">List Group Members</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeUserFromGroup(groupId, userId, callback)</td>
    <td style="padding:15px">Remove User from Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUserToGroup(groupId, userId, callback)</td>
    <td style="padding:15px">Add User to Group</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/groups/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIdentityProviders(q, after, limit, type, callback)</td>
    <td style="padding:15px">List Identity Providers</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIdentityProvider(identityProvider, callback)</td>
    <td style="padding:15px">Add Identity Provider</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIdentityProviderKeys(after, limit, callback)</td>
    <td style="padding:15px">List Keys</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/credentials/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIdentityProviderKey(jsonWebKey, callback)</td>
    <td style="padding:15px">Add X.509 Certificate Public Key</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/credentials/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityProviderKey(keyId, callback)</td>
    <td style="padding:15px">Delete Key</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/credentials/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityProviderKey(keyId, callback)</td>
    <td style="padding:15px">Get Key</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/credentials/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityProvider(idpId, callback)</td>
    <td style="padding:15px">Delete Identity Provider</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityProvider(idpId, callback)</td>
    <td style="padding:15px">Get Identity Provider</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIdentityProvider(idpId, identityProvider, callback)</td>
    <td style="padding:15px">Update Identity Provider</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCsrsForIdentityProvider(idpId, callback)</td>
    <td style="padding:15px">List Certificate Signing Requests for IdP</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/credentials/csrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateCsrForIdentityProvider(idpId, metadata, callback)</td>
    <td style="padding:15px">Generate Certificate Signing Request for IdP</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/credentials/csrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeCsrForIdentityProvider(idpId, csrId, callback)</td>
    <td style="padding:15px">Revoke a Certificate Signing Request and delete the key pair from the IdP</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/credentials/csrs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCsrForIdentityProvider(idpId, csrId, callback)</td>
    <td style="padding:15px">Gets a specific Certificate Signing Request model by id</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/credentials/csrs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApiV1IdpsIdpIdCredentialsCsrsCsrIdLifecyclePublish(idpId, csrId, callback)</td>
    <td style="padding:15px">Update the Certificate Signing Request with a signed X.509 certificate and add it into the signing</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/credentials/csrs/{pathv2}/lifecycle/publish?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIdentityProviderSigningKeys(idpId, callback)</td>
    <td style="padding:15px">List Signing Key Credentials for IdP</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/credentials/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateIdentityProviderSigningKey(idpId, validityYears, callback)</td>
    <td style="padding:15px">Generate New IdP Signing Key Credential</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/credentials/keys/generate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityProviderSigningKey(idpId, keyId, callback)</td>
    <td style="padding:15px">Get Signing Key Credential for IdP</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/credentials/keys/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloneIdentityProviderKey(idpId, keyId, targetIdpId, callback)</td>
    <td style="padding:15px">Clone Signing Key Credential for IdP</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/credentials/keys/{pathv2}/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateIdentityProvider(idpId, callback)</td>
    <td style="padding:15px">Activate Identity Provider</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/lifecycle/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateIdentityProvider(idpId, callback)</td>
    <td style="padding:15px">Deactivate Identity Provider</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/lifecycle/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIdentityProviderApplicationUsers(idpId, callback)</td>
    <td style="padding:15px">Find Users</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unlinkUserFromIdentityProvider(idpId, userId, callback)</td>
    <td style="padding:15px">Unlink User from IdP</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityProviderApplicationUser(idpId, userId, callback)</td>
    <td style="padding:15px">Fetches a linked IdP user by ID</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">linkUserToIdentityProvider(idpId, userId, userIdentityProviderLinkRequest, callback)</td>
    <td style="padding:15px">Link a user to a Social IdP without a transaction</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/users/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSocialAuthTokens(idpId, userId, callback)</td>
    <td style="padding:15px">Social Authentication Token Operation</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/idps/{pathv1}/users/{pathv2}/credentials/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listInlineHooks(type, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/inlineHooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInlineHook(inlineHook, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/inlineHooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInlineHook(inlineHookId, callback)</td>
    <td style="padding:15px">Deletes the Inline Hook matching the provided id. Once deleted, the Inline Hook is unrecoverable. A</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/inlineHooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInlineHook(inlineHookId, callback)</td>
    <td style="padding:15px">Gets an inline hook by ID</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/inlineHooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInlineHook(inlineHookId, inlineHook, callback)</td>
    <td style="padding:15px">Updates an inline hook by ID</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/inlineHooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">executeInlineHook(inlineHookId, payloadData, callback)</td>
    <td style="padding:15px">Executes the Inline Hook matching the provided inlineHookId using the request body as the input. Th</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/inlineHooks/{pathv1}/execute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateInlineHook(inlineHookId, callback)</td>
    <td style="padding:15px">Activates the Inline Hook matching the provided id</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/inlineHooks/{pathv1}/lifecycle/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateInlineHook(inlineHookId, callback)</td>
    <td style="padding:15px">Deactivates the Inline Hook matching the provided id</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/inlineHooks/{pathv1}/lifecycle/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogs(since, until, filter, q, limit, sortOrder, after, callback)</td>
    <td style="padding:15px">Fetch a list of events from your Okta organization system log.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProfileMappings(after, limit, sourceId, targetId, callback)</td>
    <td style="padding:15px">Enumerates Profile Mappings in your organization with pagination.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/mappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProfileMapping(mappingId, callback)</td>
    <td style="padding:15px">Get Profile Mapping</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/mappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateProfileMapping(mappingId, profileMapping, callback)</td>
    <td style="padding:15px">Update Profile Mapping</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/mappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationUserSchema(appInstanceId, callback)</td>
    <td style="padding:15px">Fetches the Schema for an App User</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/meta/schemas/apps/{pathv1}/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApplicationUserProfile(appInstanceId, body, callback)</td>
    <td style="padding:15px">Partial updates on the User Profile properties of the Application User Schema.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/meta/schemas/apps/{pathv1}/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserSchema(schemaId, callback)</td>
    <td style="padding:15px">Fetches the schema for a Schema Id.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/meta/schemas/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserProfile(schemaId, userSchema, callback)</td>
    <td style="padding:15px">Partial updates on the User Profile properties of the user schema.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/meta/schemas/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLinkedObjectDefinitions(callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/meta/schemas/user/linkedObjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addLinkedObjectDefinition(linkedObject, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/meta/schemas/user/linkedObjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLinkedObjectDefinition(linkedObjectName, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/meta/schemas/user/linkedObjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLinkedObjectDefinition(linkedObjectName, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/meta/schemas/user/linkedObjects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserTypes(callback)</td>
    <td style="padding:15px">Fetches all User Types in your org</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/meta/types/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUserType(userType, callback)</td>
    <td style="padding:15px">Creates a new User Type. A default User Type is automatically created along with your org, and you</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/meta/types/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserType(typeId, callback)</td>
    <td style="padding:15px">Deletes a User Type permanently. This operation is not permitted for the default type, nor for any</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/meta/types/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserType(typeId, callback)</td>
    <td style="padding:15px">Fetches a User Type by ID. The special identifier `default` may be used to fetch the default User T</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/meta/types/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserType(typeId, userType, callback)</td>
    <td style="padding:15px">Updates an existing User Type</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/meta/types/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceUserType(typeId, userType, callback)</td>
    <td style="padding:15px">Replace an existing User Type</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/meta/types/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicies(type, status, expand, callback)</td>
    <td style="padding:15px">Gets all policies with the specified type.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicy(policy, activate, callback)</td>
    <td style="padding:15px">Creates a policy.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicy(policyId, callback)</td>
    <td style="padding:15px">Removes a policy.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicy(policyId, expand, callback)</td>
    <td style="padding:15px">Gets a policy.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicy(policy, policyId, callback)</td>
    <td style="padding:15px">Updates a policy.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activatePolicy(policyId, callback)</td>
    <td style="padding:15px">Activates a policy.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/lifecycle/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivatePolicy(policyId, callback)</td>
    <td style="padding:15px">Deactivates a policy.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/lifecycle/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicyRules(policyId, callback)</td>
    <td style="padding:15px">Enumerates all policy rules.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyRule(policyId, policyRule, callback)</td>
    <td style="padding:15px">Creates a policy rule.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyRule(policyId, ruleId, callback)</td>
    <td style="padding:15px">Removes a policy rule.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyRule(policyId, ruleId, callback)</td>
    <td style="padding:15px">Gets a policy rule.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyRule(policyId, ruleId, policyRule, callback)</td>
    <td style="padding:15px">Updates a policy rule.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activatePolicyRule(policyId, ruleId, callback)</td>
    <td style="padding:15px">Activates a policy rule.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/rules/{pathv2}/lifecycle/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivatePolicyRule(policyId, ruleId, callback)</td>
    <td style="padding:15px">Deactivates a policy rule.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/policies/{pathv1}/rules/{pathv2}/lifecycle/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSession(createSessionRequest, callback)</td>
    <td style="padding:15px">Create Session with Session Token</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">endSession(sessionId, callback)</td>
    <td style="padding:15px">Close Session</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSession(sessionId, callback)</td>
    <td style="padding:15px">Get details about a session.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshSession(sessionId, callback)</td>
    <td style="padding:15px">Refresh Session</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/sessions/{pathv1}/lifecycle/refresh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSmsTemplates(templateType, callback)</td>
    <td style="padding:15px">List SMS Templates</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/templates/sms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSmsTemplate(smsTemplate, callback)</td>
    <td style="padding:15px">Add SMS Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/templates/sms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSmsTemplate(templateId, callback)</td>
    <td style="padding:15px">Remove SMS Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/templates/sms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSmsTemplate(templateId, callback)</td>
    <td style="padding:15px">Get SMS Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/templates/sms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partialUpdateSmsTemplate(templateId, smsTemplate, callback)</td>
    <td style="padding:15px">Partial SMS Template Update</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/templates/sms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSmsTemplate(templateId, smsTemplate, callback)</td>
    <td style="padding:15px">Update SMS Template</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/templates/sms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentConfiguration(callback)</td>
    <td style="padding:15px">Gets current ThreatInsight configuration</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/threats/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConfiguration(threatInsightConfiguration, callback)</td>
    <td style="padding:15px">Updates ThreatInsight configuration</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/threats/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listOrigins(q, filter, after, limit, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/trustedOrigins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrigin(trustedOrigin, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/trustedOrigins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrigin(trustedOriginId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/trustedOrigins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrigin(trustedOriginId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/trustedOrigins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrigin(trustedOriginId, trustedOrigin, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/trustedOrigins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateOrigin(trustedOriginId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/trustedOrigins/{pathv1}/lifecycle/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateOrigin(trustedOriginId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/trustedOrigins/{pathv1}/lifecycle/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUsers(q, after, limit, filter, search, sortBy, sortOrder, callback)</td>
    <td style="padding:15px">List Users</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUser(body, activate, provider, nextLogin, callback)</td>
    <td style="padding:15px">Create User</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setLinkedObjectForUser(associatedUserId, primaryRelationshipName, primaryUserId, callback)</td>
    <td style="padding:15px">setLinkedObjectForUser</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/linkedObjects/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateOrDeleteUser(userId, sendEmail, callback)</td>
    <td style="padding:15px">Delete User</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUser(userId, callback)</td>
    <td style="padding:15px">Get User</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partialUpdateUser(user, userId, strict, callback)</td>
    <td style="padding:15px">Fetch a user by `id`, `login`, or `login shortname` if the short name is unambiguous.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUser(user, userId, strict, callback)</td>
    <td style="padding:15px">Update User</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppLinks(userId, callback)</td>
    <td style="padding:15px">Get Assigned App Links</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/appLinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserClients(userId, callback)</td>
    <td style="padding:15px">Lists all client resources for which the specified user has grants or tokens.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeGrantsForUserAndClient(userId, clientId, callback)</td>
    <td style="padding:15px">Revokes all grants for the specified user and client</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/clients/{pathv2}/grants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGrantsForUserAndClient(userId, clientId, expand, after, limit, callback)</td>
    <td style="padding:15px">Lists all grants for a specified user and client</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/clients/{pathv2}/grants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeTokensForUserAndClient(userId, clientId, callback)</td>
    <td style="padding:15px">Revokes all refresh tokens issued for the specified User and Client.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/clients/{pathv2}/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRefreshTokensForUserAndClient(userId, clientId, expand, after, limit, callback)</td>
    <td style="padding:15px">Lists all refresh tokens issued for the specified User and Client.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/clients/{pathv2}/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeTokenForUserAndClient(userId, clientId, tokenId, callback)</td>
    <td style="padding:15px">Revokes the specified refresh token.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/clients/{pathv2}/tokens/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRefreshTokenForUserAndClient(userId, clientId, tokenId, expand, limit, after, callback)</td>
    <td style="padding:15px">Gets a refresh token issued for the specified User and Client.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/clients/{pathv2}/tokens/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePassword(changePasswordRequest, userId, strict, callback)</td>
    <td style="padding:15px">Change Password</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/credentials/change_password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeRecoveryQuestion(userCredentials, userId, callback)</td>
    <td style="padding:15px">Change Recovery Question</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/credentials/change_recovery_question?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApiV1UsersUserIdCredentialsForgotPassword(userId, callback)</td>
    <td style="padding:15px">Forgot Password</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/credentials/forgot_password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeUserGrants(userId, callback)</td>
    <td style="padding:15px">Revokes all grants for a specified user</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/grants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserGrants(userId, scopeId, expand, after, limit, callback)</td>
    <td style="padding:15px">Lists all grants for the specified user</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/grants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeUserGrant(userId, grantId, callback)</td>
    <td style="padding:15px">Revokes one grant for a specified user</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/grants/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserGrant(userId, grantId, expand, callback)</td>
    <td style="padding:15px">Gets a grant for the specified user</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/grants/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserGroups(userId, callback)</td>
    <td style="padding:15px">Get Member Groups</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserIdentityProviders(userId, callback)</td>
    <td style="padding:15px">Listing IdPs associated with a user</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/idps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateUser(userId, sendEmail, callback)</td>
    <td style="padding:15px">Activate User</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/lifecycle/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateUser(userId, sendEmail, callback)</td>
    <td style="padding:15px">Deactivate User</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/lifecycle/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expirePassword(userId, callback)</td>
    <td style="padding:15px">Expire Password</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/lifecycle/expire_password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">expirePasswordAndGetTemporaryPassword(userId, callback)</td>
    <td style="padding:15px">Expire Password</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/lifecycle/expire_password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reactivateUser(userId, sendEmail, callback)</td>
    <td style="padding:15px">Reactivate User</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/lifecycle/reactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetFactors(userId, callback)</td>
    <td style="padding:15px">Reset Factors</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/lifecycle/reset_factors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetPassword(userId, sendEmail, callback)</td>
    <td style="padding:15px">Reset Password</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/lifecycle/reset_password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">suspendUser(userId, callback)</td>
    <td style="padding:15px">Suspend User</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/lifecycle/suspend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unlockUser(userId, callback)</td>
    <td style="padding:15px">Unlock User</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/lifecycle/unlock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unsuspendUser(userId, callback)</td>
    <td style="padding:15px">Unsuspend User</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/lifecycle/unsuspend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeLinkedObjectForUser(userId, relationshipName, callback)</td>
    <td style="padding:15px">Delete linked objects for a user, relationshipName can be ONLY a primary relationship name</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/linkedObjects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLinkedObjectsForUser(userId, relationshipName, after, limit, callback)</td>
    <td style="padding:15px">Get linked objects for a user, relationshipName can be a primary or associated relationship name</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/linkedObjects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAssignedRolesForUser(userId, expand, callback)</td>
    <td style="padding:15px">Lists all roles assigned to a user.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignRoleToUser(assignRoleRequest, userId, disableNotifications, callback)</td>
    <td style="padding:15px">Assigns a role to a user.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeRoleFromUser(userId, roleId, callback)</td>
    <td style="padding:15px">Unassigns a role from a user.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listApplicationTargetsForApplicationAdministratorRoleForUser(userId, roleId, after, limit, callback)</td>
    <td style="padding:15px">Lists all App targets for an `APP_ADMIN` Role assigned to a User. This methods return list may incl</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/roles/{pathv2}/targets/catalog/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAllAppsAsTargetToRole(userId, roleId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/roles/{pathv2}/targets/catalog/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeApplicationTargetFromApplicationAdministratorRoleForUser(userId, roleId, appName, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/roles/{pathv2}/targets/catalog/apps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addApplicationTargetToAdminRoleForUser(userId, roleId, appName, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/roles/{pathv2}/targets/catalog/apps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeApplicationTargetFromAdministratorRoleForUser(userId, roleId, appName, applicationId, callback)</td>
    <td style="padding:15px">Remove App Instance Target to App Administrator Role given to a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/roles/{pathv2}/targets/catalog/apps/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addApplicationTargetToAppAdminRoleForUser(userId, roleId, appName, applicationId, callback)</td>
    <td style="padding:15px">Add App Instance Target to App Administrator Role given to a User</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/roles/{pathv2}/targets/catalog/apps/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGroupTargetsForRole(userId, roleId, after, limit, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/roles/{pathv2}/targets/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeGroupTargetFromRole(userId, roleId, groupId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/roles/{pathv2}/targets/groups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addGroupTargetToRole(userId, roleId, groupId, callback)</td>
    <td style="padding:15px">Success</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/roles/{pathv2}/targets/groups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearUserSessions(userId, oauthTokens, callback)</td>
    <td style="padding:15px">Removes all active identity provider sessions. This forces the user to authenticate on the next ope</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFactors(userId, callback)</td>
    <td style="padding:15px">Enumerates all the enrolled factors for the specified user</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/factors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enrollFactor(userId, body, updatePhone, templateId, tokenLifetimeSeconds, activate, callback)</td>
    <td style="padding:15px">Enroll Factor</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/factors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSupportedFactors(userId, callback)</td>
    <td style="padding:15px">Enumerates all the supported factors that can be enrolled for the specified user</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/factors/catalog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSupportedSecurityQuestions(userId, callback)</td>
    <td style="padding:15px">Enumerates all available security questions for a user's `question` factor</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/factors/questions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFactor(userId, factorId, callback)</td>
    <td style="padding:15px">Unenrolls an existing factor for the specified user, allowing the user to enroll a new factor.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/factors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFactor(userId, factorId, callback)</td>
    <td style="padding:15px">Fetches a factor for the specified user</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/factors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateFactor(userId, factorId, body, callback)</td>
    <td style="padding:15px">Activate Factor</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/factors/{pathv2}/lifecycle/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFactorTransactionStatus(userId, factorId, transactionId, callback)</td>
    <td style="padding:15px">Polls factors verification transaction for status.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/factors/{pathv2}/transactions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyFactor(userId, factorId, templateId, tokenLifetimeSeconds, body, callback)</td>
    <td style="padding:15px">Verify MFA Factor</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/users/{pathv1}/factors/{pathv2}/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworkZones(after, limit, filter, callback)</td>
    <td style="padding:15px">List Network Zones</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkZone(zone, callback)</td>
    <td style="padding:15px">Add Network Zone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkZone(zoneId, callback)</td>
    <td style="padding:15px">Delete Network Zone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkZone(zoneId, callback)</td>
    <td style="padding:15px">Get Network Zone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkZone(zoneId, zone, callback)</td>
    <td style="padding:15px">Update Network Zone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateNetworkZone(zoneId, callback)</td>
    <td style="padding:15px">Activate Network Zone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/zones/{pathv1}/lifecycle/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateNetworkZone(zoneId, callback)</td>
    <td style="padding:15px">Deactivate Network Zone</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/zones/{pathv1}/lifecycle/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
