# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Okta System. The API that was used to build the adapter for Okta is usually available in the report directory of this adapter. The adapter utilizes the Okta API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Okta adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Okta in order to enable secure authentication and authorization processes. With this adapter you have the ability to perform operations on items such as:

- Identity Provider
- User
- Policy

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
