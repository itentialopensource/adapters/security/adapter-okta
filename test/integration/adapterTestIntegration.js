/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-okta',
      type: 'Okta',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Okta = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Okta Adapter Test', () => {
  describe('Okta Class Tests', () => {
    const a = new Okta(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    let applicationAppId = 'fakedata';
    let applicationCsrId = 'fakedata';
    let applicationKeyId = 'fakedata';
    let applicationGrantId = 'fakedata';
    let applicationGroupId = 'fakedata';
    let applicationTokenId = 'fakedata';
    const applicationCreateApplicationBodyParam = {
      _embedded: {},
      _links: {},
      accessibility: {
        errorRedirectUrl: 'string',
        loginRedirectUrl: 'string',
        selfService: true
      },
      created: 'string',
      credentials: {
        signing: {
          kid: 'string',
          lastRotated: 'string',
          nextRotation: 'string',
          rotationMode: 'string',
          use: 'sig'
        },
        userNameTemplate: {
          suffix: 'string',
          template: 'string',
          type: 'string'
        }
      },
      features: [
        'string'
      ],
      id: 'string',
      label: 'string',
      lastUpdated: 'string',
      licensing: {
        seatCount: 1
      },
      name: 'string',
      profile: {},
      settings: {
        app: {},
        implicitAssignment: false,
        inlineHookId: 'string',
        notifications: {
          vpn: {
            helpUrl: 'string',
            message: 'string',
            network: {}
          }
        }
      },
      signOnMode: 'AUTO_LOGIN',
      status: 'DELETED',
      visibility: {
        appLinks: {},
        autoSubmitToolbar: false,
        hide: {
          iOS: true,
          web: false
        }
      }
    };
    describe('#createApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createApplication(applicationCreateApplicationBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.accessibility);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.credentials);
                assert.equal(true, Array.isArray(data.response.features));
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.label);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('object', typeof data.response.licensing);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.profile);
                assert.equal('object', typeof data.response.settings);
                assert.equal('SAML_2_0', data.response.signOnMode);
                assert.equal('DELETED', data.response.status);
                assert.equal('object', typeof data.response.visibility);
              } else {
                runCommonAsserts(data, error);
              }
              applicationAppId = data.response.id;
              applicationCsrId = data.response.id;
              applicationKeyId = data.response.id;
              applicationGrantId = data.response.id;
              applicationGroupId = data.response.id;
              applicationTokenId = data.response.id;
              saveMockData('Application', 'createApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationGenerateCsrForApplicationBodyParam = {
      subject: {
        commonName: 'string',
        countryName: 'string',
        localityName: 'string',
        organizationName: 'string',
        organizationalUnitName: 'string',
        stateOrProvinceName: 'string'
      },
      subjectAltNames: {
        dnsNames: [
          'string'
        ]
      }
    };
    describe('#generateCsrForApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.generateCsrForApplication(applicationAppId, applicationGenerateCsrForApplicationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.csr);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.kty);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'generateCsrForApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postApiV1AppsAppIdCredentialsCsrsCsrIdLifecyclePublish - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postApiV1AppsAppIdCredentialsCsrsCsrIdLifecyclePublish(applicationAppId, applicationCsrId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.alg);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.e);
                assert.equal('string', data.response.expiresAt);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('string', data.response.kid);
                assert.equal('string', data.response.kty);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.n);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.use);
                assert.equal(true, Array.isArray(data.response.x5c));
                assert.equal('string', data.response.x5t);
                assert.equal('string', data.response['x5t#S256']);
                assert.equal('string', data.response.x5u);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'postApiV1AppsAppIdCredentialsCsrsCsrIdLifecyclePublish', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateApplicationKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.generateApplicationKey(applicationAppId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.alg);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.e);
                assert.equal('string', data.response.expiresAt);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('string', data.response.kid);
                assert.equal('string', data.response.kty);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.n);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.use);
                assert.equal(true, Array.isArray(data.response.x5c));
                assert.equal('string', data.response.x5t);
                assert.equal('string', data.response['x5t#S256']);
                assert.equal('string', data.response.x5u);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'generateApplicationKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationTargetAid = 'fakedata';
    describe('#cloneApplicationKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cloneApplicationKey(applicationAppId, applicationKeyId, applicationTargetAid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.alg);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.e);
                assert.equal('string', data.response.expiresAt);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('string', data.response.kid);
                assert.equal('string', data.response.kty);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.n);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.use);
                assert.equal(true, Array.isArray(data.response.x5c));
                assert.equal('string', data.response.x5t);
                assert.equal('string', data.response['x5t#S256']);
                assert.equal('string', data.response.x5u);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'cloneApplicationKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let applicationUserId = 'fakedata';
    const applicationGrantConsentToScopeBodyParam = {
      _embedded: {},
      _links: {},
      clientId: 'string',
      created: 'string',
      createdBy: {
        id: 'string',
        type: 'string'
      },
      id: 'string',
      issuer: 'string',
      lastUpdated: 'string',
      scopeId: 'string',
      source: 'ADMIN',
      status: 'ACTIVE',
      userId: 'string'
    };
    describe('#grantConsentToScope - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.grantConsentToScope(applicationAppId, applicationGrantConsentToScopeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.clientId);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issuer);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.scopeId);
                assert.equal('ADMIN', data.response.source);
                assert.equal('REVOKED', data.response.status);
                assert.equal('string', data.response.userId);
              } else {
                runCommonAsserts(data, error);
              }
              applicationUserId = data.response.userId;
              saveMockData('Application', 'grantConsentToScope', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activateApplication(applicationAppId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'activateApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deactivateApplication(applicationAppId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'deactivateApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationAssignUserToApplicationBodyParam = {
      _embedded: {},
      _links: {},
      created: 'string',
      credentials: {
        password: {
          value: 'string'
        },
        userName: 'string'
      },
      externalId: 'string',
      id: 'string',
      lastSync: 'string',
      lastUpdated: 'string',
      passwordChanged: 'string',
      profile: {},
      scope: 'string',
      status: 'string',
      statusChanged: 'string',
      syncState: 'string'
    };
    describe('#assignUserToApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.assignUserToApplication(applicationAppId, applicationAssignUserToApplicationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.credentials);
                assert.equal('string', data.response.externalId);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastSync);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.passwordChanged);
                assert.equal('object', typeof data.response.profile);
                assert.equal('string', data.response.scope);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.statusChanged);
                assert.equal('string', data.response.syncState);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'assignUserToApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationUpdateApplicationUserBodyParam = {
      _embedded: {},
      _links: {},
      created: 'string',
      credentials: {
        password: {
          value: 'string'
        },
        userName: 'string'
      },
      externalId: 'string',
      id: 'string',
      lastSync: 'string',
      lastUpdated: 'string',
      passwordChanged: 'string',
      profile: {},
      scope: 'string',
      status: 'string',
      statusChanged: 'string',
      syncState: 'string'
    };
    describe('#updateApplicationUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateApplicationUser(applicationAppId, applicationUserId, applicationUpdateApplicationUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.credentials);
                assert.equal('string', data.response.externalId);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastSync);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.passwordChanged);
                assert.equal('object', typeof data.response.profile);
                assert.equal('string', data.response.scope);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.statusChanged);
                assert.equal('string', data.response.syncState);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'updateApplicationUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApplications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listApplications(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'listApplications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationUpdateApplicationBodyParam = {
      _embedded: {},
      _links: {},
      accessibility: {
        errorRedirectUrl: 'string',
        loginRedirectUrl: 'string',
        selfService: false
      },
      created: 'string',
      credentials: {
        signing: {
          kid: 'string',
          lastRotated: 'string',
          nextRotation: 'string',
          rotationMode: 'string',
          use: 'sig'
        },
        userNameTemplate: {
          suffix: 'string',
          template: 'string',
          type: 'string'
        }
      },
      features: [
        'string'
      ],
      id: 'string',
      label: 'string',
      lastUpdated: 'string',
      licensing: {
        seatCount: 6
      },
      name: 'string',
      profile: {},
      settings: {
        app: {},
        implicitAssignment: true,
        inlineHookId: 'string',
        notifications: {
          vpn: {
            helpUrl: 'string',
            message: 'string',
            network: {}
          }
        }
      },
      signOnMode: 'SAML_1_1',
      status: 'INACTIVE',
      visibility: {
        appLinks: {},
        autoSubmitToolbar: true,
        hide: {
          iOS: true,
          web: true
        }
      }
    };
    describe('#updateApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateApplication(applicationAppId, applicationUpdateApplicationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'updateApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplication(applicationAppId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.accessibility);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.credentials);
                assert.equal(true, Array.isArray(data.response.features));
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.label);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('object', typeof data.response.licensing);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.profile);
                assert.equal('object', typeof data.response.settings);
                assert.equal('SAML_2_0', data.response.signOnMode);
                assert.equal('INACTIVE', data.response.status);
                assert.equal('object', typeof data.response.visibility);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCsrsForApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listCsrsForApplication(applicationAppId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'listCsrsForApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsrForApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCsrForApplication(applicationAppId, applicationCsrId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.csr);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.kty);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getCsrForApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApplicationKeys - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listApplicationKeys(applicationAppId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'listApplicationKeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationKey(applicationAppId, applicationKeyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.alg);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.e);
                assert.equal('string', data.response.expiresAt);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('string', data.response.kid);
                assert.equal('string', data.response.kty);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.n);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.use);
                assert.equal(true, Array.isArray(data.response.x5c));
                assert.equal('string', data.response.x5t);
                assert.equal('string', data.response['x5t#S256']);
                assert.equal('string', data.response.x5u);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getApplicationKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listScopeConsentGrants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listScopeConsentGrants(applicationAppId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'listScopeConsentGrants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScopeConsentGrant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScopeConsentGrant(applicationAppId, applicationGrantId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.clientId);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issuer);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.scopeId);
                assert.equal('ADMIN', data.response.source);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('string', data.response.userId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getScopeConsentGrant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApplicationGroupAssignments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listApplicationGroupAssignments(applicationAppId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'listApplicationGroupAssignments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationCreateApplicationGroupAssignmentBodyParam = {
      _embedded: {},
      _links: {},
      id: 'string',
      lastUpdated: 'string',
      priority: 4,
      profile: {}
    };
    describe('#createApplicationGroupAssignment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createApplicationGroupAssignment(applicationAppId, applicationGroupId, applicationCreateApplicationGroupAssignmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'createApplicationGroupAssignment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationGroupAssignment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationGroupAssignment(applicationAppId, applicationGroupId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(3, data.response.priority);
                assert.equal('object', typeof data.response.profile);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getApplicationGroupAssignment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listOAuth2TokensForApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listOAuth2TokensForApplication(applicationAppId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'listOAuth2TokensForApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOAuth2TokenForApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOAuth2TokenForApplication(applicationAppId, applicationTokenId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.clientId);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.expiresAt);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issuer);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(true, Array.isArray(data.response.scopes));
                assert.equal('ACTIVE', data.response.status);
                assert.equal('string', data.response.userId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getOAuth2TokenForApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApplicationUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listApplicationUsers(applicationAppId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'listApplicationUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationUser(applicationAppId, applicationUserId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.credentials);
                assert.equal('string', data.response.externalId);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastSync);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.passwordChanged);
                assert.equal('object', typeof data.response.profile);
                assert.equal('string', data.response.scope);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.statusChanged);
                assert.equal('string', data.response.syncState);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getApplicationUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let authorizationServerPolicyId = 'fakedata';
    let authorizationServerRuleId = 'fakedata';
    let authorizationServerClaimId = 'fakedata';
    let authorizationServerClientId = 'fakedata';
    let authorizationServerTokenId = 'fakedata';
    let authorizationServerScopeId = 'fakedata';
    const authorizationServerCreateAuthorizationServerBodyParam = {
      _links: {},
      audiences: [
        'string'
      ],
      created: 'string',
      credentials: {
        signing: {
          kid: 'string',
          lastRotated: 'string',
          nextRotation: 'string',
          rotationMode: 'MANUAL',
          use: 'sig'
        }
      },
      description: 'string',
      id: 'string',
      issuer: 'string',
      issuerMode: 'CUSTOM_URL',
      lastUpdated: 'string',
      name: 'string',
      status: 'INACTIVE'
    };
    describe('#createAuthorizationServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAuthorizationServer(authorizationServerCreateAuthorizationServerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal(true, Array.isArray(data.response.audiences));
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.credentials);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issuer);
                assert.equal('ORG_URL', data.response.issuerMode);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('ACTIVE', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              authorizationServerPolicyId = data.response.id;
              authorizationServerRuleId = data.response.id;
              authorizationServerClaimId = data.response.id;
              authorizationServerClientId = data.response.id;
              authorizationServerTokenId = data.response.id;
              authorizationServerScopeId = data.response.id;
              saveMockData('AuthorizationServer', 'createAuthorizationServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizationServerAuthServerId = 'fakedata';
    const authorizationServerCreateOAuth2ClaimBodyParam = {
      _links: {},
      alwaysIncludeInToken: true,
      claimType: 'RESOURCE',
      conditions: {
        scopes: [
          'string'
        ]
      },
      group_filter_type: 'REGEX',
      id: 'string',
      name: 'string',
      status: 'ACTIVE',
      system: false,
      value: 'string',
      valueType: 'SYSTEM'
    };
    describe('#createOAuth2Claim - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOAuth2Claim(authorizationServerAuthServerId, authorizationServerCreateOAuth2ClaimBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal(false, data.response.alwaysIncludeInToken);
                assert.equal('RESOURCE', data.response.claimType);
                assert.equal('object', typeof data.response.conditions);
                assert.equal('REGEX', data.response.group_filter_type);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('INACTIVE', data.response.status);
                assert.equal(false, data.response.system);
                assert.equal('string', data.response.value);
                assert.equal('GROUPS', data.response.valueType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'createOAuth2Claim', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizationServerRotateAuthorizationServerKeysBodyParam = {
      use: 'sig'
    };
    describe('#rotateAuthorizationServerKeys - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rotateAuthorizationServerKeys(authorizationServerAuthServerId, authorizationServerRotateAuthorizationServerKeysBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'rotateAuthorizationServerKeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateAuthorizationServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activateAuthorizationServer(authorizationServerAuthServerId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'activateAuthorizationServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateAuthorizationServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deactivateAuthorizationServer(authorizationServerAuthServerId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'deactivateAuthorizationServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizationServerCreateAuthorizationServerPolicyBodyParam = {
      _embedded: {},
      _links: {},
      conditions: {
        app: {
          exclude: [
            {
              id: 'string',
              name: 'string',
              type: 'APP_TYPE'
            }
          ],
          include: [
            {
              id: 'string',
              name: 'string',
              type: 'APP_TYPE'
            }
          ]
        },
        apps: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        authContext: {
          authType: 'ANY'
        },
        authProvider: {
          include: [
            'string'
          ],
          provider: 'ANY'
        },
        beforeScheduledAction: {
          duration: {
            number: 6,
            unit: 'string'
          },
          lifecycleAction: {
            status: 'EXPIRED_PASSWORD'
          }
        },
        clients: {
          include: [
            'string'
          ]
        },
        context: {
          expression: 'string'
        },
        device: {
          migrated: false,
          platform: {
            supportedMDMFrameworks: [
              'SAFE'
            ],
            types: [
              'IOS'
            ]
          },
          rooted: false,
          trustLevel: 'TRUSTED'
        },
        grantTypes: {
          include: [
            'string'
          ]
        },
        groups: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        identityProvider: {
          idpIds: [
            'string'
          ],
          provider: 'OKTA'
        },
        mdmEnrollment: {
          blockNonSafeAndroid: true,
          enrollment: 'OMM'
        },
        network: {
          connection: 'ANYWHERE',
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        people: {
          groups: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          },
          users: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          }
        },
        platform: {
          exclude: [
            {
              os: {},
              type: 'OTHER'
            }
          ],
          include: [
            {
              os: {},
              type: 'OTHER'
            }
          ]
        },
        risk: {
          behaviors: [
            'string'
          ]
        },
        riskScore: {
          level: 'string'
        },
        scopes: {
          include: [
            'string'
          ]
        },
        userIdentifier: {
          attribute: 'string',
          patterns: [
            {
              matchType: 'EQUALS',
              value: 'string'
            }
          ],
          type: 'ATTRIBUTE'
        },
        userStatus: {
          value: 'INACTIVE'
        },
        users: {
          exclude: [
            'string'
          ],
          inactivity: {
            number: 7,
            unit: 'string'
          },
          include: [
            'string'
          ],
          lifecycleExpiration: {
            lifecycleStatus: 'string',
            number: 9,
            unit: 'string'
          },
          passwordExpiration: {
            number: 10,
            unit: 'string'
          },
          userLifecycleAttribute: {
            attributeName: 'string',
            matchingValue: 'string'
          }
        }
      },
      created: 'string',
      description: 'string',
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      priority: 3,
      status: 'ACTIVE',
      system: false,
      type: 'IDP_DISCOVERY'
    };
    describe('#createAuthorizationServerPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAuthorizationServerPolicy(authorizationServerAuthServerId, authorizationServerCreateAuthorizationServerPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.conditions);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal(7, data.response.priority);
                assert.equal('INACTIVE', data.response.status);
                assert.equal(false, data.response.system);
                assert.equal('OAUTH_AUTHORIZATION_POLICY', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'createAuthorizationServerPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateAuthorizationServerPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activateAuthorizationServerPolicy(authorizationServerAuthServerId, authorizationServerPolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'activateAuthorizationServerPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateAuthorizationServerPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deactivateAuthorizationServerPolicy(authorizationServerAuthServerId, authorizationServerPolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'deactivateAuthorizationServerPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizationServerCreateAuthorizationServerPolicyRuleBodyParam = {
      actions: {
        token: {
          accessTokenLifetimeMinutes: 5,
          refreshTokenLifetimeMinutes: 2,
          refreshTokenWindowMinutes: 4
        }
      },
      conditions: {
        clients: {
          include: [
            'string'
          ]
        },
        grantTypes: {
          include: [
            'string'
          ]
        },
        people: {
          groups: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          },
          users: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          }
        },
        scopes: {
          include: [
            'string'
          ]
        }
      },
      created: 'string',
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      priority: 4,
      status: 'ACTIVE',
      system: true,
      type: 'RESOURCE_ACCESS'
    };
    describe('#createAuthorizationServerPolicyRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAuthorizationServerPolicyRule(authorizationServerPolicyId, authorizationServerAuthServerId, authorizationServerCreateAuthorizationServerPolicyRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.actions);
                assert.equal('object', typeof data.response.conditions);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.priority);
                assert.equal('ACTIVE', data.response.status);
                assert.equal(true, data.response.system);
                assert.equal('RESOURCE_ACCESS', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'createAuthorizationServerPolicyRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateAuthorizationServerPolicyRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activateAuthorizationServerPolicyRule(authorizationServerAuthServerId, authorizationServerPolicyId, authorizationServerRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'activateAuthorizationServerPolicyRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateAuthorizationServerPolicyRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deactivateAuthorizationServerPolicyRule(authorizationServerAuthServerId, authorizationServerPolicyId, authorizationServerRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'deactivateAuthorizationServerPolicyRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizationServerCreateOAuth2ScopeBodyParam = {
      consent: 'IMPLICIT',
      default: true,
      description: 'string',
      displayName: 'string',
      id: 'string',
      metadataPublish: 'NO_CLIENTS',
      name: 'string',
      system: true
    };
    describe('#createOAuth2Scope - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOAuth2Scope(authorizationServerAuthServerId, authorizationServerCreateOAuth2ScopeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('REQUIRED', data.response.consent);
                assert.equal(false, data.response.default);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.id);
                assert.equal('NO_CLIENTS', data.response.metadataPublish);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.system);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'createOAuth2Scope', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthorizationServers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAuthorizationServers(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'listAuthorizationServers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizationServerUpdateAuthorizationServerBodyParam = {
      _links: {},
      audiences: [
        'string'
      ],
      created: 'string',
      credentials: {
        signing: {
          kid: 'string',
          lastRotated: 'string',
          nextRotation: 'string',
          rotationMode: 'MANUAL',
          use: 'sig'
        }
      },
      description: 'string',
      id: 'string',
      issuer: 'string',
      issuerMode: 'CUSTOM_URL',
      lastUpdated: 'string',
      name: 'string',
      status: 'ACTIVE'
    };
    describe('#updateAuthorizationServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAuthorizationServer(authorizationServerAuthServerId, authorizationServerUpdateAuthorizationServerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'updateAuthorizationServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthorizationServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuthorizationServer(authorizationServerAuthServerId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal(true, Array.isArray(data.response.audiences));
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.credentials);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issuer);
                assert.equal('ORG_URL', data.response.issuerMode);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('INACTIVE', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'getAuthorizationServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listOAuth2Claims - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listOAuth2Claims(authorizationServerAuthServerId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'listOAuth2Claims', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizationServerUpdateOAuth2ClaimBodyParam = {
      _links: {},
      alwaysIncludeInToken: true,
      claimType: 'IDENTITY',
      conditions: {
        scopes: [
          'string'
        ]
      },
      group_filter_type: 'CONTAINS',
      id: 'string',
      name: 'string',
      status: 'ACTIVE',
      system: true,
      value: 'string',
      valueType: 'EXPRESSION'
    };
    describe('#updateOAuth2Claim - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOAuth2Claim(authorizationServerAuthServerId, authorizationServerClaimId, authorizationServerUpdateOAuth2ClaimBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'updateOAuth2Claim', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOAuth2Claim - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOAuth2Claim(authorizationServerAuthServerId, authorizationServerClaimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal(true, data.response.alwaysIncludeInToken);
                assert.equal('RESOURCE', data.response.claimType);
                assert.equal('object', typeof data.response.conditions);
                assert.equal('CONTAINS', data.response.group_filter_type);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('INACTIVE', data.response.status);
                assert.equal(true, data.response.system);
                assert.equal('string', data.response.value);
                assert.equal('GROUPS', data.response.valueType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'getOAuth2Claim', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listOAuth2ClientsForAuthorizationServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listOAuth2ClientsForAuthorizationServer(authorizationServerAuthServerId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'listOAuth2ClientsForAuthorizationServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRefreshTokensForAuthorizationServerAndClient - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listRefreshTokensForAuthorizationServerAndClient(authorizationServerAuthServerId, authorizationServerClientId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'listRefreshTokensForAuthorizationServerAndClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRefreshTokenForAuthorizationServerAndClient - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRefreshTokenForAuthorizationServerAndClient(authorizationServerAuthServerId, authorizationServerClientId, authorizationServerTokenId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.clientId);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('string', data.response.expiresAt);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issuer);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(true, Array.isArray(data.response.scopes));
                assert.equal('ACTIVE', data.response.status);
                assert.equal('string', data.response.userId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'getRefreshTokenForAuthorizationServerAndClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthorizationServerKeys - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAuthorizationServerKeys(authorizationServerAuthServerId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'listAuthorizationServerKeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthorizationServerPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAuthorizationServerPolicies(authorizationServerAuthServerId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'listAuthorizationServerPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizationServerUpdateAuthorizationServerPolicyBodyParam = {
      _embedded: {},
      _links: {},
      conditions: {
        app: {
          exclude: [
            {
              id: 'string',
              name: 'string',
              type: 'APP_TYPE'
            }
          ],
          include: [
            {
              id: 'string',
              name: 'string',
              type: 'APP'
            }
          ]
        },
        apps: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        authContext: {
          authType: 'RADIUS'
        },
        authProvider: {
          include: [
            'string'
          ],
          provider: 'LDAP'
        },
        beforeScheduledAction: {
          duration: {
            number: 8,
            unit: 'string'
          },
          lifecycleAction: {
            status: 'PENDING'
          }
        },
        clients: {
          include: [
            'string'
          ]
        },
        context: {
          expression: 'string'
        },
        device: {
          migrated: true,
          platform: {
            supportedMDMFrameworks: [
              'AFW'
            ],
            types: [
              'WINDOWS'
            ]
          },
          rooted: true,
          trustLevel: 'ANY'
        },
        grantTypes: {
          include: [
            'string'
          ]
        },
        groups: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        identityProvider: {
          idpIds: [
            'string'
          ],
          provider: 'SPECIFIC_IDP'
        },
        mdmEnrollment: {
          blockNonSafeAndroid: true,
          enrollment: 'OMM'
        },
        network: {
          connection: 'ANYWHERE',
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        people: {
          groups: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          },
          users: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          }
        },
        platform: {
          exclude: [
            {
              os: {},
              type: 'DESKTOP'
            }
          ],
          include: [
            {
              os: {},
              type: 'ANY'
            }
          ]
        },
        risk: {
          behaviors: [
            'string'
          ]
        },
        riskScore: {
          level: 'string'
        },
        scopes: {
          include: [
            'string'
          ]
        },
        userIdentifier: {
          attribute: 'string',
          patterns: [
            {
              matchType: 'EQUALS',
              value: 'string'
            }
          ],
          type: 'ATTRIBUTE'
        },
        userStatus: {
          value: 'DELETED'
        },
        users: {
          exclude: [
            'string'
          ],
          inactivity: {
            number: 7,
            unit: 'string'
          },
          include: [
            'string'
          ],
          lifecycleExpiration: {
            lifecycleStatus: 'string',
            number: 10,
            unit: 'string'
          },
          passwordExpiration: {
            number: 3,
            unit: 'string'
          },
          userLifecycleAttribute: {
            attributeName: 'string',
            matchingValue: 'string'
          }
        }
      },
      created: 'string',
      description: 'string',
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      priority: 9,
      status: 'INACTIVE',
      system: true,
      type: 'IDP_DISCOVERY'
    };
    describe('#updateAuthorizationServerPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAuthorizationServerPolicy(authorizationServerAuthServerId, authorizationServerPolicyId, authorizationServerUpdateAuthorizationServerPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'updateAuthorizationServerPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthorizationServerPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuthorizationServerPolicy(authorizationServerAuthServerId, authorizationServerPolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.conditions);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal(10, data.response.priority);
                assert.equal('INACTIVE', data.response.status);
                assert.equal(false, data.response.system);
                assert.equal('IDP_DISCOVERY', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'getAuthorizationServerPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthorizationServerPolicyRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAuthorizationServerPolicyRules(authorizationServerPolicyId, authorizationServerAuthServerId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'listAuthorizationServerPolicyRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizationServerUpdateAuthorizationServerPolicyRuleBodyParam = {
      actions: {
        token: {
          accessTokenLifetimeMinutes: 5,
          refreshTokenLifetimeMinutes: 8,
          refreshTokenWindowMinutes: 5
        }
      },
      conditions: {
        clients: {
          include: [
            'string'
          ]
        },
        grantTypes: {
          include: [
            'string'
          ]
        },
        people: {
          groups: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          },
          users: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          }
        },
        scopes: {
          include: [
            'string'
          ]
        }
      },
      created: 'string',
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      priority: 7,
      status: 'ACTIVE',
      system: true,
      type: 'RESOURCE_ACCESS'
    };
    describe('#updateAuthorizationServerPolicyRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAuthorizationServerPolicyRule(authorizationServerPolicyId, authorizationServerAuthServerId, authorizationServerRuleId, authorizationServerUpdateAuthorizationServerPolicyRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'updateAuthorizationServerPolicyRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthorizationServerPolicyRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuthorizationServerPolicyRule(authorizationServerPolicyId, authorizationServerAuthServerId, authorizationServerRuleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.actions);
                assert.equal('object', typeof data.response.conditions);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.priority);
                assert.equal('ACTIVE', data.response.status);
                assert.equal(true, data.response.system);
                assert.equal('RESOURCE_ACCESS', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'getAuthorizationServerPolicyRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listOAuth2Scopes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listOAuth2Scopes(authorizationServerAuthServerId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'listOAuth2Scopes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizationServerUpdateOAuth2ScopeBodyParam = {
      consent: 'ADMIN',
      default: true,
      description: 'string',
      displayName: 'string',
      id: 'string',
      metadataPublish: 'ALL_CLIENTS',
      name: 'string',
      system: false
    };
    describe('#updateOAuth2Scope - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOAuth2Scope(authorizationServerAuthServerId, authorizationServerScopeId, authorizationServerUpdateOAuth2ScopeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'updateOAuth2Scope', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOAuth2Scope - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOAuth2Scope(authorizationServerAuthServerId, authorizationServerScopeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('ADMIN', data.response.consent);
                assert.equal(true, data.response.default);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.id);
                assert.equal('NO_CLIENTS', data.response.metadataPublish);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.system);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'getOAuth2Scope', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventHookCreateEventHookBodyParam = {
      _links: {},
      channel: {
        config: {
          authScheme: {
            key: 'string',
            type: {},
            value: 'string'
          },
          headers: [
            {
              key: 'string',
              value: 'string'
            }
          ],
          uri: 'string'
        },
        type: 'HTTP',
        version: 'string'
      },
      created: 'string',
      createdBy: 'string',
      events: {
        items: [
          'string'
        ],
        type: 'FLOW_EVENT'
      },
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      status: 'INACTIVE',
      verificationStatus: 'UNVERIFIED'
    };
    describe('#createEventHook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createEventHook(eventHookCreateEventHookBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.channel);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.createdBy);
                assert.equal('object', typeof data.response.events);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('INACTIVE', data.response.status);
                assert.equal('UNVERIFIED', data.response.verificationStatus);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventHook', 'createEventHook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventHookEventHookId = 'fakedata';
    describe('#activateEventHook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.activateEventHook(eventHookEventHookId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.channel);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.createdBy);
                assert.equal('object', typeof data.response.events);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('VERIFIED', data.response.verificationStatus);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventHook', 'activateEventHook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateEventHook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deactivateEventHook(eventHookEventHookId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.channel);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.createdBy);
                assert.equal('object', typeof data.response.events);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('VERIFIED', data.response.verificationStatus);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventHook', 'deactivateEventHook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verifyEventHook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.verifyEventHook(eventHookEventHookId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.channel);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.createdBy);
                assert.equal('object', typeof data.response.events);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('UNVERIFIED', data.response.verificationStatus);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventHook', 'verifyEventHook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listEventHooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listEventHooks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventHook', 'listEventHooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventHookUpdateEventHookBodyParam = {
      _links: {},
      channel: {
        config: {
          authScheme: {
            key: 'string',
            type: {},
            value: 'string'
          },
          headers: [
            {
              key: 'string',
              value: 'string'
            }
          ],
          uri: 'string'
        },
        type: 'HTTP',
        version: 'string'
      },
      created: 'string',
      createdBy: 'string',
      events: {
        items: [
          'string'
        ],
        type: 'FLOW_EVENT'
      },
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      status: 'INACTIVE',
      verificationStatus: 'VERIFIED'
    };
    describe('#updateEventHook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateEventHook(eventHookEventHookId, eventHookUpdateEventHookBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventHook', 'updateEventHook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventHook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEventHook(eventHookEventHookId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.channel);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.createdBy);
                assert.equal('object', typeof data.response.events);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('UNVERIFIED', data.response.verificationStatus);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventHook', 'getEventHook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let featureFeatureId = 'fakedata';
    const featureLifecycle = 'fakedata';
    describe('#updateFeatureLifecycle - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFeatureLifecycle(featureFeatureId, featureLifecycle, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.stage);
                assert.equal('DISABLED', data.response.status);
                assert.equal('self-service', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              featureFeatureId = data.response.id;
              saveMockData('Feature', 'updateFeatureLifecycle', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFeatures - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listFeatures((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Feature', 'listFeatures', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFeature - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFeature(featureFeatureId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.stage);
                assert.equal('ENABLED', data.response.status);
                assert.equal('self-service', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Feature', 'getFeature', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFeatureDependencies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listFeatureDependencies(featureFeatureId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Feature', 'listFeatureDependencies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFeatureDependents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listFeatureDependents(featureFeatureId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Feature', 'listFeatureDependents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let groupRuleId = 'fakedata';
    let groupGroupId = 'fakedata';
    let groupRoleId = 'fakedata';
    let groupApplicationId = 'fakedata';
    let groupUserId = 'fakedata';
    const groupCreateGroupBodyParam = {
      _embedded: {},
      _links: {},
      created: 'string',
      id: 'string',
      lastMembershipUpdated: 'string',
      lastUpdated: 'string',
      objectClass: [
        'string'
      ],
      profile: {
        description: 'string',
        name: 'string'
      },
      type: 'APP_GROUP'
    };
    describe('#createGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createGroup(groupCreateGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastMembershipUpdated);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(true, Array.isArray(data.response.objectClass));
                assert.equal('object', typeof data.response.profile);
                assert.equal('OKTA_GROUP', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              groupRuleId = data.response.id;
              groupGroupId = data.response.id;
              groupRoleId = data.response.id;
              groupApplicationId = data.response.id;
              groupUserId = data.response.id;
              saveMockData('Group', 'createGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupCreateGroupRuleBodyParam = {
      actions: {
        assignUserToGroups: {
          groupIds: [
            'string'
          ]
        }
      },
      conditions: {
        expression: {
          type: 'string',
          value: 'string'
        },
        people: {
          groups: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          },
          users: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          }
        }
      },
      created: 'string',
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      status: 'INACTIVE',
      type: 'string'
    };
    describe('#createGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createGroupRule(groupCreateGroupRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.actions);
                assert.equal('object', typeof data.response.conditions);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('INACTIVE', data.response.status);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'createGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateGroupRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activateGroupRule(groupRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'activateGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateGroupRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deactivateGroupRule(groupRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'deactivateGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupAssignRoleToGroupBodyParam = {
      type: 'SUPER_ADMIN'
    };
    describe('#assignRoleToGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.assignRoleToGroup(groupAssignRoleToGroupBodyParam, groupGroupId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('USER', data.response.assignmentType);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.label);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('INACTIVE', data.response.status);
                assert.equal('ORG_ADMIN', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'assignRoleToGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listGroups(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'listGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroupRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listGroupRules(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'listGroupRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupUpdateGroupRuleBodyParam = {
      actions: {
        assignUserToGroups: {
          groupIds: [
            'string'
          ]
        }
      },
      conditions: {
        expression: {
          type: 'string',
          value: 'string'
        },
        people: {
          groups: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          },
          users: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          }
        }
      },
      created: 'string',
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      status: 'INVALID',
      type: 'string'
    };
    describe('#updateGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGroupRule(groupRuleId, groupUpdateGroupRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'updateGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupRule(groupRuleId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.actions);
                assert.equal('object', typeof data.response.conditions);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('INVALID', data.response.status);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'getGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupUpdateGroupBodyParam = {
      _embedded: {},
      _links: {},
      created: 'string',
      id: 'string',
      lastMembershipUpdated: 'string',
      lastUpdated: 'string',
      objectClass: [
        'string'
      ],
      profile: {
        description: 'string',
        name: 'string'
      },
      type: 'BUILT_IN'
    };
    describe('#updateGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGroup(groupGroupId, groupUpdateGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'updateGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroup(groupGroupId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastMembershipUpdated);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(true, Array.isArray(data.response.objectClass));
                assert.equal('object', typeof data.response.profile);
                assert.equal('APP_GROUP', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'getGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAssignedApplicationsForGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAssignedApplicationsForGroup(groupGroupId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'listAssignedApplicationsForGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroupAssignedRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listGroupAssignedRoles(groupGroupId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'listGroupAssignedRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRole(groupGroupId, groupRoleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('USER', data.response.assignmentType);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.label);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('INACTIVE', data.response.status);
                assert.equal('HELP_DESK_ADMIN', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'getRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApplicationTargetsForApplicationAdministratorRoleForGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listApplicationTargetsForApplicationAdministratorRoleForGroup(groupGroupId, groupRoleId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'listApplicationTargetsForApplicationAdministratorRoleForGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupAppName = 'fakedata';
    describe('#addApplicationTargetToAdminRoleGivenToGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addApplicationTargetToAdminRoleGivenToGroup(groupGroupId, groupRoleId, groupAppName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'addApplicationTargetToAdminRoleGivenToGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addApplicationInstanceTargetToAppAdminRoleGivenToGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addApplicationInstanceTargetToAppAdminRoleGivenToGroup(groupGroupId, groupRoleId, groupAppName, groupApplicationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'addApplicationInstanceTargetToAppAdminRoleGivenToGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroupTargetsForGroupRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listGroupTargetsForGroupRole(groupGroupId, groupRoleId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'listGroupTargetsForGroupRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupTargetGroupId = 'fakedata';
    describe('#addGroupTargetToGroupAdministratorRoleForGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addGroupTargetToGroupAdministratorRoleForGroup(groupGroupId, groupRoleId, groupTargetGroupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'addGroupTargetToGroupAdministratorRoleForGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroupUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listGroupUsers(groupGroupId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'listGroupUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addUserToGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addUserToGroup(groupGroupId, groupUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'addUserToGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let identityProviderIdpId = 'fakedata';
    let identityProviderCsrId = 'fakedata';
    let identityProviderKeyId = 'fakedata';
    let identityProviderUserId = 'fakedata';
    const identityProviderCreateIdentityProviderBodyParam = {
      _links: {},
      created: 'string',
      id: 'string',
      issuerMode: 'ORG_URL',
      lastUpdated: 'string',
      name: 'string',
      policy: {
        accountLink: {
          action: 'DISABLED',
          filter: {
            groups: {}
          }
        },
        maxClockSkew: 9,
        provisioning: {
          action: 'CALLOUT',
          conditions: {
            deprovisioned: {},
            suspended: {}
          },
          groups: {
            action: 'ASSIGN',
            assignments: [
              'string'
            ],
            filter: [
              'string'
            ],
            sourceAttributeName: 'string'
          },
          profileMaster: false
        },
        subject: {
          filter: 'string',
          format: [
            'string'
          ],
          matchAttribute: 'string',
          matchType: 'EMAIL',
          userNameTemplate: {
            template: 'string'
          }
        }
      },
      protocol: {
        algorithms: {
          request: {
            signature: {}
          },
          response: {
            signature: {}
          }
        },
        credentials: {
          client: {
            client_id: 'string',
            client_secret: 'string'
          },
          signing: {
            kid: 'string'
          },
          trust: {
            audience: 'string',
            issuer: 'string',
            kid: 'string',
            revocation: 'OCSP',
            revocationCacheLifetime: 7
          }
        },
        endpoints: {
          acs: {
            binding: 'HTTP-REDIRECT',
            destination: 'string',
            type: 'ORG',
            url: 'string'
          },
          authorization: {
            binding: 'HTTP-POST',
            destination: 'string',
            type: 'ORG',
            url: 'string'
          },
          jwks: {
            binding: 'HTTP-POST',
            destination: 'string',
            type: 'INSTANCE',
            url: 'string'
          },
          metadata: {
            binding: 'HTTP-REDIRECT',
            destination: 'string',
            type: 'INSTANCE',
            url: 'string'
          },
          slo: {
            binding: 'HTTP-POST',
            destination: 'string',
            type: 'ORG',
            url: 'string'
          },
          sso: {
            binding: 'HTTP-REDIRECT',
            destination: 'string',
            type: 'INSTANCE',
            url: 'string'
          },
          token: {
            binding: 'HTTP-REDIRECT',
            destination: 'string',
            type: 'ORG',
            url: 'string'
          },
          userInfo: {
            binding: 'HTTP-POST',
            destination: 'string',
            type: 'INSTANCE',
            url: 'string'
          }
        },
        issuer: {
          binding: 'HTTP-REDIRECT',
          destination: 'string',
          type: 'ORG',
          url: 'string'
        },
        relayState: {
          format: 'OPAQUE'
        },
        scopes: [
          'string'
        ],
        settings: {
          nameFormat: 'string'
        },
        type: 'SAML2'
      },
      status: 'INACTIVE',
      type: 'LINKEDIN'
    };
    describe('#createIdentityProvider - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIdentityProvider(identityProviderCreateIdentityProviderBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('CUSTOM_URL_DOMAIN', data.response.issuerMode);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.policy);
                assert.equal('object', typeof data.response.protocol);
                assert.equal('INACTIVE', data.response.status);
                assert.equal('MICROSOFT', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              identityProviderIdpId = data.response.id;
              identityProviderCsrId = data.response.id;
              identityProviderKeyId = data.response.id;
              identityProviderUserId = data.response.id;
              saveMockData('IdentityProvider', 'createIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityProviderCreateIdentityProviderKeyBodyParam = {
      _links: {},
      alg: 'string',
      created: 'string',
      e: 'string',
      expiresAt: 'string',
      key_ops: [
        'string'
      ],
      kid: 'string',
      kty: 'string',
      lastUpdated: 'string',
      n: 'string',
      status: 'string',
      use: 'string',
      x5c: [
        'string'
      ],
      x5t: 'string',
      'x5t#S256': 'string',
      x5u: 'string'
    };
    describe('#createIdentityProviderKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIdentityProviderKey(identityProviderCreateIdentityProviderKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.alg);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.e);
                assert.equal('string', data.response.expiresAt);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('string', data.response.kid);
                assert.equal('string', data.response.kty);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.n);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.use);
                assert.equal(true, Array.isArray(data.response.x5c));
                assert.equal('string', data.response.x5t);
                assert.equal('string', data.response['x5t#S256']);
                assert.equal('string', data.response.x5u);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'createIdentityProviderKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityProviderGenerateCsrForIdentityProviderBodyParam = {
      subject: {
        commonName: 'string',
        countryName: 'string',
        localityName: 'string',
        organizationName: 'string',
        organizationalUnitName: 'string',
        stateOrProvinceName: 'string'
      },
      subjectAltNames: {
        dnsNames: [
          'string'
        ]
      }
    };
    describe('#generateCsrForIdentityProvider - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.generateCsrForIdentityProvider(identityProviderIdpId, identityProviderGenerateCsrForIdentityProviderBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.csr);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.kty);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'generateCsrForIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postApiV1IdpsIdpIdCredentialsCsrsCsrIdLifecyclePublish - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postApiV1IdpsIdpIdCredentialsCsrsCsrIdLifecyclePublish(identityProviderIdpId, identityProviderCsrId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.alg);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.e);
                assert.equal('string', data.response.expiresAt);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('string', data.response.kid);
                assert.equal('string', data.response.kty);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.n);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.use);
                assert.equal(true, Array.isArray(data.response.x5c));
                assert.equal('string', data.response.x5t);
                assert.equal('string', data.response['x5t#S256']);
                assert.equal('string', data.response.x5u);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'postApiV1IdpsIdpIdCredentialsCsrsCsrIdLifecyclePublish', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityProviderValidityYears = 555;
    describe('#generateIdentityProviderSigningKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.generateIdentityProviderSigningKey(identityProviderIdpId, identityProviderValidityYears, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.alg);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.e);
                assert.equal('string', data.response.expiresAt);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('string', data.response.kid);
                assert.equal('string', data.response.kty);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.n);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.use);
                assert.equal(true, Array.isArray(data.response.x5c));
                assert.equal('string', data.response.x5t);
                assert.equal('string', data.response['x5t#S256']);
                assert.equal('string', data.response.x5u);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'generateIdentityProviderSigningKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityProviderTargetIdpId = 'fakedata';
    describe('#cloneIdentityProviderKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cloneIdentityProviderKey(identityProviderIdpId, identityProviderKeyId, identityProviderTargetIdpId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.alg);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.e);
                assert.equal('string', data.response.expiresAt);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('string', data.response.kid);
                assert.equal('string', data.response.kty);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.n);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.use);
                assert.equal(true, Array.isArray(data.response.x5c));
                assert.equal('string', data.response.x5t);
                assert.equal('string', data.response['x5t#S256']);
                assert.equal('string', data.response.x5u);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'cloneIdentityProviderKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateIdentityProvider - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.activateIdentityProvider(identityProviderIdpId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('CUSTOM_URL_DOMAIN', data.response.issuerMode);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.policy);
                assert.equal('object', typeof data.response.protocol);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('X509', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'activateIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateIdentityProvider - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deactivateIdentityProvider(identityProviderIdpId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('CUSTOM_URL_DOMAIN', data.response.issuerMode);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.policy);
                assert.equal('object', typeof data.response.protocol);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('SAML2', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'deactivateIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityProviderLinkUserToIdentityProviderBodyParam = {
      externalId: 'string'
    };
    describe('#linkUserToIdentityProvider - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.linkUserToIdentityProvider(identityProviderIdpId, identityProviderUserId, identityProviderLinkUserToIdentityProviderBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.externalId);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('object', typeof data.response.profile);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'linkUserToIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIdentityProviders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listIdentityProviders(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'listIdentityProviders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIdentityProviderKeys - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listIdentityProviderKeys(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'listIdentityProviderKeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityProviderKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIdentityProviderKey(identityProviderKeyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.alg);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.e);
                assert.equal('string', data.response.expiresAt);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('string', data.response.kid);
                assert.equal('string', data.response.kty);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.n);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.use);
                assert.equal(true, Array.isArray(data.response.x5c));
                assert.equal('string', data.response.x5t);
                assert.equal('string', data.response['x5t#S256']);
                assert.equal('string', data.response.x5u);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'getIdentityProviderKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityProviderUpdateIdentityProviderBodyParam = {
      _links: {},
      created: 'string',
      id: 'string',
      issuerMode: 'CUSTOM_URL_DOMAIN',
      lastUpdated: 'string',
      name: 'string',
      policy: {
        accountLink: {
          action: 'DISABLED',
          filter: {
            groups: {}
          }
        },
        maxClockSkew: 2,
        provisioning: {
          action: 'DISABLED',
          conditions: {
            deprovisioned: {},
            suspended: {}
          },
          groups: {
            action: 'SYNC',
            assignments: [
              'string'
            ],
            filter: [
              'string'
            ],
            sourceAttributeName: 'string'
          },
          profileMaster: false
        },
        subject: {
          filter: 'string',
          format: [
            'string'
          ],
          matchAttribute: 'string',
          matchType: 'USERNAME',
          userNameTemplate: {
            template: 'string'
          }
        }
      },
      protocol: {
        algorithms: {
          request: {
            signature: {}
          },
          response: {
            signature: {}
          }
        },
        credentials: {
          client: {
            client_id: 'string',
            client_secret: 'string'
          },
          signing: {
            kid: 'string'
          },
          trust: {
            audience: 'string',
            issuer: 'string',
            kid: 'string',
            revocation: 'CRL',
            revocationCacheLifetime: 3
          }
        },
        endpoints: {
          acs: {
            binding: 'HTTP-REDIRECT',
            destination: 'string',
            type: 'INSTANCE',
            url: 'string'
          },
          authorization: {
            binding: 'HTTP-REDIRECT',
            destination: 'string',
            type: 'ORG',
            url: 'string'
          },
          jwks: {
            binding: 'HTTP-POST',
            destination: 'string',
            type: 'ORG',
            url: 'string'
          },
          metadata: {
            binding: 'HTTP-POST',
            destination: 'string',
            type: 'ORG',
            url: 'string'
          },
          slo: {
            binding: 'HTTP-POST',
            destination: 'string',
            type: 'INSTANCE',
            url: 'string'
          },
          sso: {
            binding: 'HTTP-POST',
            destination: 'string',
            type: 'ORG',
            url: 'string'
          },
          token: {
            binding: 'HTTP-REDIRECT',
            destination: 'string',
            type: 'ORG',
            url: 'string'
          },
          userInfo: {
            binding: 'HTTP-REDIRECT',
            destination: 'string',
            type: 'ORG',
            url: 'string'
          }
        },
        issuer: {
          binding: 'HTTP-REDIRECT',
          destination: 'string',
          type: 'ORG',
          url: 'string'
        },
        relayState: {
          format: 'OPAQUE'
        },
        scopes: [
          'string'
        ],
        settings: {
          nameFormat: 'string'
        },
        type: 'OIDC'
      },
      status: 'ACTIVE',
      type: 'IWA'
    };
    describe('#updateIdentityProvider - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIdentityProvider(identityProviderIdpId, identityProviderUpdateIdentityProviderBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'updateIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityProvider - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIdentityProvider(identityProviderIdpId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('ORG_URL', data.response.issuerMode);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.policy);
                assert.equal('object', typeof data.response.protocol);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('LINKEDIN', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'getIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCsrsForIdentityProvider - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listCsrsForIdentityProvider(identityProviderIdpId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'listCsrsForIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsrForIdentityProvider - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCsrForIdentityProvider(identityProviderIdpId, identityProviderCsrId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.csr);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.kty);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'getCsrForIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIdentityProviderSigningKeys - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listIdentityProviderSigningKeys(identityProviderIdpId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'listIdentityProviderSigningKeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityProviderSigningKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIdentityProviderSigningKey(identityProviderIdpId, identityProviderKeyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.alg);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.e);
                assert.equal('string', data.response.expiresAt);
                assert.equal(true, Array.isArray(data.response.key_ops));
                assert.equal('string', data.response.kid);
                assert.equal('string', data.response.kty);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.n);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.use);
                assert.equal(true, Array.isArray(data.response.x5c));
                assert.equal('string', data.response.x5t);
                assert.equal('string', data.response['x5t#S256']);
                assert.equal('string', data.response.x5u);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'getIdentityProviderSigningKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIdentityProviderApplicationUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listIdentityProviderApplicationUsers(identityProviderIdpId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'listIdentityProviderApplicationUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityProviderApplicationUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIdentityProviderApplicationUser(identityProviderIdpId, identityProviderUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.externalId);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('object', typeof data.response.profile);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'getIdentityProviderApplicationUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSocialAuthTokens - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listSocialAuthTokens(identityProviderIdpId, identityProviderUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'listSocialAuthTokens', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inlineHookCreateInlineHookBodyParam = {
      _links: {},
      channel: {
        config: {
          authScheme: {
            key: 'string',
            type: 'string',
            value: 'string'
          },
          headers: [
            {
              key: 'string',
              value: 'string'
            }
          ],
          method: 'string',
          uri: 'string'
        },
        type: 'HTTP',
        version: 'string'
      },
      created: 'string',
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      status: 'INACTIVE',
      type: 'com.okta.import.transform',
      version: 'string'
    };
    describe('#createInlineHook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createInlineHook(inlineHookCreateInlineHookBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.channel);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('INACTIVE', data.response.status);
                assert.equal('com.okta.import.transform', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InlineHook', 'createInlineHook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inlineHookInlineHookId = 'fakedata';
    const inlineHookExecuteInlineHookBodyParam = {};
    describe('#executeInlineHook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.executeInlineHook(inlineHookInlineHookId, inlineHookExecuteInlineHookBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.commands));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InlineHook', 'executeInlineHook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateInlineHook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.activateInlineHook(inlineHookInlineHookId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.channel);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('com.okta.saml.tokens.transform', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InlineHook', 'activateInlineHook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateInlineHook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deactivateInlineHook(inlineHookInlineHookId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.channel);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('INACTIVE', data.response.status);
                assert.equal('com.okta.user.pre-registration', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InlineHook', 'deactivateInlineHook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listInlineHooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listInlineHooks(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InlineHook', 'listInlineHooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inlineHookUpdateInlineHookBodyParam = {
      _links: {},
      channel: {
        config: {
          authScheme: {
            key: 'string',
            type: 'string',
            value: 'string'
          },
          headers: [
            {
              key: 'string',
              value: 'string'
            }
          ],
          method: 'string',
          uri: 'string'
        },
        type: 'HTTP',
        version: 'string'
      },
      created: 'string',
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      status: 'ACTIVE',
      type: 'com.okta.oauth2.tokens.transform',
      version: 'string'
    };
    describe('#updateInlineHook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateInlineHook(inlineHookInlineHookId, inlineHookUpdateInlineHookBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InlineHook', 'updateInlineHook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInlineHook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInlineHook(inlineHookInlineHookId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.channel);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('com.okta.saml.tokens.transform', data.response.type);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InlineHook', 'getInlineHook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLogs(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Log', 'getLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let profileMappingMappingId = 'fakedata';
    const profileMappingUpdateProfileMappingBodyParam = {
      _links: {},
      id: 'string',
      properties: {},
      source: {
        _links: {},
        id: 'string',
        name: 'string',
        type: 'string'
      },
      target: {
        _links: {},
        id: 'string',
        name: 'string',
        type: 'string'
      }
    };
    describe('#updateProfileMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateProfileMapping(profileMappingMappingId, profileMappingUpdateProfileMappingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.properties);
                assert.equal('object', typeof data.response.source);
                assert.equal('object', typeof data.response.target);
              } else {
                runCommonAsserts(data, error);
              }
              profileMappingMappingId = data.response.id;
              saveMockData('ProfileMapping', 'updateProfileMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listProfileMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listProfileMappings(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProfileMapping', 'listProfileMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProfileMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProfileMapping(profileMappingMappingId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.properties);
                assert.equal('object', typeof data.response.source);
                assert.equal('object', typeof data.response.target);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProfileMapping', 'getProfileMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userSchemaAppInstanceId = 'fakedata';
    const userSchemaUpdateApplicationUserProfileBodyParam = {
      $schema: 'string',
      _links: {},
      created: 'string',
      definitions: {
        base: {
          id: 'string',
          properties: {
            city: {},
            costCenter: {},
            countryCode: {},
            department: {},
            displayName: {},
            division: {},
            email: {},
            employeeNumber: {},
            firstName: {},
            honorificPrefix: {},
            honorificSuffix: {},
            lastName: {},
            locale: {},
            login: {},
            manager: {},
            managerId: {},
            middleName: {},
            mobilePhone: {},
            nickName: {},
            organization: {},
            postalAddress: {},
            preferredLanguage: {},
            primaryPhone: {},
            profileUrl: {},
            secondEmail: {},
            state: {},
            streetAddress: {},
            timezone: {},
            title: {},
            userType: {},
            zipCode: {}
          },
          required: [
            'string'
          ],
          type: 'string'
        },
        custom: {
          id: 'string',
          properties: {},
          required: [
            'string'
          ],
          type: 'string'
        }
      },
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      properties: {},
      title: 'string',
      type: 'string'
    };
    describe('#updateApplicationUserProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateApplicationUserProfile(userSchemaAppInstanceId, userSchemaUpdateApplicationUserProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.$schema);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.definitions);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.properties);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserSchema', 'updateApplicationUserProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let userSchemaSchemaId = 'fakedata';
    const userSchemaUpdateUserProfileBodyParam = {
      $schema: 'string',
      _links: {},
      created: 'string',
      definitions: {
        base: {
          id: 'string',
          properties: {
            city: {},
            costCenter: {},
            countryCode: {},
            department: {},
            displayName: {},
            division: {},
            email: {},
            employeeNumber: {},
            firstName: {},
            honorificPrefix: {},
            honorificSuffix: {},
            lastName: {},
            locale: {},
            login: {},
            manager: {},
            managerId: {},
            middleName: {},
            mobilePhone: {},
            nickName: {},
            organization: {},
            postalAddress: {},
            preferredLanguage: {},
            primaryPhone: {},
            profileUrl: {},
            secondEmail: {},
            state: {},
            streetAddress: {},
            timezone: {},
            title: {},
            userType: {},
            zipCode: {}
          },
          required: [
            'string'
          ],
          type: 'string'
        },
        custom: {
          id: 'string',
          properties: {},
          required: [
            'string'
          ],
          type: 'string'
        }
      },
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      properties: {},
      title: 'string',
      type: 'string'
    };
    describe('#updateUserProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateUserProfile(userSchemaSchemaId, userSchemaUpdateUserProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.$schema);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.definitions);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.properties);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              userSchemaSchemaId = data.response.id;
              saveMockData('UserSchema', 'updateUserProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationUserSchema - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationUserSchema(userSchemaAppInstanceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.$schema);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.definitions);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.properties);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserSchema', 'getApplicationUserSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserSchema - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserSchema(userSchemaSchemaId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.$schema);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.definitions);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.properties);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserSchema', 'getUserSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkedObjectAddLinkedObjectDefinitionBodyParam = {
      _links: {},
      associated: {
        description: 'string',
        name: 'string',
        title: 'string',
        type: 'USER'
      },
      primary: {
        description: 'string',
        name: 'string',
        title: 'string',
        type: 'USER'
      }
    };
    describe('#addLinkedObjectDefinition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addLinkedObjectDefinition(linkedObjectAddLinkedObjectDefinitionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.associated);
                assert.equal('object', typeof data.response.primary);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkedObject', 'addLinkedObjectDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLinkedObjectDefinitions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listLinkedObjectDefinitions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkedObject', 'listLinkedObjectDefinitions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkedObjectLinkedObjectName = 'fakedata';
    describe('#getLinkedObjectDefinition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLinkedObjectDefinition(linkedObjectLinkedObjectName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.associated);
                assert.equal('object', typeof data.response.primary);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkedObject', 'getLinkedObjectDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let userTypeTypeId = 'fakedata';
    const userTypeCreateUserTypeBodyParam = {
      _links: {},
      created: 'string',
      createdBy: 'string',
      default: false,
      description: 'string',
      displayName: 'string',
      id: 'string',
      lastUpdated: 'string',
      lastUpdatedBy: 'string',
      name: 'string'
    };
    describe('#createUserType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createUserType(userTypeCreateUserTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.createdBy);
                assert.equal(true, data.response.default);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.lastUpdatedBy);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              userTypeTypeId = data.response.id;
              saveMockData('UserType', 'createUserType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userTypeUpdateUserTypeBodyParam = {
      _links: {},
      created: 'string',
      createdBy: 'string',
      default: false,
      description: 'string',
      displayName: 'string',
      id: 'string',
      lastUpdated: 'string',
      lastUpdatedBy: 'string',
      name: 'string'
    };
    describe('#updateUserType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateUserType(userTypeTypeId, userTypeUpdateUserTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.createdBy);
                assert.equal(false, data.response.default);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.lastUpdatedBy);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserType', 'updateUserType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUserTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listUserTypes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserType', 'listUserTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userTypeReplaceUserTypeBodyParam = {
      _links: {},
      created: 'string',
      createdBy: 'string',
      default: true,
      description: 'string',
      displayName: 'string',
      id: 'string',
      lastUpdated: 'string',
      lastUpdatedBy: 'string',
      name: 'string'
    };
    describe('#replaceUserType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.replaceUserType(userTypeTypeId, userTypeReplaceUserTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserType', 'replaceUserType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserType(userTypeTypeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.createdBy);
                assert.equal(true, data.response.default);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.lastUpdatedBy);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserType', 'getUserType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let policyType = 'fakedata';
    let policyPolicyId = 'fakedata';
    let policyRuleId = 'fakedata';
    const policyCreatePolicyBodyParam = {
      _embedded: {},
      _links: {},
      conditions: {
        app: {
          exclude: [
            {
              id: 'string',
              name: 'string',
              type: 'APP'
            }
          ],
          include: [
            {
              id: 'string',
              name: 'string',
              type: 'APP'
            }
          ]
        },
        apps: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        authContext: {
          authType: 'ANY'
        },
        authProvider: {
          include: [
            'string'
          ],
          provider: 'ACTIVE_DIRECTORY'
        },
        beforeScheduledAction: {
          duration: {
            number: 2,
            unit: 'string'
          },
          lifecycleAction: {
            status: 'DELETING'
          }
        },
        clients: {
          include: [
            'string'
          ]
        },
        context: {
          expression: 'string'
        },
        device: {
          migrated: false,
          platform: {
            supportedMDMFrameworks: [
              'AFW'
            ],
            types: [
              'OSX'
            ]
          },
          rooted: false,
          trustLevel: 'TRUSTED'
        },
        grantTypes: {
          include: [
            'string'
          ]
        },
        groups: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        identityProvider: {
          idpIds: [
            'string'
          ],
          provider: 'OKTA'
        },
        mdmEnrollment: {
          blockNonSafeAndroid: false,
          enrollment: 'ANY_OR_NONE'
        },
        network: {
          connection: 'ZONE',
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        people: {
          groups: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          },
          users: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          }
        },
        platform: {
          exclude: [
            {
              os: {},
              type: 'OTHER'
            }
          ],
          include: [
            {
              os: {},
              type: 'MOBILE'
            }
          ]
        },
        risk: {
          behaviors: [
            'string'
          ]
        },
        riskScore: {
          level: 'string'
        },
        scopes: {
          include: [
            'string'
          ]
        },
        userIdentifier: {
          attribute: 'string',
          patterns: [
            {
              matchType: 'SUFFIX',
              value: 'string'
            }
          ],
          type: 'IDENTIFIER'
        },
        userStatus: {
          value: 'EXPIRED_PASSWORD'
        },
        users: {
          exclude: [
            'string'
          ],
          inactivity: {
            number: 8,
            unit: 'string'
          },
          include: [
            'string'
          ],
          lifecycleExpiration: {
            lifecycleStatus: 'string',
            number: 1,
            unit: 'string'
          },
          passwordExpiration: {
            number: 6,
            unit: 'string'
          },
          userLifecycleAttribute: {
            attributeName: 'string',
            matchingValue: 'string'
          }
        }
      },
      created: 'string',
      description: 'string',
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      priority: 10,
      status: 'INACTIVE',
      system: true,
      type: 'PASSWORD'
    };
    describe('#createPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPolicy(policyCreatePolicyBodyParam, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.conditions);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal(10, data.response.priority);
                assert.equal('INACTIVE', data.response.status);
                assert.equal(false, data.response.system);
                assert.equal('PASSWORD', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              policyType = data.response.type;
              policyPolicyId = data.response.id;
              policyRuleId = data.response.id;
              saveMockData('Policy', 'createPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activatePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activatePolicy(policyPolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'activatePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivatePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deactivatePolicy(policyPolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'deactivatePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyCreatePolicyRuleBodyParam = {
      actions: {
        enroll: {
          self: 'NEVER'
        },
        passwordChange: {
          access: 'DENY'
        },
        selfServicePasswordReset: {
          access: 'ALLOW'
        },
        selfServiceUnlock: {
          access: 'ALLOW'
        },
        signon: {
          access: 'ALLOW',
          factorLifetime: 2,
          factorPromptMode: 'ALWAYS',
          rememberDeviceByDefault: false,
          requireFactor: true,
          session: {
            maxSessionIdleMinutes: 120,
            maxSessionLifetimeMinutes: 1,
            usePersistentCookie: false
          }
        }
      },
      conditions: {
        app: {
          exclude: [
            {
              id: 'string',
              name: 'string',
              type: 'APP'
            }
          ],
          include: [
            {
              id: 'string',
              name: 'string',
              type: 'APP'
            }
          ]
        },
        apps: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        authContext: {
          authType: 'RADIUS'
        },
        authProvider: {
          include: [
            'string'
          ],
          provider: 'ACTIVE_DIRECTORY'
        },
        beforeScheduledAction: {
          duration: {
            number: 10,
            unit: 'string'
          },
          lifecycleAction: {
            status: 'DELETING'
          }
        },
        clients: {
          include: [
            'string'
          ]
        },
        context: {
          expression: 'string'
        },
        device: {
          migrated: true,
          platform: {
            supportedMDMFrameworks: [
              'NATIVE'
            ],
            types: [
              'WINDOWS'
            ]
          },
          rooted: true,
          trustLevel: 'TRUSTED'
        },
        grantTypes: {
          include: [
            'string'
          ]
        },
        groups: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        identityProvider: {
          idpIds: [
            'string'
          ],
          provider: 'OKTA'
        },
        mdmEnrollment: {
          blockNonSafeAndroid: true,
          enrollment: 'ANY_OR_NONE'
        },
        network: {
          connection: 'ANYWHERE',
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        people: {
          groups: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          },
          users: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          }
        },
        platform: {
          exclude: [
            {
              os: {},
              type: 'DESKTOP'
            }
          ],
          include: [
            {
              os: {},
              type: 'DESKTOP'
            }
          ]
        },
        risk: {
          behaviors: [
            'string'
          ]
        },
        riskScore: {
          level: 'string'
        },
        scopes: {
          include: [
            'string'
          ]
        },
        userIdentifier: {
          attribute: 'string',
          patterns: [
            {
              matchType: 'STARTS_WITH',
              value: 'string'
            }
          ],
          type: 'IDENTIFIER'
        },
        userStatus: {
          value: 'SUSPENDED'
        },
        users: {
          exclude: [
            'string'
          ],
          inactivity: {
            number: 7,
            unit: 'string'
          },
          include: [
            'string'
          ],
          lifecycleExpiration: {
            lifecycleStatus: 'string',
            number: 2,
            unit: 'string'
          },
          passwordExpiration: {
            number: 4,
            unit: 'string'
          },
          userLifecycleAttribute: {
            attributeName: 'string',
            matchingValue: 'string'
          }
        }
      },
      created: 'string',
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      priority: 3,
      status: 'ACTIVE',
      system: true,
      type: 'SIGN_ON'
    };
    describe('#createPolicyRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPolicyRule(policyPolicyId, policyCreatePolicyRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.actions);
                assert.equal('object', typeof data.response.conditions);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal(10, data.response.priority);
                assert.equal('ACTIVE', data.response.status);
                assert.equal(true, data.response.system);
                assert.equal('SIGN_ON', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'createPolicyRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activatePolicyRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activatePolicyRule(policyPolicyId, policyRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'activatePolicyRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivatePolicyRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deactivatePolicyRule(policyPolicyId, policyRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'deactivatePolicyRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listPolicies(policyType, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'listPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUpdatePolicyBodyParam = {
      _embedded: {},
      _links: {},
      conditions: {
        app: {
          exclude: [
            {
              id: 'string',
              name: 'string',
              type: 'APP_TYPE'
            }
          ],
          include: [
            {
              id: 'string',
              name: 'string',
              type: 'APP'
            }
          ]
        },
        apps: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        authContext: {
          authType: 'RADIUS'
        },
        authProvider: {
          include: [
            'string'
          ],
          provider: 'ACTIVE_DIRECTORY'
        },
        beforeScheduledAction: {
          duration: {
            number: 7,
            unit: 'string'
          },
          lifecycleAction: {
            status: 'PENDING'
          }
        },
        clients: {
          include: [
            'string'
          ]
        },
        context: {
          expression: 'string'
        },
        device: {
          migrated: false,
          platform: {
            supportedMDMFrameworks: [
              'AFW'
            ],
            types: [
              'OSX'
            ]
          },
          rooted: true,
          trustLevel: 'TRUSTED'
        },
        grantTypes: {
          include: [
            'string'
          ]
        },
        groups: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        identityProvider: {
          idpIds: [
            'string'
          ],
          provider: 'ANY'
        },
        mdmEnrollment: {
          blockNonSafeAndroid: false,
          enrollment: 'OMM'
        },
        network: {
          connection: 'ZONE',
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        people: {
          groups: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          },
          users: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          }
        },
        platform: {
          exclude: [
            {
              os: {},
              type: 'ANY'
            }
          ],
          include: [
            {
              os: {},
              type: 'MOBILE'
            }
          ]
        },
        risk: {
          behaviors: [
            'string'
          ]
        },
        riskScore: {
          level: 'string'
        },
        scopes: {
          include: [
            'string'
          ]
        },
        userIdentifier: {
          attribute: 'string',
          patterns: [
            {
              matchType: 'CONTAINS',
              value: 'string'
            }
          ],
          type: 'IDENTIFIER'
        },
        userStatus: {
          value: 'INACTIVE'
        },
        users: {
          exclude: [
            'string'
          ],
          inactivity: {
            number: 6,
            unit: 'string'
          },
          include: [
            'string'
          ],
          lifecycleExpiration: {
            lifecycleStatus: 'string',
            number: 9,
            unit: 'string'
          },
          passwordExpiration: {
            number: 5,
            unit: 'string'
          },
          userLifecycleAttribute: {
            attributeName: 'string',
            matchingValue: 'string'
          }
        }
      },
      created: 'string',
      description: 'string',
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      priority: 6,
      status: 'INACTIVE',
      system: false,
      type: 'OAUTH_AUTHORIZATION_POLICY'
    };
    describe('#updatePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePolicy(policyUpdatePolicyBodyParam, policyPolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updatePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicy(policyPolicyId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('object', typeof data.response.conditions);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal(4, data.response.priority);
                assert.equal('INACTIVE', data.response.status);
                assert.equal(true, data.response.system);
                assert.equal('IDP_DISCOVERY', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPolicyRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listPolicyRules(policyPolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'listPolicyRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyUpdatePolicyRuleBodyParam = {
      actions: {
        enroll: {
          self: 'NEVER'
        },
        passwordChange: {
          access: 'ALLOW'
        },
        selfServicePasswordReset: {
          access: 'ALLOW'
        },
        selfServiceUnlock: {
          access: 'DENY'
        },
        signon: {
          access: 'DENY',
          factorLifetime: 4,
          factorPromptMode: 'DEVICE',
          rememberDeviceByDefault: true,
          requireFactor: true,
          session: {
            maxSessionIdleMinutes: 120,
            maxSessionLifetimeMinutes: 2,
            usePersistentCookie: true
          }
        }
      },
      conditions: {
        app: {
          exclude: [
            {
              id: 'string',
              name: 'string',
              type: 'APP_TYPE'
            }
          ],
          include: [
            {
              id: 'string',
              name: 'string',
              type: 'APP_TYPE'
            }
          ]
        },
        apps: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        authContext: {
          authType: 'RADIUS'
        },
        authProvider: {
          include: [
            'string'
          ],
          provider: 'OKTA'
        },
        beforeScheduledAction: {
          duration: {
            number: 1,
            unit: 'string'
          },
          lifecycleAction: {
            status: 'DELETING'
          }
        },
        clients: {
          include: [
            'string'
          ]
        },
        context: {
          expression: 'string'
        },
        device: {
          migrated: false,
          platform: {
            supportedMDMFrameworks: [
              'SAFE'
            ],
            types: [
              'OSX'
            ]
          },
          rooted: false,
          trustLevel: 'TRUSTED'
        },
        grantTypes: {
          include: [
            'string'
          ]
        },
        groups: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        identityProvider: {
          idpIds: [
            'string'
          ],
          provider: 'SPECIFIC_IDP'
        },
        mdmEnrollment: {
          blockNonSafeAndroid: true,
          enrollment: 'OMM'
        },
        network: {
          connection: 'ANYWHERE',
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        people: {
          groups: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          },
          users: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          }
        },
        platform: {
          exclude: [
            {
              os: {},
              type: 'DESKTOP'
            }
          ],
          include: [
            {
              os: {},
              type: 'DESKTOP'
            }
          ]
        },
        risk: {
          behaviors: [
            'string'
          ]
        },
        riskScore: {
          level: 'string'
        },
        scopes: {
          include: [
            'string'
          ]
        },
        userIdentifier: {
          attribute: 'string',
          patterns: [
            {
              matchType: 'SUFFIX',
              value: 'string'
            }
          ],
          type: 'ATTRIBUTE'
        },
        userStatus: {
          value: 'DELETED'
        },
        users: {
          exclude: [
            'string'
          ],
          inactivity: {
            number: 5,
            unit: 'string'
          },
          include: [
            'string'
          ],
          lifecycleExpiration: {
            lifecycleStatus: 'string',
            number: 9,
            unit: 'string'
          },
          passwordExpiration: {
            number: 9,
            unit: 'string'
          },
          userLifecycleAttribute: {
            attributeName: 'string',
            matchingValue: 'string'
          }
        }
      },
      created: 'string',
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      priority: 3,
      status: 'ACTIVE',
      system: true,
      type: 'PASSWORD'
    };
    describe('#updatePolicyRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePolicyRule(policyPolicyId, policyRuleId, policyUpdatePolicyRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'updatePolicyRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicyRule(policyPolicyId, policyRuleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.actions);
                assert.equal('object', typeof data.response.conditions);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.priority);
                assert.equal('ACTIVE', data.response.status);
                assert.equal(true, data.response.system);
                assert.equal('PASSWORD', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getPolicyRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let sessionSessionId = 'fakedata';
    const sessionCreateSessionBodyParam = {
      sessionToken: 'string'
    };
    describe('#createSession - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSession(sessionCreateSessionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal(true, Array.isArray(data.response.amr));
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.expiresAt);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.idp);
                assert.equal('string', data.response.lastFactorVerification);
                assert.equal('string', data.response.lastPasswordVerification);
                assert.equal('string', data.response.login);
                assert.equal('MFA_ENROLL', data.response.status);
                assert.equal('string', data.response.userId);
              } else {
                runCommonAsserts(data, error);
              }
              sessionSessionId = data.response.id;
              saveMockData('Session', 'createSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshSession - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.refreshSession(sessionSessionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal(true, Array.isArray(data.response.amr));
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.expiresAt);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.idp);
                assert.equal('string', data.response.lastFactorVerification);
                assert.equal('string', data.response.lastPasswordVerification);
                assert.equal('string', data.response.login);
                assert.equal('MFA_ENROLL', data.response.status);
                assert.equal('string', data.response.userId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Session', 'refreshSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSession - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSession(sessionSessionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal(true, Array.isArray(data.response.amr));
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.expiresAt);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.idp);
                assert.equal('string', data.response.lastFactorVerification);
                assert.equal('string', data.response.lastPasswordVerification);
                assert.equal('string', data.response.login);
                assert.equal('MFA_ENROLL', data.response.status);
                assert.equal('string', data.response.userId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Session', 'getSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let templateTemplateId = 'fakedata';
    const templateCreateSmsTemplateBodyParam = {
      created: 'string',
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      template: 'string',
      translations: {},
      type: 'SMS_VERIFY_CODE'
    };
    describe('#createSmsTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSmsTemplate(templateCreateSmsTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.template);
                assert.equal('object', typeof data.response.translations);
                assert.equal('SMS_VERIFY_CODE', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              templateTemplateId = data.response.id;
              saveMockData('Template', 'createSmsTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templatePartialUpdateSmsTemplateBodyParam = {
      created: 'string',
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      template: 'string',
      translations: {},
      type: 'SMS_VERIFY_CODE'
    };
    describe('#partialUpdateSmsTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partialUpdateSmsTemplate(templateTemplateId, templatePartialUpdateSmsTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.template);
                assert.equal('object', typeof data.response.translations);
                assert.equal('SMS_VERIFY_CODE', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'partialUpdateSmsTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSmsTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listSmsTemplates(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'listSmsTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateUpdateSmsTemplateBodyParam = {
      created: 'string',
      id: 'string',
      lastUpdated: 'string',
      name: 'string',
      template: 'string',
      translations: {},
      type: 'SMS_VERIFY_CODE'
    };
    describe('#updateSmsTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSmsTemplate(templateTemplateId, templateUpdateSmsTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'updateSmsTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSmsTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSmsTemplate(templateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.template);
                assert.equal('object', typeof data.response.translations);
                assert.equal('SMS_VERIFY_CODE', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'getSmsTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const threatInsightUpdateConfigurationBodyParam = {
      _links: {},
      action: 'string',
      created: 'string',
      excludeZones: [
        'string'
      ],
      lastUpdated: 'string'
    };
    describe('#updateConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateConfiguration(threatInsightUpdateConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.action);
                assert.equal('string', data.response.created);
                assert.equal(true, Array.isArray(data.response.excludeZones));
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ThreatInsight', 'updateConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCurrentConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCurrentConfiguration((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.action);
                assert.equal('string', data.response.created);
                assert.equal(true, Array.isArray(data.response.excludeZones));
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ThreatInsight', 'getCurrentConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trustedOriginCreateOriginBodyParam = {
      _links: {},
      created: 'string',
      createdBy: 'string',
      id: 'string',
      lastUpdated: 'string',
      lastUpdatedBy: 'string',
      name: 'string',
      origin: 'string',
      scopes: [
        {
          stringValue: 'string',
          type: 'CORS'
        }
      ],
      status: 'string'
    };
    describe('#createOrigin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOrigin(trustedOriginCreateOriginBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.lastUpdatedBy);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.origin);
                assert.equal(true, Array.isArray(data.response.scopes));
                assert.equal('string', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedOrigin', 'createOrigin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trustedOriginTrustedOriginId = 'fakedata';
    describe('#activateOrigin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.activateOrigin(trustedOriginTrustedOriginId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.lastUpdatedBy);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.origin);
                assert.equal(true, Array.isArray(data.response.scopes));
                assert.equal('string', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedOrigin', 'activateOrigin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateOrigin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deactivateOrigin(trustedOriginTrustedOriginId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.lastUpdatedBy);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.origin);
                assert.equal(true, Array.isArray(data.response.scopes));
                assert.equal('string', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedOrigin', 'deactivateOrigin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listOrigins - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listOrigins(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedOrigin', 'listOrigins', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trustedOriginUpdateOriginBodyParam = {
      _links: {},
      created: 'string',
      createdBy: 'string',
      id: 'string',
      lastUpdated: 'string',
      lastUpdatedBy: 'string',
      name: 'string',
      origin: 'string',
      scopes: [
        {
          stringValue: 'string',
          type: 'CORS'
        }
      ],
      status: 'string'
    };
    describe('#updateOrigin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOrigin(trustedOriginTrustedOriginId, trustedOriginUpdateOriginBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedOrigin', 'updateOrigin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrigin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrigin(trustedOriginTrustedOriginId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.lastUpdatedBy);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.origin);
                assert.equal(true, Array.isArray(data.response.scopes));
                assert.equal('string', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedOrigin', 'getOrigin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let userUserId = 'fakedata';
    let userClientId = 'fakedata';
    let userTokenId = 'fakedata';
    let userGrantId = 'fakedata';
    let userRoleId = 'fakedata';
    let userApplicationId = 'fakedata';
    let userGroupId = 'fakedata';
    const userCreateUserBodyParam = {
      credentials: {
        password: {
          hash: {
            algorithm: {},
            salt: 'string',
            saltOrder: 'string',
            value: 'string',
            workFactor: 8
          },
          hook: {
            type: 'string'
          },
          value: 'string'
        },
        provider: {
          name: 'string',
          type: 'ACTIVE_DIRECTORY'
        },
        recovery_question: {
          answer: 'string',
          question: 'string'
        }
      },
      groupIds: [
        'string'
      ],
      profile: {
        city: 'string',
        costCenter: 'string',
        countryCode: 'string',
        department: 'string',
        displayName: 'string',
        division: 'string',
        email: 'string',
        employeeNumber: 'string',
        firstName: 'string',
        honorificPrefix: 'string',
        honorificSuffix: 'string',
        lastName: 'string',
        locale: 'string',
        login: 'string',
        manager: 'string',
        managerId: 'string',
        middleName: 'string',
        mobilePhone: 'string',
        nickName: 'string',
        organization: 'string',
        postalAddress: 'string',
        preferredLanguage: 'string',
        primaryPhone: 'string',
        profileUrl: 'string',
        secondEmail: 'string',
        state: 'string',
        streetAddress: 'string',
        timezone: 'string',
        title: 'string',
        userType: 'string',
        zipCode: 'string'
      },
      type: {
        _links: {},
        created: 'string',
        createdBy: 'string',
        default: false,
        description: 'string',
        displayName: 'string',
        id: 'string',
        lastUpdated: 'string',
        lastUpdatedBy: 'string',
        name: 'string'
      }
    };
    describe('#createUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createUser(userCreateUserBodyParam, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.activated);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.credentials);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastLogin);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.passwordChanged);
                assert.equal('object', typeof data.response.profile);
                assert.equal('DEPROVISIONED', data.response.status);
                assert.equal('string', data.response.statusChanged);
                assert.equal('PASSWORD_EXPIRED', data.response.transitioningToStatus);
                assert.equal('object', typeof data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              userUserId = data.response.id;
              userClientId = data.response.id;
              userTokenId = data.response.id;
              userGrantId = data.response.id;
              userRoleId = data.response.id;
              userApplicationId = data.response.id;
              userGroupId = data.response.id;
              saveMockData('User', 'createUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userPartialUpdateUserBodyParam = {
      _embedded: {},
      _links: {},
      activated: 'string',
      created: 'string',
      credentials: {
        password: {
          hash: {
            algorithm: {},
            salt: 'string',
            saltOrder: 'string',
            value: 'string',
            workFactor: 6
          },
          hook: {
            type: 'string'
          },
          value: 'string'
        },
        provider: {
          name: 'string',
          type: 'LDAP'
        },
        recovery_question: {
          answer: 'string',
          question: 'string'
        }
      },
      id: 'string',
      lastLogin: 'string',
      lastUpdated: 'string',
      passwordChanged: 'string',
      profile: {
        city: 'string',
        costCenter: 'string',
        countryCode: 'string',
        department: 'string',
        displayName: 'string',
        division: 'string',
        email: 'string',
        employeeNumber: 'string',
        firstName: 'string',
        honorificPrefix: 'string',
        honorificSuffix: 'string',
        lastName: 'string',
        locale: 'string',
        login: 'string',
        manager: 'string',
        managerId: 'string',
        middleName: 'string',
        mobilePhone: 'string',
        nickName: 'string',
        organization: 'string',
        postalAddress: 'string',
        preferredLanguage: 'string',
        primaryPhone: 'string',
        profileUrl: 'string',
        secondEmail: 'string',
        state: 'string',
        streetAddress: 'string',
        timezone: 'string',
        title: 'string',
        userType: 'string',
        zipCode: 'string'
      },
      status: 'PASSWORD_EXPIRED',
      statusChanged: 'string',
      transitioningToStatus: 'PASSWORD_EXPIRED',
      type: {
        _links: {},
        created: 'string',
        createdBy: 'string',
        default: true,
        description: 'string',
        displayName: 'string',
        id: 'string',
        lastUpdated: 'string',
        lastUpdatedBy: 'string',
        name: 'string'
      }
    };
    describe('#partialUpdateUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partialUpdateUser(userPartialUpdateUserBodyParam, userUserId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.activated);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.credentials);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastLogin);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.passwordChanged);
                assert.equal('object', typeof data.response.profile);
                assert.equal('STAGED', data.response.status);
                assert.equal('string', data.response.statusChanged);
                assert.equal('ACTIVE', data.response.transitioningToStatus);
                assert.equal('object', typeof data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'partialUpdateUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userChangePasswordBodyParam = {
      newPassword: {
        hash: {
          algorithm: {},
          salt: 'string',
          saltOrder: 'string',
          value: 'string',
          workFactor: 1
        },
        hook: {
          type: 'string'
        },
        value: 'string'
      },
      oldPassword: {
        hash: {
          algorithm: {},
          salt: 'string',
          saltOrder: 'string',
          value: 'string',
          workFactor: 10
        },
        hook: {
          type: 'string'
        },
        value: 'string'
      }
    };
    describe('#changePassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changePassword(userChangePasswordBodyParam, userUserId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.password);
                assert.equal('object', typeof data.response.provider);
                assert.equal('object', typeof data.response.recovery_question);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'changePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userChangeRecoveryQuestionBodyParam = {
      password: {
        hash: {
          algorithm: {},
          salt: 'string',
          saltOrder: 'string',
          value: 'string',
          workFactor: 10
        },
        hook: {
          type: 'string'
        },
        value: 'string'
      },
      provider: {
        name: 'string',
        type: 'FEDERATION'
      },
      recovery_question: {
        answer: 'string',
        question: 'string'
      }
    };
    describe('#changeRecoveryQuestion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeRecoveryQuestion(userChangeRecoveryQuestionBodyParam, userUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.password);
                assert.equal('object', typeof data.response.provider);
                assert.equal('object', typeof data.response.recovery_question);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'changeRecoveryQuestion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postApiV1UsersUserIdCredentialsForgotPassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postApiV1UsersUserIdCredentialsForgotPassword(userUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.resetPasswordUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'postApiV1UsersUserIdCredentialsForgotPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userSendEmail = true;
    describe('#activateUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.activateUser(userUserId, userSendEmail, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.activationToken);
                assert.equal('string', data.response.activationUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'activateUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deactivateUser(userUserId, userSendEmail, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'deactivateUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expirePassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.expirePassword(userUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.activated);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.credentials);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastLogin);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.passwordChanged);
                assert.equal('object', typeof data.response.profile);
                assert.equal('LOCKED_OUT', data.response.status);
                assert.equal('string', data.response.statusChanged);
                assert.equal('ACTIVE', data.response.transitioningToStatus);
                assert.equal('object', typeof data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'expirePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reactivateUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.reactivateUser(userUserId, userSendEmail, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.activationToken);
                assert.equal('string', data.response.activationUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'reactivateUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetFactors - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resetFactors(userUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'resetFactors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetPassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.resetPassword(userUserId, userSendEmail, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.resetPasswordUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'resetPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#suspendUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.suspendUser(userUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'suspendUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unlockUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.unlockUser(userUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'unlockUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unsuspendUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.unsuspendUser(userUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'unsuspendUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userAssignRoleToUserBodyParam = {
      type: 'APP_ADMIN'
    };
    describe('#assignRoleToUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.assignRoleToUser(userAssignRoleToUserBodyParam, userUserId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('GROUP', data.response.assignmentType);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.label);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('INACTIVE', data.response.status);
                assert.equal('SUPER_ADMIN', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'assignRoleToUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listUsers(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'listUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userAssociatedUserId = 'fakedata';
    const userPrimaryRelationshipName = 'fakedata';
    const userPrimaryUserId = 'fakedata';
    describe('#setLinkedObjectForUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setLinkedObjectForUser(userAssociatedUserId, userPrimaryRelationshipName, userPrimaryUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'setLinkedObjectForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userUpdateUserBodyParam = {
      _embedded: {},
      _links: {},
      activated: 'string',
      created: 'string',
      credentials: {
        password: {
          hash: {
            algorithm: {},
            salt: 'string',
            saltOrder: 'string',
            value: 'string',
            workFactor: 5
          },
          hook: {
            type: 'string'
          },
          value: 'string'
        },
        provider: {
          name: 'string',
          type: 'IMPORT'
        },
        recovery_question: {
          answer: 'string',
          question: 'string'
        }
      },
      id: 'string',
      lastLogin: 'string',
      lastUpdated: 'string',
      passwordChanged: 'string',
      profile: {
        city: 'string',
        costCenter: 'string',
        countryCode: 'string',
        department: 'string',
        displayName: 'string',
        division: 'string',
        email: 'string',
        employeeNumber: 'string',
        firstName: 'string',
        honorificPrefix: 'string',
        honorificSuffix: 'string',
        lastName: 'string',
        locale: 'string',
        login: 'string',
        manager: 'string',
        managerId: 'string',
        middleName: 'string',
        mobilePhone: 'string',
        nickName: 'string',
        organization: 'string',
        postalAddress: 'string',
        preferredLanguage: 'string',
        primaryPhone: 'string',
        profileUrl: 'string',
        secondEmail: 'string',
        state: 'string',
        streetAddress: 'string',
        timezone: 'string',
        title: 'string',
        userType: 'string',
        zipCode: 'string'
      },
      status: 'PASSWORD_EXPIRED',
      statusChanged: 'string',
      transitioningToStatus: 'STAGED',
      type: {
        _links: {},
        created: 'string',
        createdBy: 'string',
        default: false,
        description: 'string',
        displayName: 'string',
        id: 'string',
        lastUpdated: 'string',
        lastUpdatedBy: 'string',
        name: 'string'
      }
    };
    describe('#updateUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateUser(userUpdateUserBodyParam, userUserId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'updateUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUser(userUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.activated);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.credentials);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastLogin);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.passwordChanged);
                assert.equal('object', typeof data.response.profile);
                assert.equal('RECOVERY', data.response.status);
                assert.equal('string', data.response.statusChanged);
                assert.equal('STAGED', data.response.transitioningToStatus);
                assert.equal('object', typeof data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppLinks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAppLinks(userUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'listAppLinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUserClients - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listUserClients(userUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'listUserClients', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGrantsForUserAndClient - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listGrantsForUserAndClient(userUserId, userClientId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'listGrantsForUserAndClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRefreshTokensForUserAndClient - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listRefreshTokensForUserAndClient(userUserId, userClientId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'listRefreshTokensForUserAndClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRefreshTokenForUserAndClient - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRefreshTokenForUserAndClient(userUserId, userClientId, userTokenId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.clientId);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('string', data.response.expiresAt);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issuer);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(true, Array.isArray(data.response.scopes));
                assert.equal('ACTIVE', data.response.status);
                assert.equal('string', data.response.userId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getRefreshTokenForUserAndClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUserGrants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listUserGrants(userUserId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'listUserGrants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserGrant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserGrant(userUserId, userGrantId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.clientId);
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.createdBy);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issuer);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('string', data.response.scopeId);
                assert.equal('END_USER', data.response.source);
                assert.equal('ACTIVE', data.response.status);
                assert.equal('string', data.response.userId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getUserGrant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUserGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listUserGroups(userUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'listUserGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUserIdentityProviders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listUserIdentityProviders(userUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'listUserIdentityProviders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userRelationshipName = 'fakedata';
    describe('#getLinkedObjectsForUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLinkedObjectsForUser(userUserId, userRelationshipName, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getLinkedObjectsForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAssignedRolesForUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAssignedRolesForUser(userUserId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'listAssignedRolesForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAllAppsAsTargetToRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAllAppsAsTargetToRole(userUserId, userRoleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'addAllAppsAsTargetToRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApplicationTargetsForApplicationAdministratorRoleForUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listApplicationTargetsForApplicationAdministratorRoleForUser(userUserId, userRoleId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'listApplicationTargetsForApplicationAdministratorRoleForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userAppName = 'fakedata';
    describe('#addApplicationTargetToAdminRoleForUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addApplicationTargetToAdminRoleForUser(userUserId, userRoleId, userAppName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'addApplicationTargetToAdminRoleForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addApplicationTargetToAppAdminRoleForUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addApplicationTargetToAppAdminRoleForUser(userUserId, userRoleId, userAppName, userApplicationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'addApplicationTargetToAppAdminRoleForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroupTargetsForRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listGroupTargetsForRole(userUserId, userRoleId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'listGroupTargetsForRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addGroupTargetToRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addGroupTargetToRole(userUserId, userRoleId, userGroupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'addGroupTargetToRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let userFactorUserId = 'fakedata';
    let userFactorFactorId = 'fakedata';
    let userFactorTransactionId = 'fakedata';
    const userFactorEnrollFactorBodyParam = {
      _embedded: {},
      _links: {},
      created: 'string',
      factorType: 'email',
      id: 'string',
      lastUpdated: 'string',
      provider: 'GOOGLE',
      status: 'PENDING_ACTIVATION',
      verify: {
        activationToken: 'string',
        answer: 'string',
        attestation: 'string',
        clientData: 'string',
        nextPassCode: 'string',
        passCode: 'string',
        registrationData: 'string',
        stateToken: 'string'
      }
    };
    describe('#enrollFactor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enrollFactor(userFactorUserId, userFactorEnrollFactorBodyParam, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('sms', data.response.factorType);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('FIDO', data.response.provider);
                assert.equal('PENDING_ACTIVATION', data.response.status);
                assert.equal('object', typeof data.response.verify);
              } else {
                runCommonAsserts(data, error);
              }
              userFactorUserId = data.response.id;
              userFactorFactorId = data.response.id;
              userFactorTransactionId = data.response.id;
              saveMockData('UserFactor', 'enrollFactor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userFactorActivateFactorBodyParam = {
      attestation: 'string',
      clientData: 'string',
      passCode: 'string',
      registrationData: 'string',
      stateToken: 'string'
    };
    describe('#activateFactor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.activateFactor(userFactorUserId, userFactorFactorId, userFactorActivateFactorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('hotp', data.response.factorType);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('FIDO', data.response.provider);
                assert.equal('INACTIVE', data.response.status);
                assert.equal('object', typeof data.response.verify);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserFactor', 'activateFactor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userFactorVerifyFactorBodyParam = {
      activationToken: 'string',
      answer: 'string',
      attestation: 'string',
      clientData: 'string',
      nextPassCode: 'string',
      passCode: 'string',
      registrationData: 'string',
      stateToken: 'string'
    };
    describe('#verifyFactor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.verifyFactor(userFactorUserId, userFactorFactorId, null, null, userFactorVerifyFactorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.expiresAt);
                assert.equal('SUCCESS', data.response.factorResult);
                assert.equal('string', data.response.factorResultMessage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserFactor', 'verifyFactor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFactors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listFactors(userFactorUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserFactor', 'listFactors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSupportedFactors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listSupportedFactors(userFactorUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserFactor', 'listSupportedFactors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSupportedSecurityQuestions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listSupportedSecurityQuestions(userFactorUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserFactor', 'listSupportedSecurityQuestions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFactor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFactor(userFactorUserId, userFactorFactorId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.created);
                assert.equal('hotp', data.response.factorType);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal('YUBICO', data.response.provider);
                assert.equal('DISABLED', data.response.status);
                assert.equal('object', typeof data.response.verify);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserFactor', 'getFactor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFactorTransactionStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFactorTransactionStatus(userFactorUserId, userFactorFactorId, userFactorTransactionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._embedded);
                assert.equal('object', typeof data.response._links);
                assert.equal('string', data.response.expiresAt);
                assert.equal('EXPIRED', data.response.factorResult);
                assert.equal('string', data.response.factorResultMessage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserFactor', 'getFactorTransactionStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let networkZoneZoneId = 'fakedata';
    const networkZoneCreateNetworkZoneBodyParam = {
      _links: {},
      asns: [
        'string'
      ],
      created: 'string',
      gateways: [
        {
          type: 'RANGE',
          value: 'string'
        }
      ],
      id: 'string',
      lastUpdated: 'string',
      locations: [
        {
          country: 'string',
          region: 'string'
        }
      ],
      name: 'string',
      proxies: [
        {
          type: 'RANGE',
          value: 'string'
        }
      ],
      proxyType: 'string',
      status: 'INACTIVE',
      system: true,
      type: 'IP',
      usage: 'POLICY'
    };
    describe('#createNetworkZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkZone(networkZoneCreateNetworkZoneBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal(true, Array.isArray(data.response.asns));
                assert.equal('string', data.response.created);
                assert.equal(true, Array.isArray(data.response.gateways));
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(true, Array.isArray(data.response.locations));
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.proxies));
                assert.equal('string', data.response.proxyType);
                assert.equal('ACTIVE', data.response.status);
                assert.equal(true, data.response.system);
                assert.equal('IP', data.response.type);
                assert.equal('BLOCKLIST', data.response.usage);
              } else {
                runCommonAsserts(data, error);
              }
              networkZoneZoneId = data.response.id;
              saveMockData('NetworkZone', 'createNetworkZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateNetworkZone - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activateNetworkZone(networkZoneZoneId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkZone', 'activateNetworkZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateNetworkZone - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deactivateNetworkZone(networkZoneZoneId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkZone', 'deactivateNetworkZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworkZones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listNetworkZones(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkZone', 'listNetworkZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkZoneUpdateNetworkZoneBodyParam = {
      _links: {},
      asns: [
        'string'
      ],
      created: 'string',
      gateways: [
        {
          type: 'RANGE',
          value: 'string'
        }
      ],
      id: 'string',
      lastUpdated: 'string',
      locations: [
        {
          country: 'string',
          region: 'string'
        }
      ],
      name: 'string',
      proxies: [
        {
          type: 'RANGE',
          value: 'string'
        }
      ],
      proxyType: 'string',
      status: 'INACTIVE',
      system: false,
      type: 'DYNAMIC',
      usage: 'BLOCKLIST'
    };
    describe('#updateNetworkZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkZone(networkZoneZoneId, networkZoneUpdateNetworkZoneBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkZone', 'updateNetworkZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkZone(networkZoneZoneId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response._links);
                assert.equal(true, Array.isArray(data.response.asns));
                assert.equal('string', data.response.created);
                assert.equal(true, Array.isArray(data.response.gateways));
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(true, Array.isArray(data.response.locations));
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.proxies));
                assert.equal('string', data.response.proxyType);
                assert.equal('INACTIVE', data.response.status);
                assert.equal(false, data.response.system);
                assert.equal('IP', data.response.type);
                assert.equal('BLOCKLIST', data.response.usage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkZone', 'getNetworkZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApplication(applicationAppId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'deleteApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeCsrFromApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeCsrFromApplication(applicationAppId, applicationCsrId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'revokeCsrFromApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeScopeConsentGrant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeScopeConsentGrant(applicationAppId, applicationGrantId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'revokeScopeConsentGrant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationGroupAssignment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApplicationGroupAssignment(applicationAppId, applicationGroupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'deleteApplicationGroupAssignment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeOAuth2TokensForApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeOAuth2TokensForApplication(applicationAppId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'revokeOAuth2TokensForApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeOAuth2TokenForApplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeOAuth2TokenForApplication(applicationAppId, applicationTokenId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'revokeOAuth2TokenForApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApplicationUser(applicationAppId, applicationUserId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'deleteApplicationUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthorizationServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAuthorizationServer(authorizationServerAuthServerId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'deleteAuthorizationServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOAuth2Claim - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOAuth2Claim(authorizationServerAuthServerId, authorizationServerClaimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'deleteOAuth2Claim', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeRefreshTokensForAuthorizationServerAndClient - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeRefreshTokensForAuthorizationServerAndClient(authorizationServerAuthServerId, authorizationServerClientId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'revokeRefreshTokensForAuthorizationServerAndClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeRefreshTokenForAuthorizationServerAndClient - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeRefreshTokenForAuthorizationServerAndClient(authorizationServerAuthServerId, authorizationServerClientId, authorizationServerTokenId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'revokeRefreshTokenForAuthorizationServerAndClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthorizationServerPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAuthorizationServerPolicy(authorizationServerAuthServerId, authorizationServerPolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'deleteAuthorizationServerPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthorizationServerPolicyRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAuthorizationServerPolicyRule(authorizationServerPolicyId, authorizationServerAuthServerId, authorizationServerRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'deleteAuthorizationServerPolicyRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOAuth2Scope - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOAuth2Scope(authorizationServerAuthServerId, authorizationServerScopeId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationServer', 'deleteOAuth2Scope', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEventHook - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteEventHook(eventHookEventHookId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventHook', 'deleteEventHook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroupRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGroupRule(groupRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'deleteGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGroup(groupGroupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'deleteGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeRoleFromGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeRoleFromGroup(groupGroupId, groupRoleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'removeRoleFromGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeApplicationTargetFromApplicationAdministratorRoleGivenToGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeApplicationTargetFromApplicationAdministratorRoleGivenToGroup(groupGroupId, groupRoleId, groupAppName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'removeApplicationTargetFromApplicationAdministratorRoleGivenToGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeApplicationTargetFromAdministratorRoleGivenToGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeApplicationTargetFromAdministratorRoleGivenToGroup(groupGroupId, groupRoleId, groupAppName, groupApplicationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'removeApplicationTargetFromAdministratorRoleGivenToGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeGroupTargetFromGroupAdministratorRoleGivenToGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeGroupTargetFromGroupAdministratorRoleGivenToGroup(groupGroupId, groupRoleId, groupTargetGroupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'removeGroupTargetFromGroupAdministratorRoleGivenToGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeUserFromGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeUserFromGroup(groupGroupId, groupUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'removeUserFromGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityProviderKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIdentityProviderKey(identityProviderKeyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'deleteIdentityProviderKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityProvider - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIdentityProvider(identityProviderIdpId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'deleteIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeCsrForIdentityProvider - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeCsrForIdentityProvider(identityProviderIdpId, identityProviderCsrId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'revokeCsrForIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unlinkUserFromIdentityProvider - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.unlinkUserFromIdentityProvider(identityProviderIdpId, identityProviderUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityProvider', 'unlinkUserFromIdentityProvider', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInlineHook - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteInlineHook(inlineHookInlineHookId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InlineHook', 'deleteInlineHook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLinkedObjectDefinition - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteLinkedObjectDefinition(linkedObjectLinkedObjectName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkedObject', 'deleteLinkedObjectDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUserType(userTypeTypeId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserType', 'deleteUserType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePolicy(policyPolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'deletePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePolicyRule(policyPolicyId, policyRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'deletePolicyRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#endSession - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.endSession(sessionSessionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Session', 'endSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSmsTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSmsTemplate(templateTemplateId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'deleteSmsTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrigin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOrigin(trustedOriginTrustedOriginId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedOrigin', 'deleteOrigin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateOrDeleteUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deactivateOrDeleteUser(userUserId, userSendEmail, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'deactivateOrDeleteUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeGrantsForUserAndClient - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeGrantsForUserAndClient(userUserId, userClientId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'revokeGrantsForUserAndClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeTokensForUserAndClient - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeTokensForUserAndClient(userUserId, userClientId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'revokeTokensForUserAndClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeTokenForUserAndClient - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeTokenForUserAndClient(userUserId, userClientId, userTokenId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'revokeTokenForUserAndClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeUserGrants - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeUserGrants(userUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'revokeUserGrants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeUserGrant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeUserGrant(userUserId, userGrantId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'revokeUserGrant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeLinkedObjectForUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeLinkedObjectForUser(userUserId, userRelationshipName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'removeLinkedObjectForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeRoleFromUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeRoleFromUser(userUserId, userRoleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'removeRoleFromUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeApplicationTargetFromApplicationAdministratorRoleForUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeApplicationTargetFromApplicationAdministratorRoleForUser(userUserId, userRoleId, userAppName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'removeApplicationTargetFromApplicationAdministratorRoleForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeApplicationTargetFromAdministratorRoleForUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeApplicationTargetFromAdministratorRoleForUser(userUserId, userRoleId, userAppName, userApplicationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'removeApplicationTargetFromAdministratorRoleForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeGroupTargetFromRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeGroupTargetFromRole(userUserId, userRoleId, userGroupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'removeGroupTargetFromRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clearUserSessions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.clearUserSessions(userUserId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'clearUserSessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFactor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteFactor(userFactorUserId, userFactorFactorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserFactor', 'deleteFactor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkZone - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkZone(networkZoneZoneId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-okta-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkZone', 'deleteNetworkZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
