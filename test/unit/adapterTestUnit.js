/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-okta',
      type: 'Okta',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Okta = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Okta Adapter Test', () => {
  describe('Okta Class Tests', () => {
    const a = new Okta(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('okta'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('okta'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Okta', pronghornDotJson.export);
          assert.equal('Okta', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-okta', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('okta'));
          assert.equal('Okta', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-okta', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-okta', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#listApplications - errors', () => {
      it('should have a listApplications function', (done) => {
        try {
          assert.equal(true, typeof a.listApplications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApplication - errors', () => {
      it('should have a createApplication function', (done) => {
        try {
          assert.equal(true, typeof a.createApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing application', (done) => {
        try {
          a.createApplication(null, null, (data, error) => {
            try {
              const displayE = 'application is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplication - errors', () => {
      it('should have a deleteApplication function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.deleteApplication(null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplication - errors', () => {
      it('should have a getApplication function', (done) => {
        try {
          assert.equal(true, typeof a.getApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.getApplication(null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateApplication - errors', () => {
      it('should have a updateApplication function', (done) => {
        try {
          assert.equal(true, typeof a.updateApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.updateApplication(null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing application', (done) => {
        try {
          a.updateApplication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'application is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCsrsForApplication - errors', () => {
      it('should have a listCsrsForApplication function', (done) => {
        try {
          assert.equal(true, typeof a.listCsrsForApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.listCsrsForApplication(null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listCsrsForApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateCsrForApplication - errors', () => {
      it('should have a generateCsrForApplication function', (done) => {
        try {
          assert.equal(true, typeof a.generateCsrForApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.generateCsrForApplication(null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-generateCsrForApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metadata', (done) => {
        try {
          a.generateCsrForApplication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'metadata is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-generateCsrForApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeCsrFromApplication - errors', () => {
      it('should have a revokeCsrFromApplication function', (done) => {
        try {
          assert.equal(true, typeof a.revokeCsrFromApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.revokeCsrFromApplication(null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeCsrFromApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing csrId', (done) => {
        try {
          a.revokeCsrFromApplication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'csrId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeCsrFromApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsrForApplication - errors', () => {
      it('should have a getCsrForApplication function', (done) => {
        try {
          assert.equal(true, typeof a.getCsrForApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.getCsrForApplication(null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getCsrForApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing csrId', (done) => {
        try {
          a.getCsrForApplication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'csrId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getCsrForApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postApiV1AppsAppIdCredentialsCsrsCsrIdLifecyclePublish - errors', () => {
      it('should have a postApiV1AppsAppIdCredentialsCsrsCsrIdLifecyclePublish function', (done) => {
        try {
          assert.equal(true, typeof a.postApiV1AppsAppIdCredentialsCsrsCsrIdLifecyclePublish === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.postApiV1AppsAppIdCredentialsCsrsCsrIdLifecyclePublish(null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-postApiV1AppsAppIdCredentialsCsrsCsrIdLifecyclePublish', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing csrId', (done) => {
        try {
          a.postApiV1AppsAppIdCredentialsCsrsCsrIdLifecyclePublish('fakeparam', null, (data, error) => {
            try {
              const displayE = 'csrId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-postApiV1AppsAppIdCredentialsCsrsCsrIdLifecyclePublish', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApplicationKeys - errors', () => {
      it('should have a listApplicationKeys function', (done) => {
        try {
          assert.equal(true, typeof a.listApplicationKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.listApplicationKeys(null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listApplicationKeys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateApplicationKey - errors', () => {
      it('should have a generateApplicationKey function', (done) => {
        try {
          assert.equal(true, typeof a.generateApplicationKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.generateApplicationKey(null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-generateApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationKey - errors', () => {
      it('should have a getApplicationKey function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.getApplicationKey(null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyId', (done) => {
        try {
          a.getApplicationKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'keyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cloneApplicationKey - errors', () => {
      it('should have a cloneApplicationKey function', (done) => {
        try {
          assert.equal(true, typeof a.cloneApplicationKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.cloneApplicationKey(null, null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-cloneApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyId', (done) => {
        try {
          a.cloneApplicationKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'keyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-cloneApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetAid', (done) => {
        try {
          a.cloneApplicationKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'targetAid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-cloneApplicationKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listScopeConsentGrants - errors', () => {
      it('should have a listScopeConsentGrants function', (done) => {
        try {
          assert.equal(true, typeof a.listScopeConsentGrants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.listScopeConsentGrants(null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listScopeConsentGrants', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#grantConsentToScope - errors', () => {
      it('should have a grantConsentToScope function', (done) => {
        try {
          assert.equal(true, typeof a.grantConsentToScope === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.grantConsentToScope(null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-grantConsentToScope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oAuth2ScopeConsentGrant', (done) => {
        try {
          a.grantConsentToScope('fakeparam', null, (data, error) => {
            try {
              const displayE = 'oAuth2ScopeConsentGrant is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-grantConsentToScope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeScopeConsentGrant - errors', () => {
      it('should have a revokeScopeConsentGrant function', (done) => {
        try {
          assert.equal(true, typeof a.revokeScopeConsentGrant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.revokeScopeConsentGrant(null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeScopeConsentGrant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing grantId', (done) => {
        try {
          a.revokeScopeConsentGrant('fakeparam', null, (data, error) => {
            try {
              const displayE = 'grantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeScopeConsentGrant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScopeConsentGrant - errors', () => {
      it('should have a getScopeConsentGrant function', (done) => {
        try {
          assert.equal(true, typeof a.getScopeConsentGrant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.getScopeConsentGrant(null, null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getScopeConsentGrant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing grantId', (done) => {
        try {
          a.getScopeConsentGrant('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'grantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getScopeConsentGrant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApplicationGroupAssignments - errors', () => {
      it('should have a listApplicationGroupAssignments function', (done) => {
        try {
          assert.equal(true, typeof a.listApplicationGroupAssignments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.listApplicationGroupAssignments(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listApplicationGroupAssignments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationGroupAssignment - errors', () => {
      it('should have a deleteApplicationGroupAssignment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplicationGroupAssignment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.deleteApplicationGroupAssignment(null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteApplicationGroupAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.deleteApplicationGroupAssignment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteApplicationGroupAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationGroupAssignment - errors', () => {
      it('should have a getApplicationGroupAssignment function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationGroupAssignment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.getApplicationGroupAssignment(null, null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getApplicationGroupAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.getApplicationGroupAssignment('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getApplicationGroupAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApplicationGroupAssignment - errors', () => {
      it('should have a createApplicationGroupAssignment function', (done) => {
        try {
          assert.equal(true, typeof a.createApplicationGroupAssignment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.createApplicationGroupAssignment(null, null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createApplicationGroupAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.createApplicationGroupAssignment('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createApplicationGroupAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateApplication - errors', () => {
      it('should have a activateApplication function', (done) => {
        try {
          assert.equal(true, typeof a.activateApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.activateApplication(null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateApplication - errors', () => {
      it('should have a deactivateApplication function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.deactivateApplication(null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivateApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeOAuth2TokensForApplication - errors', () => {
      it('should have a revokeOAuth2TokensForApplication function', (done) => {
        try {
          assert.equal(true, typeof a.revokeOAuth2TokensForApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.revokeOAuth2TokensForApplication(null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeOAuth2TokensForApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listOAuth2TokensForApplication - errors', () => {
      it('should have a listOAuth2TokensForApplication function', (done) => {
        try {
          assert.equal(true, typeof a.listOAuth2TokensForApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.listOAuth2TokensForApplication(null, null, null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listOAuth2TokensForApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeOAuth2TokenForApplication - errors', () => {
      it('should have a revokeOAuth2TokenForApplication function', (done) => {
        try {
          assert.equal(true, typeof a.revokeOAuth2TokenForApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.revokeOAuth2TokenForApplication(null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeOAuth2TokenForApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tokenId', (done) => {
        try {
          a.revokeOAuth2TokenForApplication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tokenId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeOAuth2TokenForApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOAuth2TokenForApplication - errors', () => {
      it('should have a getOAuth2TokenForApplication function', (done) => {
        try {
          assert.equal(true, typeof a.getOAuth2TokenForApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.getOAuth2TokenForApplication(null, null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getOAuth2TokenForApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tokenId', (done) => {
        try {
          a.getOAuth2TokenForApplication('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tokenId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getOAuth2TokenForApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApplicationUsers - errors', () => {
      it('should have a listApplicationUsers function', (done) => {
        try {
          assert.equal(true, typeof a.listApplicationUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.listApplicationUsers(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listApplicationUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignUserToApplication - errors', () => {
      it('should have a assignUserToApplication function', (done) => {
        try {
          assert.equal(true, typeof a.assignUserToApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.assignUserToApplication(null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-assignUserToApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appUser', (done) => {
        try {
          a.assignUserToApplication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'appUser is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-assignUserToApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationUser - errors', () => {
      it('should have a deleteApplicationUser function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplicationUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.deleteApplicationUser(null, null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteApplicationUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.deleteApplicationUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteApplicationUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationUser - errors', () => {
      it('should have a getApplicationUser function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.getApplicationUser(null, null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getApplicationUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.getApplicationUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getApplicationUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateApplicationUser - errors', () => {
      it('should have a updateApplicationUser function', (done) => {
        try {
          assert.equal(true, typeof a.updateApplicationUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.updateApplicationUser(null, null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateApplicationUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.updateApplicationUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateApplicationUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appUser', (done) => {
        try {
          a.updateApplicationUser('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'appUser is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateApplicationUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthorizationServers - errors', () => {
      it('should have a listAuthorizationServers function', (done) => {
        try {
          assert.equal(true, typeof a.listAuthorizationServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthorizationServer - errors', () => {
      it('should have a createAuthorizationServer function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthorizationServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authorizationServer', (done) => {
        try {
          a.createAuthorizationServer(null, (data, error) => {
            try {
              const displayE = 'authorizationServer is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createAuthorizationServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthorizationServer - errors', () => {
      it('should have a deleteAuthorizationServer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthorizationServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.deleteAuthorizationServer(null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteAuthorizationServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthorizationServer - errors', () => {
      it('should have a getAuthorizationServer function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthorizationServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.getAuthorizationServer(null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getAuthorizationServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAuthorizationServer - errors', () => {
      it('should have a updateAuthorizationServer function', (done) => {
        try {
          assert.equal(true, typeof a.updateAuthorizationServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.updateAuthorizationServer(null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateAuthorizationServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authorizationServer', (done) => {
        try {
          a.updateAuthorizationServer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'authorizationServer is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateAuthorizationServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listOAuth2Claims - errors', () => {
      it('should have a listOAuth2Claims function', (done) => {
        try {
          assert.equal(true, typeof a.listOAuth2Claims === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.listOAuth2Claims(null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listOAuth2Claims', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOAuth2Claim - errors', () => {
      it('should have a createOAuth2Claim function', (done) => {
        try {
          assert.equal(true, typeof a.createOAuth2Claim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.createOAuth2Claim(null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createOAuth2Claim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oAuth2Claim', (done) => {
        try {
          a.createOAuth2Claim('fakeparam', null, (data, error) => {
            try {
              const displayE = 'oAuth2Claim is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createOAuth2Claim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOAuth2Claim - errors', () => {
      it('should have a deleteOAuth2Claim function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOAuth2Claim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.deleteOAuth2Claim(null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteOAuth2Claim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing claimId', (done) => {
        try {
          a.deleteOAuth2Claim('fakeparam', null, (data, error) => {
            try {
              const displayE = 'claimId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteOAuth2Claim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOAuth2Claim - errors', () => {
      it('should have a getOAuth2Claim function', (done) => {
        try {
          assert.equal(true, typeof a.getOAuth2Claim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.getOAuth2Claim(null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getOAuth2Claim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing claimId', (done) => {
        try {
          a.getOAuth2Claim('fakeparam', null, (data, error) => {
            try {
              const displayE = 'claimId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getOAuth2Claim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOAuth2Claim - errors', () => {
      it('should have a updateOAuth2Claim function', (done) => {
        try {
          assert.equal(true, typeof a.updateOAuth2Claim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.updateOAuth2Claim(null, null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateOAuth2Claim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing claimId', (done) => {
        try {
          a.updateOAuth2Claim('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'claimId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateOAuth2Claim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oAuth2Claim', (done) => {
        try {
          a.updateOAuth2Claim('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'oAuth2Claim is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateOAuth2Claim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listOAuth2ClientsForAuthorizationServer - errors', () => {
      it('should have a listOAuth2ClientsForAuthorizationServer function', (done) => {
        try {
          assert.equal(true, typeof a.listOAuth2ClientsForAuthorizationServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.listOAuth2ClientsForAuthorizationServer(null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listOAuth2ClientsForAuthorizationServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeRefreshTokensForAuthorizationServerAndClient - errors', () => {
      it('should have a revokeRefreshTokensForAuthorizationServerAndClient function', (done) => {
        try {
          assert.equal(true, typeof a.revokeRefreshTokensForAuthorizationServerAndClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.revokeRefreshTokensForAuthorizationServerAndClient(null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeRefreshTokensForAuthorizationServerAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.revokeRefreshTokensForAuthorizationServerAndClient('fakeparam', null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeRefreshTokensForAuthorizationServerAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRefreshTokensForAuthorizationServerAndClient - errors', () => {
      it('should have a listRefreshTokensForAuthorizationServerAndClient function', (done) => {
        try {
          assert.equal(true, typeof a.listRefreshTokensForAuthorizationServerAndClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.listRefreshTokensForAuthorizationServerAndClient(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listRefreshTokensForAuthorizationServerAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.listRefreshTokensForAuthorizationServerAndClient('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listRefreshTokensForAuthorizationServerAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeRefreshTokenForAuthorizationServerAndClient - errors', () => {
      it('should have a revokeRefreshTokenForAuthorizationServerAndClient function', (done) => {
        try {
          assert.equal(true, typeof a.revokeRefreshTokenForAuthorizationServerAndClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.revokeRefreshTokenForAuthorizationServerAndClient(null, null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeRefreshTokenForAuthorizationServerAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.revokeRefreshTokenForAuthorizationServerAndClient('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeRefreshTokenForAuthorizationServerAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tokenId', (done) => {
        try {
          a.revokeRefreshTokenForAuthorizationServerAndClient('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'tokenId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeRefreshTokenForAuthorizationServerAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRefreshTokenForAuthorizationServerAndClient - errors', () => {
      it('should have a getRefreshTokenForAuthorizationServerAndClient function', (done) => {
        try {
          assert.equal(true, typeof a.getRefreshTokenForAuthorizationServerAndClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.getRefreshTokenForAuthorizationServerAndClient(null, null, null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getRefreshTokenForAuthorizationServerAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.getRefreshTokenForAuthorizationServerAndClient('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getRefreshTokenForAuthorizationServerAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tokenId', (done) => {
        try {
          a.getRefreshTokenForAuthorizationServerAndClient('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tokenId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getRefreshTokenForAuthorizationServerAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthorizationServerKeys - errors', () => {
      it('should have a listAuthorizationServerKeys function', (done) => {
        try {
          assert.equal(true, typeof a.listAuthorizationServerKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.listAuthorizationServerKeys(null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listAuthorizationServerKeys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rotateAuthorizationServerKeys - errors', () => {
      it('should have a rotateAuthorizationServerKeys function', (done) => {
        try {
          assert.equal(true, typeof a.rotateAuthorizationServerKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.rotateAuthorizationServerKeys(null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-rotateAuthorizationServerKeys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing use', (done) => {
        try {
          a.rotateAuthorizationServerKeys('fakeparam', null, (data, error) => {
            try {
              const displayE = 'use is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-rotateAuthorizationServerKeys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateAuthorizationServer - errors', () => {
      it('should have a activateAuthorizationServer function', (done) => {
        try {
          assert.equal(true, typeof a.activateAuthorizationServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.activateAuthorizationServer(null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateAuthorizationServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateAuthorizationServer - errors', () => {
      it('should have a deactivateAuthorizationServer function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateAuthorizationServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.deactivateAuthorizationServer(null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivateAuthorizationServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthorizationServerPolicies - errors', () => {
      it('should have a listAuthorizationServerPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.listAuthorizationServerPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.listAuthorizationServerPolicies(null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listAuthorizationServerPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthorizationServerPolicy - errors', () => {
      it('should have a createAuthorizationServerPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthorizationServerPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.createAuthorizationServerPolicy(null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createAuthorizationServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policy', (done) => {
        try {
          a.createAuthorizationServerPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'policy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createAuthorizationServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthorizationServerPolicy - errors', () => {
      it('should have a deleteAuthorizationServerPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthorizationServerPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.deleteAuthorizationServerPolicy(null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteAuthorizationServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deleteAuthorizationServerPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteAuthorizationServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthorizationServerPolicy - errors', () => {
      it('should have a getAuthorizationServerPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthorizationServerPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.getAuthorizationServerPolicy(null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getAuthorizationServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.getAuthorizationServerPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getAuthorizationServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAuthorizationServerPolicy - errors', () => {
      it('should have a updateAuthorizationServerPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateAuthorizationServerPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.updateAuthorizationServerPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateAuthorizationServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updateAuthorizationServerPolicy('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateAuthorizationServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policy', (done) => {
        try {
          a.updateAuthorizationServerPolicy('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateAuthorizationServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateAuthorizationServerPolicy - errors', () => {
      it('should have a activateAuthorizationServerPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.activateAuthorizationServerPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.activateAuthorizationServerPolicy(null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateAuthorizationServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.activateAuthorizationServerPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateAuthorizationServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateAuthorizationServerPolicy - errors', () => {
      it('should have a deactivateAuthorizationServerPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateAuthorizationServerPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.deactivateAuthorizationServerPolicy(null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivateAuthorizationServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deactivateAuthorizationServerPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivateAuthorizationServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuthorizationServerPolicyRules - errors', () => {
      it('should have a listAuthorizationServerPolicyRules function', (done) => {
        try {
          assert.equal(true, typeof a.listAuthorizationServerPolicyRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.listAuthorizationServerPolicyRules(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listAuthorizationServerPolicyRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.listAuthorizationServerPolicyRules('fakeparam', null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listAuthorizationServerPolicyRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthorizationServerPolicyRule - errors', () => {
      it('should have a createAuthorizationServerPolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthorizationServerPolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.createAuthorizationServerPolicyRule(null, null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.createAuthorizationServerPolicyRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyRule', (done) => {
        try {
          a.createAuthorizationServerPolicyRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthorizationServerPolicyRule - errors', () => {
      it('should have a deleteAuthorizationServerPolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthorizationServerPolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deleteAuthorizationServerPolicyRule(null, null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.deleteAuthorizationServerPolicyRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deleteAuthorizationServerPolicyRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthorizationServerPolicyRule - errors', () => {
      it('should have a getAuthorizationServerPolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthorizationServerPolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.getAuthorizationServerPolicyRule(null, null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.getAuthorizationServerPolicyRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.getAuthorizationServerPolicyRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAuthorizationServerPolicyRule - errors', () => {
      it('should have a updateAuthorizationServerPolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateAuthorizationServerPolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updateAuthorizationServerPolicyRule(null, null, null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.updateAuthorizationServerPolicyRule('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updateAuthorizationServerPolicyRule('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyRule', (done) => {
        try {
          a.updateAuthorizationServerPolicyRule('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateAuthorizationServerPolicyRule - errors', () => {
      it('should have a activateAuthorizationServerPolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.activateAuthorizationServerPolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.activateAuthorizationServerPolicyRule(null, null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.activateAuthorizationServerPolicyRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.activateAuthorizationServerPolicyRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateAuthorizationServerPolicyRule - errors', () => {
      it('should have a deactivateAuthorizationServerPolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateAuthorizationServerPolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.deactivateAuthorizationServerPolicyRule(null, null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivateAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deactivateAuthorizationServerPolicyRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivateAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deactivateAuthorizationServerPolicyRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivateAuthorizationServerPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listOAuth2Scopes - errors', () => {
      it('should have a listOAuth2Scopes function', (done) => {
        try {
          assert.equal(true, typeof a.listOAuth2Scopes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.listOAuth2Scopes(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listOAuth2Scopes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOAuth2Scope - errors', () => {
      it('should have a createOAuth2Scope function', (done) => {
        try {
          assert.equal(true, typeof a.createOAuth2Scope === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.createOAuth2Scope(null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createOAuth2Scope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oAuth2Scope', (done) => {
        try {
          a.createOAuth2Scope('fakeparam', null, (data, error) => {
            try {
              const displayE = 'oAuth2Scope is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createOAuth2Scope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOAuth2Scope - errors', () => {
      it('should have a deleteOAuth2Scope function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOAuth2Scope === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.deleteOAuth2Scope(null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteOAuth2Scope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scopeId', (done) => {
        try {
          a.deleteOAuth2Scope('fakeparam', null, (data, error) => {
            try {
              const displayE = 'scopeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteOAuth2Scope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOAuth2Scope - errors', () => {
      it('should have a getOAuth2Scope function', (done) => {
        try {
          assert.equal(true, typeof a.getOAuth2Scope === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.getOAuth2Scope(null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getOAuth2Scope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scopeId', (done) => {
        try {
          a.getOAuth2Scope('fakeparam', null, (data, error) => {
            try {
              const displayE = 'scopeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getOAuth2Scope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOAuth2Scope - errors', () => {
      it('should have a updateOAuth2Scope function', (done) => {
        try {
          assert.equal(true, typeof a.updateOAuth2Scope === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authServerId', (done) => {
        try {
          a.updateOAuth2Scope(null, null, null, (data, error) => {
            try {
              const displayE = 'authServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateOAuth2Scope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scopeId', (done) => {
        try {
          a.updateOAuth2Scope('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'scopeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateOAuth2Scope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing oAuth2Scope', (done) => {
        try {
          a.updateOAuth2Scope('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'oAuth2Scope is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateOAuth2Scope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listEventHooks - errors', () => {
      it('should have a listEventHooks function', (done) => {
        try {
          assert.equal(true, typeof a.listEventHooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createEventHook - errors', () => {
      it('should have a createEventHook function', (done) => {
        try {
          assert.equal(true, typeof a.createEventHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventHook', (done) => {
        try {
          a.createEventHook(null, (data, error) => {
            try {
              const displayE = 'eventHook is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createEventHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEventHook - errors', () => {
      it('should have a deleteEventHook function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEventHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventHookId', (done) => {
        try {
          a.deleteEventHook(null, (data, error) => {
            try {
              const displayE = 'eventHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteEventHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventHook - errors', () => {
      it('should have a getEventHook function', (done) => {
        try {
          assert.equal(true, typeof a.getEventHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventHookId', (done) => {
        try {
          a.getEventHook(null, (data, error) => {
            try {
              const displayE = 'eventHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getEventHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateEventHook - errors', () => {
      it('should have a updateEventHook function', (done) => {
        try {
          assert.equal(true, typeof a.updateEventHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventHookId', (done) => {
        try {
          a.updateEventHook(null, null, (data, error) => {
            try {
              const displayE = 'eventHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateEventHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventHook', (done) => {
        try {
          a.updateEventHook('fakeparam', null, (data, error) => {
            try {
              const displayE = 'eventHook is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateEventHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateEventHook - errors', () => {
      it('should have a activateEventHook function', (done) => {
        try {
          assert.equal(true, typeof a.activateEventHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventHookId', (done) => {
        try {
          a.activateEventHook(null, (data, error) => {
            try {
              const displayE = 'eventHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateEventHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateEventHook - errors', () => {
      it('should have a deactivateEventHook function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateEventHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventHookId', (done) => {
        try {
          a.deactivateEventHook(null, (data, error) => {
            try {
              const displayE = 'eventHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivateEventHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verifyEventHook - errors', () => {
      it('should have a verifyEventHook function', (done) => {
        try {
          assert.equal(true, typeof a.verifyEventHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventHookId', (done) => {
        try {
          a.verifyEventHook(null, (data, error) => {
            try {
              const displayE = 'eventHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-verifyEventHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFeatures - errors', () => {
      it('should have a listFeatures function', (done) => {
        try {
          assert.equal(true, typeof a.listFeatures === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFeature - errors', () => {
      it('should have a getFeature function', (done) => {
        try {
          assert.equal(true, typeof a.getFeature === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing featureId', (done) => {
        try {
          a.getFeature(null, (data, error) => {
            try {
              const displayE = 'featureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getFeature', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFeatureDependencies - errors', () => {
      it('should have a listFeatureDependencies function', (done) => {
        try {
          assert.equal(true, typeof a.listFeatureDependencies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing featureId', (done) => {
        try {
          a.listFeatureDependencies(null, (data, error) => {
            try {
              const displayE = 'featureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listFeatureDependencies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFeatureDependents - errors', () => {
      it('should have a listFeatureDependents function', (done) => {
        try {
          assert.equal(true, typeof a.listFeatureDependents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing featureId', (done) => {
        try {
          a.listFeatureDependents(null, (data, error) => {
            try {
              const displayE = 'featureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listFeatureDependents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFeatureLifecycle - errors', () => {
      it('should have a updateFeatureLifecycle function', (done) => {
        try {
          assert.equal(true, typeof a.updateFeatureLifecycle === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing featureId', (done) => {
        try {
          a.updateFeatureLifecycle(null, null, null, (data, error) => {
            try {
              const displayE = 'featureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateFeatureLifecycle', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lifecycle', (done) => {
        try {
          a.updateFeatureLifecycle('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'lifecycle is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateFeatureLifecycle', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroups - errors', () => {
      it('should have a listGroups function', (done) => {
        try {
          assert.equal(true, typeof a.listGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGroup - errors', () => {
      it('should have a createGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing group', (done) => {
        try {
          a.createGroup(null, (data, error) => {
            try {
              const displayE = 'group is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroupRules - errors', () => {
      it('should have a listGroupRules function', (done) => {
        try {
          assert.equal(true, typeof a.listGroupRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGroupRule - errors', () => {
      it('should have a createGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.createGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupRule', (done) => {
        try {
          a.createGroupRule(null, (data, error) => {
            try {
              const displayE = 'groupRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroupRule - errors', () => {
      it('should have a deleteGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deleteGroupRule(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupRule - errors', () => {
      it('should have a getGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.getGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.getGroupRule(null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGroupRule - errors', () => {
      it('should have a updateGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updateGroupRule(null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupRule', (done) => {
        try {
          a.updateGroupRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateGroupRule - errors', () => {
      it('should have a activateGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.activateGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.activateGroupRule(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateGroupRule - errors', () => {
      it('should have a deactivateGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deactivateGroupRule(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivateGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroup - errors', () => {
      it('should have a deleteGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.deleteGroup(null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroup - errors', () => {
      it('should have a getGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.getGroup(null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGroup - errors', () => {
      it('should have a updateGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.updateGroup(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing group', (done) => {
        try {
          a.updateGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'group is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAssignedApplicationsForGroup - errors', () => {
      it('should have a listAssignedApplicationsForGroup function', (done) => {
        try {
          assert.equal(true, typeof a.listAssignedApplicationsForGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.listAssignedApplicationsForGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listAssignedApplicationsForGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroupAssignedRoles - errors', () => {
      it('should have a listGroupAssignedRoles function', (done) => {
        try {
          assert.equal(true, typeof a.listGroupAssignedRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.listGroupAssignedRoles(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listGroupAssignedRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignRoleToGroup - errors', () => {
      it('should have a assignRoleToGroup function', (done) => {
        try {
          assert.equal(true, typeof a.assignRoleToGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing assignRoleRequest', (done) => {
        try {
          a.assignRoleToGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'assignRoleRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-assignRoleToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.assignRoleToGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-assignRoleToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeRoleFromGroup - errors', () => {
      it('should have a removeRoleFromGroup function', (done) => {
        try {
          assert.equal(true, typeof a.removeRoleFromGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.removeRoleFromGroup(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeRoleFromGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.removeRoleFromGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeRoleFromGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRole - errors', () => {
      it('should have a getRole function', (done) => {
        try {
          assert.equal(true, typeof a.getRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.getRole(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.getRole('fakeparam', null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApplicationTargetsForApplicationAdministratorRoleForGroup - errors', () => {
      it('should have a listApplicationTargetsForApplicationAdministratorRoleForGroup function', (done) => {
        try {
          assert.equal(true, typeof a.listApplicationTargetsForApplicationAdministratorRoleForGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.listApplicationTargetsForApplicationAdministratorRoleForGroup(null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listApplicationTargetsForApplicationAdministratorRoleForGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.listApplicationTargetsForApplicationAdministratorRoleForGroup('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listApplicationTargetsForApplicationAdministratorRoleForGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeApplicationTargetFromApplicationAdministratorRoleGivenToGroup - errors', () => {
      it('should have a removeApplicationTargetFromApplicationAdministratorRoleGivenToGroup function', (done) => {
        try {
          assert.equal(true, typeof a.removeApplicationTargetFromApplicationAdministratorRoleGivenToGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.removeApplicationTargetFromApplicationAdministratorRoleGivenToGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeApplicationTargetFromApplicationAdministratorRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.removeApplicationTargetFromApplicationAdministratorRoleGivenToGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeApplicationTargetFromApplicationAdministratorRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appName', (done) => {
        try {
          a.removeApplicationTargetFromApplicationAdministratorRoleGivenToGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'appName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeApplicationTargetFromApplicationAdministratorRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addApplicationTargetToAdminRoleGivenToGroup - errors', () => {
      it('should have a addApplicationTargetToAdminRoleGivenToGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addApplicationTargetToAdminRoleGivenToGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.addApplicationTargetToAdminRoleGivenToGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addApplicationTargetToAdminRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.addApplicationTargetToAdminRoleGivenToGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addApplicationTargetToAdminRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appName', (done) => {
        try {
          a.addApplicationTargetToAdminRoleGivenToGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'appName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addApplicationTargetToAdminRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeApplicationTargetFromAdministratorRoleGivenToGroup - errors', () => {
      it('should have a removeApplicationTargetFromAdministratorRoleGivenToGroup function', (done) => {
        try {
          assert.equal(true, typeof a.removeApplicationTargetFromAdministratorRoleGivenToGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.removeApplicationTargetFromAdministratorRoleGivenToGroup(null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeApplicationTargetFromAdministratorRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.removeApplicationTargetFromAdministratorRoleGivenToGroup('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeApplicationTargetFromAdministratorRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appName', (done) => {
        try {
          a.removeApplicationTargetFromAdministratorRoleGivenToGroup('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'appName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeApplicationTargetFromAdministratorRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.removeApplicationTargetFromAdministratorRoleGivenToGroup('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeApplicationTargetFromAdministratorRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addApplicationInstanceTargetToAppAdminRoleGivenToGroup - errors', () => {
      it('should have a addApplicationInstanceTargetToAppAdminRoleGivenToGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addApplicationInstanceTargetToAppAdminRoleGivenToGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.addApplicationInstanceTargetToAppAdminRoleGivenToGroup(null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addApplicationInstanceTargetToAppAdminRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.addApplicationInstanceTargetToAppAdminRoleGivenToGroup('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addApplicationInstanceTargetToAppAdminRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appName', (done) => {
        try {
          a.addApplicationInstanceTargetToAppAdminRoleGivenToGroup('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'appName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addApplicationInstanceTargetToAppAdminRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.addApplicationInstanceTargetToAppAdminRoleGivenToGroup('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addApplicationInstanceTargetToAppAdminRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroupTargetsForGroupRole - errors', () => {
      it('should have a listGroupTargetsForGroupRole function', (done) => {
        try {
          assert.equal(true, typeof a.listGroupTargetsForGroupRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.listGroupTargetsForGroupRole(null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listGroupTargetsForGroupRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.listGroupTargetsForGroupRole('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listGroupTargetsForGroupRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeGroupTargetFromGroupAdministratorRoleGivenToGroup - errors', () => {
      it('should have a removeGroupTargetFromGroupAdministratorRoleGivenToGroup function', (done) => {
        try {
          assert.equal(true, typeof a.removeGroupTargetFromGroupAdministratorRoleGivenToGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.removeGroupTargetFromGroupAdministratorRoleGivenToGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeGroupTargetFromGroupAdministratorRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.removeGroupTargetFromGroupAdministratorRoleGivenToGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeGroupTargetFromGroupAdministratorRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetGroupId', (done) => {
        try {
          a.removeGroupTargetFromGroupAdministratorRoleGivenToGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'targetGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeGroupTargetFromGroupAdministratorRoleGivenToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addGroupTargetToGroupAdministratorRoleForGroup - errors', () => {
      it('should have a addGroupTargetToGroupAdministratorRoleForGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addGroupTargetToGroupAdministratorRoleForGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.addGroupTargetToGroupAdministratorRoleForGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addGroupTargetToGroupAdministratorRoleForGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.addGroupTargetToGroupAdministratorRoleForGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addGroupTargetToGroupAdministratorRoleForGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetGroupId', (done) => {
        try {
          a.addGroupTargetToGroupAdministratorRoleForGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'targetGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addGroupTargetToGroupAdministratorRoleForGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroupUsers - errors', () => {
      it('should have a listGroupUsers function', (done) => {
        try {
          assert.equal(true, typeof a.listGroupUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.listGroupUsers(null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listGroupUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeUserFromGroup - errors', () => {
      it('should have a removeUserFromGroup function', (done) => {
        try {
          assert.equal(true, typeof a.removeUserFromGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.removeUserFromGroup(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeUserFromGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.removeUserFromGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeUserFromGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addUserToGroup - errors', () => {
      it('should have a addUserToGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addUserToGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.addUserToGroup(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addUserToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.addUserToGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addUserToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIdentityProviders - errors', () => {
      it('should have a listIdentityProviders function', (done) => {
        try {
          assert.equal(true, typeof a.listIdentityProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIdentityProvider - errors', () => {
      it('should have a createIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.createIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing identityProvider', (done) => {
        try {
          a.createIdentityProvider(null, (data, error) => {
            try {
              const displayE = 'identityProvider is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIdentityProviderKeys - errors', () => {
      it('should have a listIdentityProviderKeys function', (done) => {
        try {
          assert.equal(true, typeof a.listIdentityProviderKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIdentityProviderKey - errors', () => {
      it('should have a createIdentityProviderKey function', (done) => {
        try {
          assert.equal(true, typeof a.createIdentityProviderKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jsonWebKey', (done) => {
        try {
          a.createIdentityProviderKey(null, (data, error) => {
            try {
              const displayE = 'jsonWebKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createIdentityProviderKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityProviderKey - errors', () => {
      it('should have a deleteIdentityProviderKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIdentityProviderKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyId', (done) => {
        try {
          a.deleteIdentityProviderKey(null, (data, error) => {
            try {
              const displayE = 'keyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteIdentityProviderKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityProviderKey - errors', () => {
      it('should have a getIdentityProviderKey function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityProviderKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyId', (done) => {
        try {
          a.getIdentityProviderKey(null, (data, error) => {
            try {
              const displayE = 'keyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getIdentityProviderKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityProvider - errors', () => {
      it('should have a deleteIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.deleteIdentityProvider(null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityProvider - errors', () => {
      it('should have a getIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.getIdentityProvider(null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIdentityProvider - errors', () => {
      it('should have a updateIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.updateIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.updateIdentityProvider(null, null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing identityProvider', (done) => {
        try {
          a.updateIdentityProvider('fakeparam', null, (data, error) => {
            try {
              const displayE = 'identityProvider is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCsrsForIdentityProvider - errors', () => {
      it('should have a listCsrsForIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.listCsrsForIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.listCsrsForIdentityProvider(null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listCsrsForIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateCsrForIdentityProvider - errors', () => {
      it('should have a generateCsrForIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.generateCsrForIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.generateCsrForIdentityProvider(null, null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-generateCsrForIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metadata', (done) => {
        try {
          a.generateCsrForIdentityProvider('fakeparam', null, (data, error) => {
            try {
              const displayE = 'metadata is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-generateCsrForIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeCsrForIdentityProvider - errors', () => {
      it('should have a revokeCsrForIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.revokeCsrForIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.revokeCsrForIdentityProvider(null, null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeCsrForIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing csrId', (done) => {
        try {
          a.revokeCsrForIdentityProvider('fakeparam', null, (data, error) => {
            try {
              const displayE = 'csrId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeCsrForIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCsrForIdentityProvider - errors', () => {
      it('should have a getCsrForIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.getCsrForIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.getCsrForIdentityProvider(null, null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getCsrForIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing csrId', (done) => {
        try {
          a.getCsrForIdentityProvider('fakeparam', null, (data, error) => {
            try {
              const displayE = 'csrId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getCsrForIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postApiV1IdpsIdpIdCredentialsCsrsCsrIdLifecyclePublish - errors', () => {
      it('should have a postApiV1IdpsIdpIdCredentialsCsrsCsrIdLifecyclePublish function', (done) => {
        try {
          assert.equal(true, typeof a.postApiV1IdpsIdpIdCredentialsCsrsCsrIdLifecyclePublish === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.postApiV1IdpsIdpIdCredentialsCsrsCsrIdLifecyclePublish(null, null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-postApiV1IdpsIdpIdCredentialsCsrsCsrIdLifecyclePublish', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing csrId', (done) => {
        try {
          a.postApiV1IdpsIdpIdCredentialsCsrsCsrIdLifecyclePublish('fakeparam', null, (data, error) => {
            try {
              const displayE = 'csrId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-postApiV1IdpsIdpIdCredentialsCsrsCsrIdLifecyclePublish', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIdentityProviderSigningKeys - errors', () => {
      it('should have a listIdentityProviderSigningKeys function', (done) => {
        try {
          assert.equal(true, typeof a.listIdentityProviderSigningKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.listIdentityProviderSigningKeys(null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listIdentityProviderSigningKeys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateIdentityProviderSigningKey - errors', () => {
      it('should have a generateIdentityProviderSigningKey function', (done) => {
        try {
          assert.equal(true, typeof a.generateIdentityProviderSigningKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.generateIdentityProviderSigningKey(null, null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-generateIdentityProviderSigningKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing validityYears', (done) => {
        try {
          a.generateIdentityProviderSigningKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'validityYears is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-generateIdentityProviderSigningKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityProviderSigningKey - errors', () => {
      it('should have a getIdentityProviderSigningKey function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityProviderSigningKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.getIdentityProviderSigningKey(null, null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getIdentityProviderSigningKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyId', (done) => {
        try {
          a.getIdentityProviderSigningKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'keyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getIdentityProviderSigningKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cloneIdentityProviderKey - errors', () => {
      it('should have a cloneIdentityProviderKey function', (done) => {
        try {
          assert.equal(true, typeof a.cloneIdentityProviderKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.cloneIdentityProviderKey(null, null, null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-cloneIdentityProviderKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyId', (done) => {
        try {
          a.cloneIdentityProviderKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'keyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-cloneIdentityProviderKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetIdpId', (done) => {
        try {
          a.cloneIdentityProviderKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'targetIdpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-cloneIdentityProviderKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateIdentityProvider - errors', () => {
      it('should have a activateIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.activateIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.activateIdentityProvider(null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateIdentityProvider - errors', () => {
      it('should have a deactivateIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.deactivateIdentityProvider(null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivateIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIdentityProviderApplicationUsers - errors', () => {
      it('should have a listIdentityProviderApplicationUsers function', (done) => {
        try {
          assert.equal(true, typeof a.listIdentityProviderApplicationUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.listIdentityProviderApplicationUsers(null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listIdentityProviderApplicationUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unlinkUserFromIdentityProvider - errors', () => {
      it('should have a unlinkUserFromIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.unlinkUserFromIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.unlinkUserFromIdentityProvider(null, null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-unlinkUserFromIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.unlinkUserFromIdentityProvider('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-unlinkUserFromIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityProviderApplicationUser - errors', () => {
      it('should have a getIdentityProviderApplicationUser function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityProviderApplicationUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.getIdentityProviderApplicationUser(null, null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getIdentityProviderApplicationUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.getIdentityProviderApplicationUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getIdentityProviderApplicationUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#linkUserToIdentityProvider - errors', () => {
      it('should have a linkUserToIdentityProvider function', (done) => {
        try {
          assert.equal(true, typeof a.linkUserToIdentityProvider === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.linkUserToIdentityProvider(null, null, null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-linkUserToIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.linkUserToIdentityProvider('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-linkUserToIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userIdentityProviderLinkRequest', (done) => {
        try {
          a.linkUserToIdentityProvider('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'userIdentityProviderLinkRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-linkUserToIdentityProvider', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSocialAuthTokens - errors', () => {
      it('should have a listSocialAuthTokens function', (done) => {
        try {
          assert.equal(true, typeof a.listSocialAuthTokens === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.listSocialAuthTokens(null, null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listSocialAuthTokens', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.listSocialAuthTokens('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listSocialAuthTokens', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listInlineHooks - errors', () => {
      it('should have a listInlineHooks function', (done) => {
        try {
          assert.equal(true, typeof a.listInlineHooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createInlineHook - errors', () => {
      it('should have a createInlineHook function', (done) => {
        try {
          assert.equal(true, typeof a.createInlineHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inlineHook', (done) => {
        try {
          a.createInlineHook(null, (data, error) => {
            try {
              const displayE = 'inlineHook is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createInlineHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInlineHook - errors', () => {
      it('should have a deleteInlineHook function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInlineHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inlineHookId', (done) => {
        try {
          a.deleteInlineHook(null, (data, error) => {
            try {
              const displayE = 'inlineHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteInlineHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInlineHook - errors', () => {
      it('should have a getInlineHook function', (done) => {
        try {
          assert.equal(true, typeof a.getInlineHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inlineHookId', (done) => {
        try {
          a.getInlineHook(null, (data, error) => {
            try {
              const displayE = 'inlineHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getInlineHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateInlineHook - errors', () => {
      it('should have a updateInlineHook function', (done) => {
        try {
          assert.equal(true, typeof a.updateInlineHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inlineHookId', (done) => {
        try {
          a.updateInlineHook(null, null, (data, error) => {
            try {
              const displayE = 'inlineHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateInlineHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inlineHook', (done) => {
        try {
          a.updateInlineHook('fakeparam', null, (data, error) => {
            try {
              const displayE = 'inlineHook is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateInlineHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#executeInlineHook - errors', () => {
      it('should have a executeInlineHook function', (done) => {
        try {
          assert.equal(true, typeof a.executeInlineHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inlineHookId', (done) => {
        try {
          a.executeInlineHook(null, null, (data, error) => {
            try {
              const displayE = 'inlineHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-executeInlineHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payloadData', (done) => {
        try {
          a.executeInlineHook('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payloadData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-executeInlineHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateInlineHook - errors', () => {
      it('should have a activateInlineHook function', (done) => {
        try {
          assert.equal(true, typeof a.activateInlineHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inlineHookId', (done) => {
        try {
          a.activateInlineHook(null, (data, error) => {
            try {
              const displayE = 'inlineHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateInlineHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateInlineHook - errors', () => {
      it('should have a deactivateInlineHook function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateInlineHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing inlineHookId', (done) => {
        try {
          a.deactivateInlineHook(null, (data, error) => {
            try {
              const displayE = 'inlineHookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivateInlineHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogs - errors', () => {
      it('should have a getLogs function', (done) => {
        try {
          assert.equal(true, typeof a.getLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listProfileMappings - errors', () => {
      it('should have a listProfileMappings function', (done) => {
        try {
          assert.equal(true, typeof a.listProfileMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProfileMapping - errors', () => {
      it('should have a getProfileMapping function', (done) => {
        try {
          assert.equal(true, typeof a.getProfileMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mappingId', (done) => {
        try {
          a.getProfileMapping(null, (data, error) => {
            try {
              const displayE = 'mappingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getProfileMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateProfileMapping - errors', () => {
      it('should have a updateProfileMapping function', (done) => {
        try {
          assert.equal(true, typeof a.updateProfileMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mappingId', (done) => {
        try {
          a.updateProfileMapping(null, null, (data, error) => {
            try {
              const displayE = 'mappingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateProfileMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileMapping', (done) => {
        try {
          a.updateProfileMapping('fakeparam', null, (data, error) => {
            try {
              const displayE = 'profileMapping is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateProfileMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationUserSchema - errors', () => {
      it('should have a getApplicationUserSchema function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationUserSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appInstanceId', (done) => {
        try {
          a.getApplicationUserSchema(null, (data, error) => {
            try {
              const displayE = 'appInstanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getApplicationUserSchema', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateApplicationUserProfile - errors', () => {
      it('should have a updateApplicationUserProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updateApplicationUserProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appInstanceId', (done) => {
        try {
          a.updateApplicationUserProfile(null, null, (data, error) => {
            try {
              const displayE = 'appInstanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateApplicationUserProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserSchema - errors', () => {
      it('should have a getUserSchema function', (done) => {
        try {
          assert.equal(true, typeof a.getUserSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schemaId', (done) => {
        try {
          a.getUserSchema(null, (data, error) => {
            try {
              const displayE = 'schemaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getUserSchema', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUserProfile - errors', () => {
      it('should have a updateUserProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updateUserProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schemaId', (done) => {
        try {
          a.updateUserProfile(null, null, (data, error) => {
            try {
              const displayE = 'schemaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateUserProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userSchema', (done) => {
        try {
          a.updateUserProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userSchema is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateUserProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLinkedObjectDefinitions - errors', () => {
      it('should have a listLinkedObjectDefinitions function', (done) => {
        try {
          assert.equal(true, typeof a.listLinkedObjectDefinitions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addLinkedObjectDefinition - errors', () => {
      it('should have a addLinkedObjectDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.addLinkedObjectDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkedObject', (done) => {
        try {
          a.addLinkedObjectDefinition(null, (data, error) => {
            try {
              const displayE = 'linkedObject is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addLinkedObjectDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLinkedObjectDefinition - errors', () => {
      it('should have a deleteLinkedObjectDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLinkedObjectDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkedObjectName', (done) => {
        try {
          a.deleteLinkedObjectDefinition(null, (data, error) => {
            try {
              const displayE = 'linkedObjectName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteLinkedObjectDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLinkedObjectDefinition - errors', () => {
      it('should have a getLinkedObjectDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.getLinkedObjectDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkedObjectName', (done) => {
        try {
          a.getLinkedObjectDefinition(null, (data, error) => {
            try {
              const displayE = 'linkedObjectName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getLinkedObjectDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUserTypes - errors', () => {
      it('should have a listUserTypes function', (done) => {
        try {
          assert.equal(true, typeof a.listUserTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUserType - errors', () => {
      it('should have a createUserType function', (done) => {
        try {
          assert.equal(true, typeof a.createUserType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userType', (done) => {
        try {
          a.createUserType(null, (data, error) => {
            try {
              const displayE = 'userType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createUserType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserType - errors', () => {
      it('should have a deleteUserType function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUserType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.deleteUserType(null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteUserType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserType - errors', () => {
      it('should have a getUserType function', (done) => {
        try {
          assert.equal(true, typeof a.getUserType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.getUserType(null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getUserType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUserType - errors', () => {
      it('should have a updateUserType function', (done) => {
        try {
          assert.equal(true, typeof a.updateUserType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.updateUserType(null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateUserType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userType', (done) => {
        try {
          a.updateUserType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateUserType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceUserType - errors', () => {
      it('should have a replaceUserType function', (done) => {
        try {
          assert.equal(true, typeof a.replaceUserType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.replaceUserType(null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-replaceUserType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userType', (done) => {
        try {
          a.replaceUserType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-replaceUserType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPolicies - errors', () => {
      it('should have a listPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.listPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.listPolicies(null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPolicy - errors', () => {
      it('should have a createPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policy', (done) => {
        try {
          a.createPolicy(null, null, (data, error) => {
            try {
              const displayE = 'policy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicy - errors', () => {
      it('should have a deletePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deletePolicy(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deletePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicy - errors', () => {
      it('should have a getPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.getPolicy(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePolicy - errors', () => {
      it('should have a updatePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updatePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policy', (done) => {
        try {
          a.updatePolicy(null, null, (data, error) => {
            try {
              const displayE = 'policy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updatePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updatePolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updatePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activatePolicy - errors', () => {
      it('should have a activatePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.activatePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.activatePolicy(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activatePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivatePolicy - errors', () => {
      it('should have a deactivatePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deactivatePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deactivatePolicy(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivatePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPolicyRules - errors', () => {
      it('should have a listPolicyRules function', (done) => {
        try {
          assert.equal(true, typeof a.listPolicyRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.listPolicyRules(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listPolicyRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPolicyRule - errors', () => {
      it('should have a createPolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.createPolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.createPolicyRule(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyRule', (done) => {
        try {
          a.createPolicyRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyRule - errors', () => {
      it('should have a deletePolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deletePolicyRule(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deletePolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deletePolicyRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deletePolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyRule - errors', () => {
      it('should have a getPolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.getPolicyRule(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.getPolicyRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePolicyRule - errors', () => {
      it('should have a updatePolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.updatePolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updatePolicyRule(null, null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updatePolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updatePolicyRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updatePolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyRule', (done) => {
        try {
          a.updatePolicyRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updatePolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activatePolicyRule - errors', () => {
      it('should have a activatePolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.activatePolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.activatePolicyRule(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activatePolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.activatePolicyRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activatePolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivatePolicyRule - errors', () => {
      it('should have a deactivatePolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.deactivatePolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deactivatePolicyRule(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivatePolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deactivatePolicyRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivatePolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSession - errors', () => {
      it('should have a createSession function', (done) => {
        try {
          assert.equal(true, typeof a.createSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createSessionRequest', (done) => {
        try {
          a.createSession(null, (data, error) => {
            try {
              const displayE = 'createSessionRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#endSession - errors', () => {
      it('should have a endSession function', (done) => {
        try {
          assert.equal(true, typeof a.endSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.endSession(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-endSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSession - errors', () => {
      it('should have a getSession function', (done) => {
        try {
          assert.equal(true, typeof a.getSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.getSession(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshSession - errors', () => {
      it('should have a refreshSession function', (done) => {
        try {
          assert.equal(true, typeof a.refreshSession === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionId', (done) => {
        try {
          a.refreshSession(null, (data, error) => {
            try {
              const displayE = 'sessionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-refreshSession', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSmsTemplates - errors', () => {
      it('should have a listSmsTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.listSmsTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSmsTemplate - errors', () => {
      it('should have a createSmsTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createSmsTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing smsTemplate', (done) => {
        try {
          a.createSmsTemplate(null, (data, error) => {
            try {
              const displayE = 'smsTemplate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createSmsTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSmsTemplate - errors', () => {
      it('should have a deleteSmsTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSmsTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteSmsTemplate(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteSmsTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSmsTemplate - errors', () => {
      it('should have a getSmsTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getSmsTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getSmsTemplate(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getSmsTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partialUpdateSmsTemplate - errors', () => {
      it('should have a partialUpdateSmsTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.partialUpdateSmsTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.partialUpdateSmsTemplate(null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-partialUpdateSmsTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing smsTemplate', (done) => {
        try {
          a.partialUpdateSmsTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'smsTemplate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-partialUpdateSmsTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSmsTemplate - errors', () => {
      it('should have a updateSmsTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updateSmsTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateSmsTemplate(null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateSmsTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing smsTemplate', (done) => {
        try {
          a.updateSmsTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'smsTemplate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateSmsTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCurrentConfiguration - errors', () => {
      it('should have a getCurrentConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getCurrentConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateConfiguration - errors', () => {
      it('should have a updateConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.updateConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing threatInsightConfiguration', (done) => {
        try {
          a.updateConfiguration(null, (data, error) => {
            try {
              const displayE = 'threatInsightConfiguration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listOrigins - errors', () => {
      it('should have a listOrigins function', (done) => {
        try {
          assert.equal(true, typeof a.listOrigins === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrigin - errors', () => {
      it('should have a createOrigin function', (done) => {
        try {
          assert.equal(true, typeof a.createOrigin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trustedOrigin', (done) => {
        try {
          a.createOrigin(null, (data, error) => {
            try {
              const displayE = 'trustedOrigin is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createOrigin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrigin - errors', () => {
      it('should have a deleteOrigin function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOrigin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trustedOriginId', (done) => {
        try {
          a.deleteOrigin(null, (data, error) => {
            try {
              const displayE = 'trustedOriginId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteOrigin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrigin - errors', () => {
      it('should have a getOrigin function', (done) => {
        try {
          assert.equal(true, typeof a.getOrigin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trustedOriginId', (done) => {
        try {
          a.getOrigin(null, (data, error) => {
            try {
              const displayE = 'trustedOriginId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getOrigin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrigin - errors', () => {
      it('should have a updateOrigin function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrigin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trustedOriginId', (done) => {
        try {
          a.updateOrigin(null, null, (data, error) => {
            try {
              const displayE = 'trustedOriginId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateOrigin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trustedOrigin', (done) => {
        try {
          a.updateOrigin('fakeparam', null, (data, error) => {
            try {
              const displayE = 'trustedOrigin is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateOrigin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateOrigin - errors', () => {
      it('should have a activateOrigin function', (done) => {
        try {
          assert.equal(true, typeof a.activateOrigin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trustedOriginId', (done) => {
        try {
          a.activateOrigin(null, (data, error) => {
            try {
              const displayE = 'trustedOriginId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateOrigin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateOrigin - errors', () => {
      it('should have a deactivateOrigin function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateOrigin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trustedOriginId', (done) => {
        try {
          a.deactivateOrigin(null, (data, error) => {
            try {
              const displayE = 'trustedOriginId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivateOrigin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUsers - errors', () => {
      it('should have a listUsers function', (done) => {
        try {
          assert.equal(true, typeof a.listUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUser - errors', () => {
      it('should have a createUser function', (done) => {
        try {
          assert.equal(true, typeof a.createUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createUser(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setLinkedObjectForUser - errors', () => {
      it('should have a setLinkedObjectForUser function', (done) => {
        try {
          assert.equal(true, typeof a.setLinkedObjectForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associatedUserId', (done) => {
        try {
          a.setLinkedObjectForUser(null, null, null, (data, error) => {
            try {
              const displayE = 'associatedUserId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-setLinkedObjectForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing primaryRelationshipName', (done) => {
        try {
          a.setLinkedObjectForUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'primaryRelationshipName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-setLinkedObjectForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing primaryUserId', (done) => {
        try {
          a.setLinkedObjectForUser('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'primaryUserId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-setLinkedObjectForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateOrDeleteUser - errors', () => {
      it('should have a deactivateOrDeleteUser function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateOrDeleteUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.deactivateOrDeleteUser(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivateOrDeleteUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUser - errors', () => {
      it('should have a getUser function', (done) => {
        try {
          assert.equal(true, typeof a.getUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.getUser(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#partialUpdateUser - errors', () => {
      it('should have a partialUpdateUser function', (done) => {
        try {
          assert.equal(true, typeof a.partialUpdateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing user', (done) => {
        try {
          a.partialUpdateUser(null, null, null, (data, error) => {
            try {
              const displayE = 'user is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-partialUpdateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.partialUpdateUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-partialUpdateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUser - errors', () => {
      it('should have a updateUser function', (done) => {
        try {
          assert.equal(true, typeof a.updateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing user', (done) => {
        try {
          a.updateUser(null, null, null, (data, error) => {
            try {
              const displayE = 'user is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.updateUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppLinks - errors', () => {
      it('should have a listAppLinks function', (done) => {
        try {
          assert.equal(true, typeof a.listAppLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.listAppLinks(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listAppLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUserClients - errors', () => {
      it('should have a listUserClients function', (done) => {
        try {
          assert.equal(true, typeof a.listUserClients === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.listUserClients(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listUserClients', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeGrantsForUserAndClient - errors', () => {
      it('should have a revokeGrantsForUserAndClient function', (done) => {
        try {
          assert.equal(true, typeof a.revokeGrantsForUserAndClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.revokeGrantsForUserAndClient(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeGrantsForUserAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.revokeGrantsForUserAndClient('fakeparam', null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeGrantsForUserAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGrantsForUserAndClient - errors', () => {
      it('should have a listGrantsForUserAndClient function', (done) => {
        try {
          assert.equal(true, typeof a.listGrantsForUserAndClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.listGrantsForUserAndClient(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listGrantsForUserAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.listGrantsForUserAndClient('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listGrantsForUserAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeTokensForUserAndClient - errors', () => {
      it('should have a revokeTokensForUserAndClient function', (done) => {
        try {
          assert.equal(true, typeof a.revokeTokensForUserAndClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.revokeTokensForUserAndClient(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeTokensForUserAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.revokeTokensForUserAndClient('fakeparam', null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeTokensForUserAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRefreshTokensForUserAndClient - errors', () => {
      it('should have a listRefreshTokensForUserAndClient function', (done) => {
        try {
          assert.equal(true, typeof a.listRefreshTokensForUserAndClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.listRefreshTokensForUserAndClient(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listRefreshTokensForUserAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.listRefreshTokensForUserAndClient('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listRefreshTokensForUserAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeTokenForUserAndClient - errors', () => {
      it('should have a revokeTokenForUserAndClient function', (done) => {
        try {
          assert.equal(true, typeof a.revokeTokenForUserAndClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.revokeTokenForUserAndClient(null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeTokenForUserAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.revokeTokenForUserAndClient('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeTokenForUserAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tokenId', (done) => {
        try {
          a.revokeTokenForUserAndClient('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'tokenId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeTokenForUserAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRefreshTokenForUserAndClient - errors', () => {
      it('should have a getRefreshTokenForUserAndClient function', (done) => {
        try {
          assert.equal(true, typeof a.getRefreshTokenForUserAndClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.getRefreshTokenForUserAndClient(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getRefreshTokenForUserAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.getRefreshTokenForUserAndClient('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getRefreshTokenForUserAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tokenId', (done) => {
        try {
          a.getRefreshTokenForUserAndClient('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'tokenId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getRefreshTokenForUserAndClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changePassword - errors', () => {
      it('should have a changePassword function', (done) => {
        try {
          assert.equal(true, typeof a.changePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing changePasswordRequest', (done) => {
        try {
          a.changePassword(null, null, null, (data, error) => {
            try {
              const displayE = 'changePasswordRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-changePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.changePassword('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-changePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeRecoveryQuestion - errors', () => {
      it('should have a changeRecoveryQuestion function', (done) => {
        try {
          assert.equal(true, typeof a.changeRecoveryQuestion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userCredentials', (done) => {
        try {
          a.changeRecoveryQuestion(null, null, (data, error) => {
            try {
              const displayE = 'userCredentials is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-changeRecoveryQuestion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.changeRecoveryQuestion('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-changeRecoveryQuestion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postApiV1UsersUserIdCredentialsForgotPassword - errors', () => {
      it('should have a postApiV1UsersUserIdCredentialsForgotPassword function', (done) => {
        try {
          assert.equal(true, typeof a.postApiV1UsersUserIdCredentialsForgotPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.postApiV1UsersUserIdCredentialsForgotPassword(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-postApiV1UsersUserIdCredentialsForgotPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeUserGrants - errors', () => {
      it('should have a revokeUserGrants function', (done) => {
        try {
          assert.equal(true, typeof a.revokeUserGrants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.revokeUserGrants(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeUserGrants', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUserGrants - errors', () => {
      it('should have a listUserGrants function', (done) => {
        try {
          assert.equal(true, typeof a.listUserGrants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.listUserGrants(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listUserGrants', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeUserGrant - errors', () => {
      it('should have a revokeUserGrant function', (done) => {
        try {
          assert.equal(true, typeof a.revokeUserGrant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.revokeUserGrant(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeUserGrant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing grantId', (done) => {
        try {
          a.revokeUserGrant('fakeparam', null, (data, error) => {
            try {
              const displayE = 'grantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-revokeUserGrant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserGrant - errors', () => {
      it('should have a getUserGrant function', (done) => {
        try {
          assert.equal(true, typeof a.getUserGrant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.getUserGrant(null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getUserGrant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing grantId', (done) => {
        try {
          a.getUserGrant('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'grantId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getUserGrant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUserGroups - errors', () => {
      it('should have a listUserGroups function', (done) => {
        try {
          assert.equal(true, typeof a.listUserGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.listUserGroups(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listUserGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUserIdentityProviders - errors', () => {
      it('should have a listUserIdentityProviders function', (done) => {
        try {
          assert.equal(true, typeof a.listUserIdentityProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.listUserIdentityProviders(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listUserIdentityProviders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateUser - errors', () => {
      it('should have a activateUser function', (done) => {
        try {
          assert.equal(true, typeof a.activateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.activateUser(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sendEmail', (done) => {
        try {
          a.activateUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sendEmail is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateUser - errors', () => {
      it('should have a deactivateUser function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.deactivateUser(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expirePassword - errors', () => {
      it('should have a expirePassword function', (done) => {
        try {
          assert.equal(true, typeof a.expirePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.expirePassword(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-expirePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#expirePasswordAndGetTemporaryPassword - errors', () => {
      it('should have a expirePasswordAndGetTemporaryPassword function', (done) => {
        try {
          assert.equal(true, typeof a.expirePasswordAndGetTemporaryPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.expirePasswordAndGetTemporaryPassword(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-expirePasswordAndGetTemporaryPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reactivateUser - errors', () => {
      it('should have a reactivateUser function', (done) => {
        try {
          assert.equal(true, typeof a.reactivateUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.reactivateUser(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-reactivateUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetFactors - errors', () => {
      it('should have a resetFactors function', (done) => {
        try {
          assert.equal(true, typeof a.resetFactors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.resetFactors(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-resetFactors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetPassword - errors', () => {
      it('should have a resetPassword function', (done) => {
        try {
          assert.equal(true, typeof a.resetPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.resetPassword(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-resetPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sendEmail', (done) => {
        try {
          a.resetPassword('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sendEmail is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-resetPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#suspendUser - errors', () => {
      it('should have a suspendUser function', (done) => {
        try {
          assert.equal(true, typeof a.suspendUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.suspendUser(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-suspendUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unlockUser - errors', () => {
      it('should have a unlockUser function', (done) => {
        try {
          assert.equal(true, typeof a.unlockUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.unlockUser(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-unlockUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unsuspendUser - errors', () => {
      it('should have a unsuspendUser function', (done) => {
        try {
          assert.equal(true, typeof a.unsuspendUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.unsuspendUser(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-unsuspendUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeLinkedObjectForUser - errors', () => {
      it('should have a removeLinkedObjectForUser function', (done) => {
        try {
          assert.equal(true, typeof a.removeLinkedObjectForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.removeLinkedObjectForUser(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeLinkedObjectForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing relationshipName', (done) => {
        try {
          a.removeLinkedObjectForUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'relationshipName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeLinkedObjectForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLinkedObjectsForUser - errors', () => {
      it('should have a getLinkedObjectsForUser function', (done) => {
        try {
          assert.equal(true, typeof a.getLinkedObjectsForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.getLinkedObjectsForUser(null, null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getLinkedObjectsForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing relationshipName', (done) => {
        try {
          a.getLinkedObjectsForUser('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'relationshipName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getLinkedObjectsForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAssignedRolesForUser - errors', () => {
      it('should have a listAssignedRolesForUser function', (done) => {
        try {
          assert.equal(true, typeof a.listAssignedRolesForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.listAssignedRolesForUser(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listAssignedRolesForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignRoleToUser - errors', () => {
      it('should have a assignRoleToUser function', (done) => {
        try {
          assert.equal(true, typeof a.assignRoleToUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing assignRoleRequest', (done) => {
        try {
          a.assignRoleToUser(null, null, null, (data, error) => {
            try {
              const displayE = 'assignRoleRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-assignRoleToUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.assignRoleToUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-assignRoleToUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeRoleFromUser - errors', () => {
      it('should have a removeRoleFromUser function', (done) => {
        try {
          assert.equal(true, typeof a.removeRoleFromUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.removeRoleFromUser(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeRoleFromUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.removeRoleFromUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeRoleFromUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApplicationTargetsForApplicationAdministratorRoleForUser - errors', () => {
      it('should have a listApplicationTargetsForApplicationAdministratorRoleForUser function', (done) => {
        try {
          assert.equal(true, typeof a.listApplicationTargetsForApplicationAdministratorRoleForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.listApplicationTargetsForApplicationAdministratorRoleForUser(null, null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listApplicationTargetsForApplicationAdministratorRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.listApplicationTargetsForApplicationAdministratorRoleForUser('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listApplicationTargetsForApplicationAdministratorRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAllAppsAsTargetToRole - errors', () => {
      it('should have a addAllAppsAsTargetToRole function', (done) => {
        try {
          assert.equal(true, typeof a.addAllAppsAsTargetToRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.addAllAppsAsTargetToRole(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addAllAppsAsTargetToRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.addAllAppsAsTargetToRole('fakeparam', null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addAllAppsAsTargetToRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeApplicationTargetFromApplicationAdministratorRoleForUser - errors', () => {
      it('should have a removeApplicationTargetFromApplicationAdministratorRoleForUser function', (done) => {
        try {
          assert.equal(true, typeof a.removeApplicationTargetFromApplicationAdministratorRoleForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.removeApplicationTargetFromApplicationAdministratorRoleForUser(null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeApplicationTargetFromApplicationAdministratorRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.removeApplicationTargetFromApplicationAdministratorRoleForUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeApplicationTargetFromApplicationAdministratorRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appName', (done) => {
        try {
          a.removeApplicationTargetFromApplicationAdministratorRoleForUser('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'appName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeApplicationTargetFromApplicationAdministratorRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addApplicationTargetToAdminRoleForUser - errors', () => {
      it('should have a addApplicationTargetToAdminRoleForUser function', (done) => {
        try {
          assert.equal(true, typeof a.addApplicationTargetToAdminRoleForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.addApplicationTargetToAdminRoleForUser(null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addApplicationTargetToAdminRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.addApplicationTargetToAdminRoleForUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addApplicationTargetToAdminRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appName', (done) => {
        try {
          a.addApplicationTargetToAdminRoleForUser('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'appName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addApplicationTargetToAdminRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeApplicationTargetFromAdministratorRoleForUser - errors', () => {
      it('should have a removeApplicationTargetFromAdministratorRoleForUser function', (done) => {
        try {
          assert.equal(true, typeof a.removeApplicationTargetFromAdministratorRoleForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.removeApplicationTargetFromAdministratorRoleForUser(null, null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeApplicationTargetFromAdministratorRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.removeApplicationTargetFromAdministratorRoleForUser('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeApplicationTargetFromAdministratorRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appName', (done) => {
        try {
          a.removeApplicationTargetFromAdministratorRoleForUser('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'appName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeApplicationTargetFromAdministratorRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.removeApplicationTargetFromAdministratorRoleForUser('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeApplicationTargetFromAdministratorRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addApplicationTargetToAppAdminRoleForUser - errors', () => {
      it('should have a addApplicationTargetToAppAdminRoleForUser function', (done) => {
        try {
          assert.equal(true, typeof a.addApplicationTargetToAppAdminRoleForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.addApplicationTargetToAppAdminRoleForUser(null, null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addApplicationTargetToAppAdminRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.addApplicationTargetToAppAdminRoleForUser('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addApplicationTargetToAppAdminRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appName', (done) => {
        try {
          a.addApplicationTargetToAppAdminRoleForUser('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'appName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addApplicationTargetToAppAdminRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.addApplicationTargetToAppAdminRoleForUser('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addApplicationTargetToAppAdminRoleForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGroupTargetsForRole - errors', () => {
      it('should have a listGroupTargetsForRole function', (done) => {
        try {
          assert.equal(true, typeof a.listGroupTargetsForRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.listGroupTargetsForRole(null, null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listGroupTargetsForRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.listGroupTargetsForRole('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listGroupTargetsForRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeGroupTargetFromRole - errors', () => {
      it('should have a removeGroupTargetFromRole function', (done) => {
        try {
          assert.equal(true, typeof a.removeGroupTargetFromRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.removeGroupTargetFromRole(null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeGroupTargetFromRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.removeGroupTargetFromRole('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeGroupTargetFromRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.removeGroupTargetFromRole('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-removeGroupTargetFromRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addGroupTargetToRole - errors', () => {
      it('should have a addGroupTargetToRole function', (done) => {
        try {
          assert.equal(true, typeof a.addGroupTargetToRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.addGroupTargetToRole(null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addGroupTargetToRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleId', (done) => {
        try {
          a.addGroupTargetToRole('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'roleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addGroupTargetToRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.addGroupTargetToRole('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-addGroupTargetToRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clearUserSessions - errors', () => {
      it('should have a clearUserSessions function', (done) => {
        try {
          assert.equal(true, typeof a.clearUserSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.clearUserSessions(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-clearUserSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFactors - errors', () => {
      it('should have a listFactors function', (done) => {
        try {
          assert.equal(true, typeof a.listFactors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.listFactors(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listFactors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enrollFactor - errors', () => {
      it('should have a enrollFactor function', (done) => {
        try {
          assert.equal(true, typeof a.enrollFactor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.enrollFactor(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-enrollFactor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.enrollFactor('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-enrollFactor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSupportedFactors - errors', () => {
      it('should have a listSupportedFactors function', (done) => {
        try {
          assert.equal(true, typeof a.listSupportedFactors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.listSupportedFactors(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listSupportedFactors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSupportedSecurityQuestions - errors', () => {
      it('should have a listSupportedSecurityQuestions function', (done) => {
        try {
          assert.equal(true, typeof a.listSupportedSecurityQuestions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.listSupportedSecurityQuestions(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-listSupportedSecurityQuestions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFactor - errors', () => {
      it('should have a deleteFactor function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFactor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.deleteFactor(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteFactor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing factorId', (done) => {
        try {
          a.deleteFactor('fakeparam', null, (data, error) => {
            try {
              const displayE = 'factorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteFactor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFactor - errors', () => {
      it('should have a getFactor function', (done) => {
        try {
          assert.equal(true, typeof a.getFactor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.getFactor(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getFactor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing factorId', (done) => {
        try {
          a.getFactor('fakeparam', null, (data, error) => {
            try {
              const displayE = 'factorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getFactor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateFactor - errors', () => {
      it('should have a activateFactor function', (done) => {
        try {
          assert.equal(true, typeof a.activateFactor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.activateFactor(null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateFactor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing factorId', (done) => {
        try {
          a.activateFactor('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'factorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateFactor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFactorTransactionStatus - errors', () => {
      it('should have a getFactorTransactionStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getFactorTransactionStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.getFactorTransactionStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getFactorTransactionStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing factorId', (done) => {
        try {
          a.getFactorTransactionStatus('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'factorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getFactorTransactionStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transactionId', (done) => {
        try {
          a.getFactorTransactionStatus('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'transactionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getFactorTransactionStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verifyFactor - errors', () => {
      it('should have a verifyFactor function', (done) => {
        try {
          assert.equal(true, typeof a.verifyFactor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.verifyFactor(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-verifyFactor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing factorId', (done) => {
        try {
          a.verifyFactor('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'factorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-verifyFactor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworkZones - errors', () => {
      it('should have a listNetworkZones function', (done) => {
        try {
          assert.equal(true, typeof a.listNetworkZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkZone - errors', () => {
      it('should have a createNetworkZone function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zone', (done) => {
        try {
          a.createNetworkZone(null, (data, error) => {
            try {
              const displayE = 'zone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-createNetworkZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkZone - errors', () => {
      it('should have a deleteNetworkZone function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneId', (done) => {
        try {
          a.deleteNetworkZone(null, (data, error) => {
            try {
              const displayE = 'zoneId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deleteNetworkZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkZone - errors', () => {
      it('should have a getNetworkZone function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneId', (done) => {
        try {
          a.getNetworkZone(null, (data, error) => {
            try {
              const displayE = 'zoneId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-getNetworkZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkZone - errors', () => {
      it('should have a updateNetworkZone function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneId', (done) => {
        try {
          a.updateNetworkZone(null, null, (data, error) => {
            try {
              const displayE = 'zoneId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateNetworkZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zone', (done) => {
        try {
          a.updateNetworkZone('fakeparam', null, (data, error) => {
            try {
              const displayE = 'zone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-updateNetworkZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activateNetworkZone - errors', () => {
      it('should have a activateNetworkZone function', (done) => {
        try {
          assert.equal(true, typeof a.activateNetworkZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneId', (done) => {
        try {
          a.activateNetworkZone(null, (data, error) => {
            try {
              const displayE = 'zoneId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-activateNetworkZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateNetworkZone - errors', () => {
      it('should have a deactivateNetworkZone function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateNetworkZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneId', (done) => {
        try {
          a.deactivateNetworkZone(null, (data, error) => {
            try {
              const displayE = 'zoneId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-okta-adapter-deactivateNetworkZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
