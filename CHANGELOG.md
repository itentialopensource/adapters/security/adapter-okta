
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:32PM

See merge request itentialopensource/adapters/adapter-okta!11

---

## 0.4.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-okta!9

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:45PM

See merge request itentialopensource/adapters/adapter-okta!8

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:58PM

See merge request itentialopensource/adapters/adapter-okta!7

---

## 0.4.0 [05-16-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-okta!6

---

## 0.3.3 [03-27-2024]

* Changes made at 2024.03.27_13:44PM

See merge request itentialopensource/adapters/security/adapter-okta!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_16:16PM

See merge request itentialopensource/adapters/security/adapter-okta!4

---

## 0.3.1 [02-27-2024]

* Changes made at 2024.02.27_11:50AM

See merge request itentialopensource/adapters/security/adapter-okta!3

---

## 0.3.0 [01-02-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-okta!2

---

## 0.2.0 [06-02-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-okta!1

---

## 0.1.1 [06-04-2021]

- Initial Commit

See commit 69bed90

---
