# Okta

Vendor: Okta
Homepage: https://www.okta.com/

Product: Okta
Product Page: https://www.okta.com/

## Introduction
We classify Okta into the Security/SASE domain as Okta helps securely manage and authenticate users across various applications and services, ensuring streamlined access control and enhanced security.

## Why Integrate
The Okta adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Okta in order to enable secure authentication and authorization processes. With this adapter you have the ability to perform operations on items such as:

- Identity Provider
- User
- Policy

## Additional Product Documentation
The [API Documents for Okta](https://developer.okta.com/docs/reference/core-okta-api/)